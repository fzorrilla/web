<?php

use Illuminate\Database\Seeder;

class RelationshipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $relationships = [
        	['name' => 'Titular', 'valid' => '0'],
        	['name' => 'Conyuge', 'valid' => '0'],
        	['name' => 'Concubino(a)', 'valid' => '0'],
        	['name' => 'Madre gestante de hijo extramatrimonial', 'valid' => '0'],
        	['name' => 'Hijo no incapacitado', 'valid' => '0'],
        	['name' => 'Hijo incapacitado', 'valid' => '0'],
        	['name' => 'Padres - Calidad'],
        	['name' => 'Hermano(a)'],
        	['name' => 'Abuelo(a)'],
        	['name' => 'Tío(a)'],
        	['name' => 'Nieto(a)'],
        	['name' => 'Sobrino(a)'],
        	['name' => 'Primo(a)'],
        	['name' => 'Padres del conyuge o concubino(a)'],
        	['name' => 'Hijo(a) del conyuge o concubino(a)'],
        	['name' => 'Viudo(a)'],
        	['name' => 'Otros'],
        ];

    	foreach ($relationships as $relationship) {
        	App\Relationship::create($relationship);    	
    	}
    }
}
