<?php

use Illuminate\Database\Seeder;

class PathologiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pathologies = [
            ['name' => 'Ninguno'],
        	['name' => 'Neurológicas'],
        	['name' => 'Psicológicas o Psiquiátricas'],
        	['name' => 'Cardiovasculares'],
        	['name' => 'Vías Respiratorias'],
        	['name' => 'Gastrointestinales'],
        	['name' => 'Pared Abdominal'],
        	['name' => 'Endocrinas'],
        	['name' => 'Aparatos urinario y genital'],
        	['name' => 'Huesos'],
        	['name' => 'Hematológicas'],
        	['name' => 'Oftalmologicas'],
        	['name' => 'Otorrinolaringológicas'],
        	['name' => 'Dermatologica'],
        	['name' => 'Enfermedades congénitas'],
        	['name' => 'Enfermedades infecciosas'],
        	['name' => 'Oncológicas']
        ];

    	foreach ($pathologies as $pathology) {
        	App\Pathology::create($pathology);
    	}
    }
}
