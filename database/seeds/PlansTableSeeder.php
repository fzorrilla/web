<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            [
                'name' => 'MediSanitas',
                'description' => 'Plan de salud con cobertura ilimitada y asistencia internacional. Incluye cobertura de gastos de maternidad y niño sano al 100%',
                'client_id' => 1
            ], [
                'name' => 'Sanitas',
                'description' => 'Plan de salud con cobertura ilimitada. Incluye cobertura de gastos de maternidad y niño sano al 100%',
                'client_id' => 1
            ], [
                'name' => 'Integral',
                'description' => 'Plan integral de salud con cobertura ilimitada y asistencia internacional. Incluye cobertura de gastos de maternidad y niño sano al 100% ',
                'client_id' => 1
            ], [
                'name' => 'PEAS',
                'description' => 'Plan de salud con cobertura obligatoria ilimitada. Incluye cobertura de gastos de maternidad y niño sano al 100%',
                'client_id' => 1
            ], [
                'name' => 'MediSanitas Empresarial',
                'description' => 'Plan de salud con cobertura ilimitada y asistencia internacional. Incluye cobertura de gastos de maternidad y niño sano al 100%',
                // 'active' => "0",
                'client_id' => 2
            ], [
                'name' => 'Plan Salud Vital (PEAS)',
                'description' => 'Plan de salud con cobertura obligatoria ilimitada. Incluye cobertura de gastos de maternidad y niño sano al 100%',
                // 'active' => "0",
                'client_id' => 2
            ]
        ];

        foreach ($plans as $plan) {
            App\Plan::create($plan);
        }
    }
}
