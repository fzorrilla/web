<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pathology extends Model
{
	protected $table = 'pathologies';
    protected $fillable = ['pathology_id']; //voy a nombrarlo así para evitar conflictos con el mismo nombre del modelo

    public function diseases()
    {
         return $this->hasMany(Disease::class); //usar Torre en singular e inicial en mayúsculas al ser el nombre del modelo
    }

    public function families()
    {
    	return $this->belongsToMany(Family::class);
    }

}
