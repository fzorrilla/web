<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insured extends Model
{
    public function ages()
    {
        return $this->hasMany(Age::class);
    }

    public function families()
    {
    	return $this->hasMany(Family::class);
    }    
}
