<?php

// use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

// class ProvincesTableSeeder extends Seeder
class ProvincesTableSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct()
    {
        $this->table = 'provinces';
        $this->csv_delimiter = ';';
        $this->filename = base_path().'/database/seeds/csvs/provinces.csv';
        $this->offset_rows = 1;
        $this->mapping = [
            0 => 'name',
            1 => 'departament_id',
            2 => 'delivery',

        ];
        $this->should_trim = true;
    }

    public function run()
    {
        // $provinces = [
        //     ['name' => 'Chachapoyas', 'departament_id' => 1],
        //     ['name' => 'Bagua', 'departament_id' => 1],
        //     ['name' => 'Bongará', 'departament_id' => 1],
        //     ['name' => 'Condorcanqui', 'departament_id' => 1],
        //     ['name' => 'Luya', 'departament_id' => 1],
        //     ['name' => 'Rodríguez de Mendoza', 'departament_id' => 1],
        //     ['name' => 'Utcubamba', 'departament_id' => 1]
        // ];

        // foreach ($provinces as $province) {
        //     App\Province::create($province);
        // }

         parent::run();
    }
}
