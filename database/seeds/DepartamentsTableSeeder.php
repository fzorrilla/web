<?php

use Illuminate\Database\Seeder;

class DepartamentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departaments = [
            ['name' => 'Amazonas'],
            ['name' => 'Áncash'],
            ['name' => 'Apurímac'],
            ['name' => 'Arequipa'],
            ['name' => 'Ayacucho'],
            ['name' => 'Cajamarca'],
            ['name' => 'Callao','delivery' => '1'],
            ['name' => 'Cusco'],
            ['name' => 'Huancavelica'],
            ['name' => 'Huánuco'],
            ['name' => 'Ica'],
            ['name' => 'Junín'],
            ['name' => 'La Libertad'],
            ['name' => 'Lambayeque'],
            ['name' => 'Lima','delivery' => '1'],
            ['name' => 'Loreto'],
            ['name' => 'Madre de Dios'],
            ['name' => 'Moquegua'],
            ['name' => 'Pasco'],
            ['name' => 'Piura'],
            ['name' => 'Puno'],
            ['name' => 'San Martín'],
            ['name' => 'Tacna'],
            ['name' => 'Tumbes'],
            ['name' => 'Ucayali']
        ];

        foreach ($departaments as $departament) {
            App\Departament::create($departament);
        }
    }
}
