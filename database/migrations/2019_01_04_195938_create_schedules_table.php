<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('district_id');
            $table->string('address');
            $table->string('square')->nullable();
            $table->string('block')->nullable();
            $table->string('reference');
            $table->string('phone');
            $table->string('email');
            $table->date('date');
            $table->date('validity');
            $table->unsignedInteger('turn_id')->nullable();
            $table->unsignedInteger('quotation_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
