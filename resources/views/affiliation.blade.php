@extends('layouts.mob_new')

@section('title', 'Lima Lab Consulting Group')

@section('content')

    @if (session('info'))
        <div class="container">
            <div class="row">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('info') }}
                </div>
            </div>
        </div>
    @endif
    <div class="pl-5 pr-5 pb-5 pt-5" style="background-color: #f7f7f7">
        <div class="row mb-5" style="display: flex;align-items: center;">
            <div class="col-md-8">
                <h2 style="color: #8ea1b2;font-family: Gotham-Bold;font-size: 40px;text-transform: capitalize;">
                    Grupo Familiar
                </h2>
                <p style="color: #8ea1b2;font-family: Gotham-Light;font-size: 18px;text-transform: capitalize;">Cónyugue/Hijos menores</p>
            </div>
            <div class="col-md-4 text-right">
                <img src="{{ asset('img/socorro_personaje4.png') }}" alt="" class="chatbot__image">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="myTable" class="table table-hover table-striped table-bordered nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>Doc. Identidad</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Genero</th>
                            <th>Fe. Nacimiento</th>
                            <th>Tipo</th>
                            <th>Plan</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td>asdasd</td>
                            <td>asdasdasdsad</td>
                            <td>asdsadasdasd</td>
                            <td>asdsadasdasd</td>
                            <td>asdsadasdasd</td>
                            <td>asdsadasdasd</td>
                            <td>asdsadasdasd</td>
                            <td>
                                <button type="button" class="btn btn-success btn-completar btn-xs btn-block" data-toggle="modal" data-target="#myModal"">
                                    Completar
                                </button>
                            </td>

                        </tr>
                        
                    </tbody>
                </table>

            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                
            </div>
            <div class="col-md-4 text-right">
                <a class="btn btn-completar btn-success btn-block">
                   Agregar (+) Dependientes
                </a>
            </div>
            <div class="col-md-3  text-center">
                <a href="" class="btn cotizar btn-cotiza btn-warning btn-block" id="schedule">Cotizar</a>
            </div>
        </div>
    </div>
@stop

