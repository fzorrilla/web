<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = [
            ['name' => 'Potestativo (Personas)'],
            ['name' => 'Empresas'],
        ];

        foreach ($clients as $client) {
            App\Client::create($client);      
        }
    }
}
