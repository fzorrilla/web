@extends('layouts.mob_new')

@section('title', 'Lima Lab Consulting Group')

@section('content') {{-- {{ dd($quotation) }} --}}
<div class="container culqui pb-5 mb-5">
    <div class="row title-pay">
        <div class="col-md-5 text-center active">
           <h1 style="font-family: Gotham-Bold !important;">Datos Personales</h1> 
        </div>
        <div class="col-md-7 text-center">
           <h1 style="font-family: Gotham-Bold !important;">Declaracion de Salud</h1> 
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group" style="width: 100%;">
                        <label for="NameOnCard">Tipo de Documento (*)</label>
                    </div>
                    <div class="col-md-4">
                        <div class="radio-inline">
                            <label><input type="radio" id="dni" name="type" value="1" checked>DNI</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio-inline">
                            <label><input type="radio" id="ce" name="type" value="2" >CE</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio-inline">
                            <label><input type="radio" id="passport" name="type" value="3" >Pasaporte</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Número de Documento (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Nombres Completos (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group" style="width: 100%;">
                        <label for="NameOnCard">Género (*)</label>
                    </div>
                    <div class="col-md-4">
                        <div class="radio-inline">
                            <label><input type="radio" id="dni" name="type" value="1" checked>Masculino</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio-inline">
                            <label><input type="radio" id="ce" name="type" value="2" >Femenino</label>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Apellidos Completos (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Nacionalidad (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Estado Civil (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="NameOnCard">Fecha de Nacimiento</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="NameOnCard">Edad</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Teléfono (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Correo Electrónico (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Pais (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Departamento (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Provincia (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Distrito (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="NameOnCard">Direccion (*)</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <span style="color: #91a3b3">Los campos (*) son requeridos</span>
            </div>
            <div class="col-md-6">
                <div class="pad" style="width: 25%;"> 
                    <button type="submit" class="btn btn-small btn-block ml-4 mr-4">
                        <span class="submit-button-lock"></span>
                        <span class="align-middle">Guardar</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    
</div>
@stop

@section('css')
 
@stop

@section('js')

@stop
