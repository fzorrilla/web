<?php

use Illuminate\Database\Seeder;

class AgesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ages = [
            [
                'name' => '0-9',
                'min' => '0',
                'max' => '9',
                'description' => 'De 0 a 9 años',
                'insured_id' => '3'
            ], [
                'name' => '10-14',
                'min' => '10',
                'max' => '14',            
                'description' => 'De 10 a 14 años',
                'insured_id' => '3'
            ], [
                'name' => '15-18',
                'min' => '15',
                'max' => '18',             
                'description' => 'De 15 a 18 años',
                'insured_id' => '3'
            ], [
                'name' => '15-44',
                'min' => '15',
                'max' => '44',             
                'description' => 'De 18 a 44 años',
                'insured_id' => '2'
            ], [
                'name' => '45-59',
                'min' => '45',
                'max' => '59',             
                'description' => 'De 45 a 59 años',
                'insured_id' => '2'
            ], [
                'name' => '60-64',
                'min' => '60',
                'max' => '64',            
                'description' => 'De 60 a 64 años',
                'insured_id' => '2'
            ], [
                'name' => '65-69',
                'min' => '65',
                'max' => '69',             
                'description' => 'De 65 a 69 años',
                'insured_id' => '2'
            ], [
                'name' => '65',
                'min' => '65',
                'max' => '99999',
                'active' => '0',             
                'description' => 'Mayor de 65 años',
                'insured_id' => '2'
            ], [
                'name' => '70',
                'min' => '70',
                'max' => '99999',             
                'description' => 'Mayor de 70 años',
                'insured_id' => '2'
            ], [
                'name' => '18-28',
                'min' => '18',
                'max' => '28',             
                'description' => 'De 18 a 28 años',
                'insured_id' => '4'
            ], [
                'name' => '0-99999',
                'min' => '0',
                'max' => '99999',
                'active' => '0',             
                'description' => 'Todas las edades',
            ]
        ];       

        // App\Age::create([
        //     'name' => '15-25',
        //     'min' => '15',
        //     'max' => '25',             
        //     'description' => 'De 15 a 25 años'
        // ]);

        // App\Age::create([
        //     'name' => '18-25',
        //     'min' => '18',
        //     'max' => '25',              
        //     'description' => 'De 18 a 25 años'
        // ]);        

        // App\Age::create([
        //     'name' => '70',
        //     'min' => '70',
        //     'max' => '99999',             
        //     'description' => 'Mayor de 70 años'
        // ]);

        foreach ($ages as $age) {
            App\Age::create($age);      
        }                        
    }
}
