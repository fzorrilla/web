<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});
Route::get('/faq', function () {
	return view('faqview');
});
Route::get('/seguros', function () {
	return view('segurosview');
});
Route::get('/socorro', function () {
	return view('socorroview');
});
Route::get('/seguroEmp', function () {
	return view('seguroEmp');
});
Route::get('/seguroInd', function () {
	return view('seguroInd');
});
Route::get('/payinvoice', function () {
	return view('payinvoice');
});
Route::get('/chatbot/{client?}', 'QuotationController@index')->name('chatbot');

Route::post('/quotation', 'QuotationController@response')->name('response');
Route::put('/quotation/{quotation}', 'QuotationController@update')
->name('public.quotation.update');
Route::get('/store', 'AffiliationController@store')->name('public.store');
Route::get('/quotation2', function () {
	return view('plans.quotation');
})->name('public.plans.quotation');
Route::get('/data', function () {
	return view('formData');
})->name('formData');
Route::get('/affiliation', function () {
	return view('affiliation');
})->name('affiliation');

