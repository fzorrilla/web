@extends('layouts.mob')

@section('title', 'SOCORRO WEB')

@section('content')
<div style="background: #FFF;">
  <div style="background: #e2e2e252;">
    <div class="row">
      <div class="col-md-12">
        <h2 class="title-section" style="margin-left: 13%;">
          PREGUNTAS QUE SIEMPRE ME HACEN
        </h2>
        <br>
        <hr class="borde-naranja" style="margin-left: 13%;">
      </div>
    </div>
  </div>
  <div class="container">  
    <p class="pt-4"></p>
    <div class="row">
      <div class="col-md-12">
        <div id="accordion" class="faq-socorro">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link btn-pregunta" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <img src="{{ asset('img/plus.png') }}">¿Qué es un EPS?
                </button>
              </h5>
            </div>

            <div id="collapseOne" class="collapse text-left" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                  Un EPS es una Entidad Prestadora de Salud y se encarga de administrar tu plan de salud y ofrecerte servicios médicos a través de las clínicas y otras instituciones de salud afiliadas.
                </div>
              </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link btn-pregunta collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <img src="{{ asset('img/plus.png') }}">¿Qué es un Plan de Salud?
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                Un Plan de Salud es un convenio que se tiene entre la EPS y sus usuarios (ustedes) donde se especifica las coberturas, exclusiones, copagos y red de clínicas disponibles a los usuarios (ustedes).
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed btn-pregunta" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  <img src="{{ asset('img/plus.png') }}">¿Existe límite de edad para afiliarme a un Plan de Salud?
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                No, la edad no es una limitación.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed btn-pregunta" data-toggle="collapse" data-target="#collapseF" aria-expanded="false" aria-controls="collapseThree">
                  <img src="{{ asset('img/plus.png') }}">¿Qué es un Copago?
                </button>
              </h5>
            </div>
            <div id="collapseF" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                Los usuarios que se atienden por alguna institución de salud participan del pago del servicio con aporte
menor. En el sistema de EPS hay dos formas esto; deducibles, que es un porcentaje de la factura, o
copago que es un monto fijo. En Socorro ofrecemos Planes de Salud de Sanitas Peru en los cuales solo
se exige un copago. Es decir, un monto fijo sin importar cuan alta sea la factura.
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed btn-pregunta" data-toggle="collapse" data-target="#collapseFi" aria-expanded="false" aria-controls="collapseThree">
                    <img src="{{ asset('img/plus.png') }}">¿Qué es Carencia?
                  </button>
                </h5>
              </div>
              <div id="collapseFi" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                  Carencia es el periodo de espera donde no puedes hacer el uso del plan de salud para coberturas muy
  específicas.
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed btn-pregunta text-left" data-toggle="collapse" data-target="#collapses" aria-expanded="false" aria-controls="collapseThree">
                    <img src="{{ asset('img/plus.png') }}">¿Existe algún periodo de carencia y/o espera para recibir las prestaciones del Plan de Salud?
                  </button>
                </h5>
              </div>
              <div id="collapses" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                  Hay un periodo de carencia de 30 días para las personas que adquieren un seguro independientemente o potestativo.<br>
                  En el caso de periodos de espera, estas se dan para determinadas atenciones:
                  <ul>
                    <li>Hospitalización, cirugías programadas, Unidad de Cuidados Intensivos (UCI): 3 meses.</li>
                    <li>Medios complementarios de diagnóstico (resonancias, tomografías, endoscopias, etc.): 3 meses.</li>
                    <li>Exámenes complementarios de diagnóstico (como resonancias, tomografía, endoscopía, etc.): 3 meses</li>
                    <li>Maternidad (atención obstétrica del parto o cesárea): 9 meses.</li>
                    <li>Oncología (Quimioterapia, radioterapia, braquiterapia): 12 meses.</li>
                    <li>Prótesis articulares (rodilla, cadera y hombro), Stent coronario: 37 meses.</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed btn-pregunta text-left" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapseThree">
                    <img src="{{ asset('img/plus.png') }}">¿Se cubren las preexistencias?
                  </button>
                </h5>
              </div>
              <div id="collapse8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                  Los planes familiares (potestativos) incluyen la cobertura del PEAS (Plan Esencial de aseguramiento en
salud), que comprende un listado priorizado de 1015 diagnósticos de condiciones asegurables e
intervenciones garantizadas por el Aseguramiento Universal en Salud.<br>
Para los planes empresariales (regulares) la cobertura se dará, según lo indique cada plan de salud.
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed btn-pregunta text-left" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapseThree">
                    <img src="{{ asset('img/plus.png') }}">¿Qué no me cubre una EPS?
                  </button>
                </h5>
              </div>
              <div id="collapse9" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                  No se cubre las exclusiones que se encuentran detalladas en el Plan de Salud.
                </div>
              </div>
            </div>  
            <div class="card">
              <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed btn-pregunta text-left" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapseThree">
                    <img src="{{ asset('img/plus.png') }}">¿Debo pasar un examen médico antes de afiliarme?
                  </button>
                </h5>
              </div>
              <div id="collapse10" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                  Solo a partir de los 60 años deberás pasar un examen de ingreso que consta de los siguientes procedimientos:
                  <ul>
                    <li>Consulta de medicina interna (hombres) o ginecología (mujeres)</li>
                    <li>Glucosa.</li>
                    <li>Ecografía abdominal.</li>
                    <li>EKG + Prueba de esfuerzo</li>
                  </ul>
                </div>
              </div>
            </div> 
            <div class="card">
              <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed btn-pregunta text-left" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapseThree">
                    <img src="{{ asset('img/plus.png') }}">¿Están cubiertas las vacunas en el Plan de Salud?
                  </button>
                </h5>
              </div>
              <div id="collapse11" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                  Los planes de Sanitas Perú ofrecen un programa de inmunizaciones y vacunaciones como parte del
control del niño sano, incluyendo los refuerzos necesarios hasta los cuatro años, con 100% de cobertura.
                </div>
              </div>
            </div>  
        </div>
        <br><br>
        <!--<p class="text-center pb-5 mb-5">
          <div class="pb-5 mb-5">
            <button class="ml-auto mr-auto active text-white pt-2 pb-2 pl-4 pr-4">¿TIENES MAS PREGUNTAS?</button>
          </div>
        </p>-->
      </div>

    </div>
    
    
  
  </div>
</div>
@endsection