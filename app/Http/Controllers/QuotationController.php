<?php

namespace App\Http\Controllers;

use App\Quotation;
use App\Complement;
use App\Affiliation;
use Illuminate\Http\Request;
use App\Departament;
use App\Insured;
use App\Plan;
use App\Detail;
use App\Age;
use App\Family;
use App\Activity;
use App\Identity;
use App\Direction;
use App\Way;
use App\Mail\EmailConfirmation;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;

class QuotationController extends Controller
{
    public function index($client = null)
    {
        $ages = Age::where('active', '1')->get();
      //  dd($ages);
        $children = Insured::find(3)->ages->where('active', '1');
        $partners = Insured::find(2)->ages->where('active', '1');
        return view('chatbot', compact('ages', 'children', 'partners', 'client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quotation $quotation)
    {
      //  dd($request->all());
        if($request->unchecked != null)
        {
            $split = explode(',',$request->unchecked);

                foreach ($split as $key => $value) {
                    $result = explode('-',$value);
                    if($result[2] == 0)
                    {
                        $id = $result[1];
                        Family::find($id)-> delete();
                    }
                }

            $quotation->update([
            'total' => $request->total,
            'plan_id' => $request->plan_id,
            ]);

            $total = $quotation->total;
            $name = $quotation->name;
            $id = $quotation->id;


            return view('payinvoice', compact('quotation'));

        }else
        {
            $quotation->update([
            'total' => $request->total,
            'plan_id' => $request->plan_id,
            ]);

            $total = $quotation->total;
            $name = $quotation->name;
            $id = $quotation->id;

            return view('payinvoice', compact('quotation'));
        }

        // return redirect()->route('public.payinvoice', [$data, $name, $id])->with('info', 'Registro actualizado con éxito!');
    }

    public function response(Request $request)
    {
         //dd($request->all());
        $id = 0;
        $payperplan = array();
        $amount = array();
        $affiliate = 0;
        $realInsureds = array(1);
        $insureds = Insured::all();
        $client = $request->client;
        $plans = Plan::where('client_id', $client)->where('active', '1')->get();
        $departaments = Departament::where('active', '1')->get();
       // dd($plans);
        $identities = Identity::all();
        $directions = Direction::all();
        $ways = Way::all();
        $insured = array();

        // Personas
        if ($client == "1") {

            if ($request->gender == "1") {

                $affiliate++;

                array_push($insured, array(
                    'id' => $id,
                    'age_min' => intval($request->age), // $age->min,
                    'age_max' => intval($request->age), // $age->max,
                    'type' => 1,
                    'gender_id' => intval($request->gender),
                    'description' => 'Titular'
                ));

                if ($request->married == "1") {

                    $affiliate++;
                    $age_partner = explode("-", $request->input('age-partner'));

                    array_push($insured, array(
                        'id' => ++$id,
                        'age_min' => intval($age_partner[0]),
                        'age_max' => intval($age_partner[1]),
                        'type' => 2,
                        'gender_id' => 2,
                        'description' => 'Conyuge'
                    ));

                    // Hombre CASADO con HIJO(S)
                    if ($request->children == "1") {

                        foreach ($request->age_children as $key => $child) {

                            $affiliate++;

                            if ($child >= "18") {
                                $type = 4;
                            } else {
                                $type = 3;
                            }

                            $description = Insured::find($type)->name;

                            array_push($insured, array(
                                'id' => ++$id,
                                'age_min' => intval($child),
                                'age_max' => intval($child),
                                'type' => $type,
                                'gender_id' => intval($request->gender_children[$key]),
                                'description' => $description
                            ));
                        }
                        $result = array(1, 2 , 3);
                    // Hombre CASADO sin HIJO(S)
                    } else {
                        $result = array(1, 2);
                    }
                } else {
                    // Hombre SOLTERO con HIJO(S)
                    if ($request->children == "1") {

                        foreach ($request->age_children as $key => $child) {

                            $affiliate++;

                            if ($child >= "18") {
                                $type = 4;
                            } else {
                                $type = 3;
                            }

                            $description = Insured::find($type)->name;

                            array_push($insured, array(
                                'id' => ++$id,
                                'age_min' => intval($child),
                                'age_max' => intval($child),
                                'type' => $type,
                                'gender_id' => intval($request->gender_children[$key]),
                                'description' => $description
                            ));
                        }
                        $result = array(1, 3);
                    // Hombre SOLTERO sin HIJO(S)
                    } else {
                        $result = array(1);
                    }
                }
            } elseif ($request->gender == "2") {

                $affiliate++;

                array_push($insured, array(
                    'id' => $id++,
                    'age_min' => intval($request->age), // $age->min,
                    'age_max' => intval($request->age), // $age->max,
                    'type' => 1,
                    'gender_id' => intval($request->gender),
                    'description' => 'Titular'
                ));

                if ($request->married == "1") {

                    $affiliate++;
                    $age_partner = explode("-", $request->input('age-partner'));

                    array_push($insured, array(
                        'id' => $id++,
                        'age_min' => intval($age_partner[0]),
                        'age_max' => intval($age_partner[1]),
                        'type' => 2,
                        'gender_id' => 1,
                        'description' => 'Conyuge'
                    ));

                    if ($request->children == "1") {

                        foreach ($request->age_children as $key => $child) {

                            $affiliate++;

                            if ($child >= "18") {
                                $type = 4;
                            } else {
                                $type = 3;
                            }

                            $description = Insured::find($type)->name;

                            array_push($insured, array(
                                'id' => $id++,
                                'age_min' => intval($child),
                                'age_max' => intval($child),
                                'type' => $type,
                                'gender_id' => intval($request->gender_children[$key]),
                                'description' => $description
                            ));
                        }
                        $result = array(1, 2 , 3);
                    // Mujer CASADA sin HIJO(S)
                    } else {
                        $result = array(1, 2);
                    }
                } else {
                    // Mujer SOLTERA con HIJO(S)
                    if ($request->children == "1") {

                        foreach ($request->age_children as $key => $child) {

                            $affiliate++;

                            if ($child >= "18") {
                                $type = 4;
                            } else {
                                $type = 3;
                            }

                            $description = Insured::find($type)->name;

                            array_push($insured, array(
                                'id' => $id++,
                                'age_min' => intval($child),
                                'age_max' => intval($child),
                                'type' => $type,
                                'gender_id' => intval($request->gender_children[$key]),
                                'description' => $description
                            ));
                        }
                        $result = array(1, 3);
                    // Mujer SOLTERA sin HIJO(S)
                    } else {
                        $result = array(1);
                    }
                }
            } else {
            }

            $insured = collect($insured)->map(function($item, $key) use($plans, &$payperplan, $affiliate) {
                foreach ($plans as $plan) {
                    foreach ($plan->details as $pos => $detail) {
                        if ($affiliate >= $detail->min_affiliate && $affiliate <= $detail->max_affiliate) {
                            if ($item['age_min'] >= $detail->min_age && $item['age_max'] <= $detail->max_age)  {
                                if ($detail->min_age == "15" && $detail->max_age == "44")  {
                                    if ($detail->genders[0]->id == $item['gender_id']) {
                                        $item['price'] = $detail->price;
                                        $item['plan_id'] = $detail->plan_id;
                                        array_push($payperplan, $item);
                                        break;
                                    }
                                } elseif ($detail->min_age == "18" && $detail->max_age == "28") {
                                    if ($detail->genders[0]->id == $item['gender_id']) {
                                        $item['price'] = $detail->price;
                                        $item['plan_id'] = $detail->plan_id;
                                        array_push($payperplan, $item);
                                        break;
                                    }
                                } else {
                                    $item['price'] = $detail->price;
                                    $item['plan_id'] = $detail->plan_id;
                                    array_push($payperplan, $item);
                                    break;
                                }
                            }
                        }
                    }
                }
                return $item;
            })->all();

            // $payperplan = collect($payperplan);

            $data = Quotation::create([
                'name' => $request->name,
                'client_id' => $client,
                'children' => $request->children,
                'amount' => $request->range,
            ]);

            foreach ($insured as $key => $item) {
                Family::create([
                    'name' => ($item['type'] == 1) ? $request->name : null,
                    'gender_id' => $item['gender_id'],
                    'bod' => ($item['type'] == 1) ? $request->bod : null,
                    'age' => ($item['type'] == 1) ? $request->age : null,
                    'insured_id' => $item['type'],
                    'quotation_id' => $data->id,
                ]);
            }

            $payperplan = collect($payperplan)->map(function($item, $key) use($plans, $data, &$payperplan) {
                foreach ($data->families as $key => $family) {
                    if ($item['id'] == $key) {
                        $item['family_id'] = $family->id;
                    }
                }
                return $item;
            })->all();

            //dd($payperplan);

        // Empresas
        } elseif ($client == "2") {

            $affiliate = $request->qty_worker + $request->qty_dependent;
            $amount = $request->qty_contribute;

            $annualCredit = $amount * 0.25;
            $monthCredit = $annualCredit / 12;
            $affiliateCredit = $monthCredit / $affiliate;
            $igv = $affiliateCredit * 1.18;

            if ($affiliate > 5) {
                array_push($insured, array(
                    'age_min' => 0,
                    'age_max' => 99999,
                    'type' => null,
                    'gender_id' => null,
                    'description' => null
                ));
            } else {
                foreach ($request->age_worker as $key => $worker) {

                    array_push($insured, array(
                        'age_min' => intval($worker),
                        'age_max' => intval($worker),
                        'type' => null,
                        'gender_id' => intval($request->gender_worker[$key]),
                        'description' => null
                    ));
                }
            }

            // dd($insured);

            $insured = collect($insured)->map(function($item, $key) use($plans, &$payperplan, $affiliate, $igv) {
                foreach ($plans as $plan) {
                    foreach ($plan->details as $pos => $detail) {
                        if ($affiliate >= $detail->min_affiliate && $affiliate <= $detail->max_affiliate) {
                            if ($item['age_min'] >= $detail->min_age && $item['age_max'] <= $detail->max_age)  {
                                if ($detail->min_age == "15" && $detail->max_age == "44")  {
                                    if ($detail->genders[0]->id == $item['gender_id']) {
                                        $item['price'] = $detail->price - $igv;
                                        $item['additional_1'] = $detail->additional_1;
                                        $item['additional_2'] = $detail->additional_2;
                                        $item['additional_3'] = isset($detail->additional_3) ? $detail->additional_3 : 0;
                                        $item['plan_id'] = $detail->plan_id;
                                        $item['igv'] = $igv;
                                        array_push($payperplan, $item);
                                        break;
                                    }
                                } elseif ($detail->min_age == "18" && $detail->max_age == "28") {
                                    if ($detail->genders[0]->id == $item['gender_id']) {
                                        $item['price'] = $detail->price - $igv;
                                        $item['additional_1'] = $detail->additional_1;
                                        $item['additional_2'] = $detail->additional_2;
                                        $item['additional_3'] = isset($detail->additional_3) ? $detail->additional_3 : 0;
                                        $item['plan_id'] = $detail->plan_id;
                                        $item['igv'] = $igv;
                                        array_push($payperplan, $item);
                                        break;
                                    }
                                } else {
                                    $item['price'] = $detail->price - $igv;
                                    $item['additional_1'] = $detail->additional_1;
                                    $item['additional_2'] = $detail->additional_2;
                                    $item['additional_3'] = isset($detail->additional_3) ? $detail->additional_3 : 0;
                                    $item['plan_id'] = $detail->plan_id;
                                    $item['igv'] = $igv;
                                    array_push($payperplan, $item);
                                    break;
                                }
                            }
                        }
                    }
                }
                return $item;
            })->all();

            $payperplan = collect($payperplan);

            $data = Quotation::create([
                'name' => $request->name,
                'client_id' => $client,
                'worker' => $request->qty_worker,
                'dependent' => $request->qty_dependent,
                'amount' => $request->qty_contribute,
            ]);

            // Mail::to('dacosta@techpatagon.com')->send(new EmailConfirmation($data));

        } else {
        }
        // sctr
        if ($client == "4") {
            $data = Complement::create([
            'name' => $request->name,
            'client_id' => $request->client,
            'type' => $request->type,
            'identity_type' => $request->identity_type,
            'activity_id' => $request->activity_id,
            'search' => $request->search,
            'worker_number' => $request->worker_number,
            'monthly_salary' => $request->monthly_salary,
        ]);
        }
        $id = $data->id;
        //dd($plans);
        return view('plans.quotation', compact('name', 'plans', 'insureds', 'payperplan', 'id','family', 'client', 'igv','identities','departaments','ways','directions'));
    }
     //public function add(Request $request)
    public function add(Quotation $quotation)
    {
         //dd($request->all());
        $id = 0;
        $payperplan = array();
        $amount = array();
        $affiliate = 0;
        $realInsureds = array(1);
        $insureds = Insured::all();
        $client = 2;
        $id = 5;
        $name = $request->name;
        $plans = Plan::where('client_id', $client)->where('active', '0')->get();
       // dd($plans);
        $identities = Identity::all();
        $insured = array();
        $quotation = Quotation::all();

        // Personas
        if ($client == "1") {

            if ($request->gender == "1") {

                $affiliate++;

                array_push($insured, array(
                    'id' => $id,
                    'age_min' => intval($request->age), // $age->min,
                    'age_max' => intval($request->age), // $age->max,
                    'type' => 1,
                    'gender_id' => intval($request->gender),
                    'description' => 'Titular'
                ));

                if ($request->married == "1") {

                    $affiliate++;
                    $age_partner = explode("-", $request->input('age-partner'));

                    array_push($insured, array(
                        'id' => ++$id,
                        'age_min' => intval($age_partner[0]),
                        'age_max' => intval($age_partner[1]),
                        'type' => 2,
                        'gender_id' => 2,
                        'description' => 'Conyuge'
                    ));

                    // Hombre CASADO con HIJO(S)
                    if ($request->children == "1") {

                        foreach ($request->age_children as $key => $child) {

                            $affiliate++;

                            if ($child >= "18") {
                                $type = 4;
                            } else {
                                $type = 3;
                            }

                            $description = Insured::find($type)->name;

                            array_push($insured, array(
                                'id' => ++$id,
                                'age_min' => intval($child),
                                'age_max' => intval($child),
                                'type' => $type,
                                'gender_id' => intval($request->gender_children[$key]),
                                'description' => $description
                            ));
                        }
                        $result = array(1, 2 , 3);
                    // Hombre CASADO sin HIJO(S)
                    } else {
                        $result = array(1, 2);
                    }
                } else {
                    // Hombre SOLTERO con HIJO(S)
                    if ($request->children == "1") {

                        foreach ($request->age_children as $key => $child) {

                            $affiliate++;

                            if ($child >= "18") {
                                $type = 4;
                            } else {
                                $type = 3;
                            }

                            $description = Insured::find($type)->name;

                            array_push($insured, array(
                                'id' => ++$id,
                                'age_min' => intval($child),
                                'age_max' => intval($child),
                                'type' => $type,
                                'gender_id' => intval($request->gender_children[$key]),
                                'description' => $description
                            ));
                        }
                        $result = array(1, 3);
                    // Hombre SOLTERO sin HIJO(S)
                    } else {
                        $result = array(1);
                    }
                }
            } elseif ($request->gender == "2") {

                $affiliate++;

                array_push($insured, array(
                    'id' => $id++,
                    'age_min' => intval($request->age), // $age->min,
                    'age_max' => intval($request->age), // $age->max,
                    'type' => 1,
                    'gender_id' => intval($request->gender),
                    'description' => 'Titular'
                ));

                if ($request->married == "1") {

                    $affiliate++;
                    $age_partner = explode("-", $request->input('age-partner'));

                    array_push($insured, array(
                        'id' => $id++,
                        'age_min' => intval($age_partner[0]),
                        'age_max' => intval($age_partner[1]),
                        'type' => 2,
                        'gender_id' => 1,
                        'description' => 'Conyuge'
                    ));

                    if ($request->children == "1") {

                        foreach ($request->age_children as $key => $child) {

                            $affiliate++;

                            if ($child >= "18") {
                                $type = 4;
                            } else {
                                $type = 3;
                            }

                            $description = Insured::find($type)->name;

                            array_push($insured, array(
                                'id' => $id++,
                                'age_min' => intval($child),
                                'age_max' => intval($child),
                                'type' => $type,
                                'gender_id' => intval($request->gender_children[$key]),
                                'description' => $description
                            ));
                        }
                        $result = array(1, 2 , 3);
                    // Mujer CASADA sin HIJO(S)
                    } else {
                        $result = array(1, 2);
                    }
                } else {
                    // Mujer SOLTERA con HIJO(S)
                    if ($request->children == "1") {

                        foreach ($request->age_children as $key => $child) {

                            $affiliate++;

                            if ($child >= "18") {
                                $type = 4;
                            } else {
                                $type = 3;
                            }

                            $description = Insured::find($type)->name;

                            array_push($insured, array(
                                'id' => $id++,
                                'age_min' => intval($child),
                                'age_max' => intval($child),
                                'type' => $type,
                                'gender_id' => intval($request->gender_children[$key]),
                                'description' => $description
                            ));
                        }
                        $result = array(1, 3);
                    // Mujer SOLTERA sin HIJO(S)
                    } else {
                        $result = array(1);
                    }
                }
            } else {
            }

            $insured = collect($insured)->map(function($item, $key) use($plans, &$payperplan, $affiliate) {
                foreach ($plans as $plan) {
                    foreach ($plan->details as $pos => $detail) {
                        if ($affiliate >= $detail->min_affiliate && $affiliate <= $detail->max_affiliate) {
                            if ($item['age_min'] >= $detail->min_age && $item['age_max'] <= $detail->max_age)  {
                                if ($detail->min_age == "15" && $detail->max_age == "44")  {
                                    if ($detail->genders[0]->id == $item['gender_id']) {
                                        $item['price'] = $detail->price;
                                        $item['plan_id'] = $detail->plan_id;
                                        array_push($payperplan, $item);
                                        break;
                                    }
                                } elseif ($detail->min_age == "18" && $detail->max_age == "28") {
                                    if ($detail->genders[0]->id == $item['gender_id']) {
                                        $item['price'] = $detail->price;
                                        $item['plan_id'] = $detail->plan_id;
                                        array_push($payperplan, $item);
                                        break;
                                    }
                                } else {
                                    $item['price'] = $detail->price;
                                    $item['plan_id'] = $detail->plan_id;
                                    array_push($payperplan, $item);
                                    break;
                                }
                            }
                        }
                    }
                }
                return $item;
            })->all();

            // $payperplan = collect($payperplan);

            $data = Quotation::create([
                'name' => $request->name,
                'client_id' => $client,
                'children' => $request->children,
                'amount' => $request->range,
            ]);

            foreach ($insured as $key => $item) {
                Family::create([
                    'name' => ($item['type'] == 1) ? $request->name : null,
                    'gender_id' => $item['gender_id'],
                    'bod' => ($item['type'] == 1) ? $request->bod : null,
                    'age' => ($item['type'] == 1) ? $request->age : null,
                    'insured_id' => $item['type'],
                    'quotation_id' => $data->id,
                ]);
            }

            $payperplan = collect($payperplan)->map(function($item, $key) use($plans, $data, &$payperplan) {
                foreach ($data->families as $key => $family) {
                    if ($item['id'] == $key) {
                        $item['family_id'] = $family->id;
                    }
                }
                return $item;
            })->all();

            //dd($payperplan);

        // Empresas
        } elseif ($client == "2") {

            $affiliate = $request->qty_worker + $request->qty_dependent;
            $amount = $request->qty_contribute;

            $annualCredit = $amount * 0.25;
            $monthCredit = $annualCredit / 12;
            $affiliateCredit = $monthCredit / $affiliate;
            $igv = $affiliateCredit * 1.18;

            if ($affiliate > 5) {
                array_push($insured, array(
                    'age_min' => 0,
                    'age_max' => 99999,
                    'type' => null,
                    'gender_id' => null,
                    'description' => null
                ));
            } else {
              //  foreach ($request->age_worker as $key => $worker) {
                    $worker = $request->age_worker;
                    array_push($insured, array(
                        'age_min' => intval($worker),
                        'age_max' => intval($worker),
                        'type' => null,
                        'gender_id' => intval($request->gender_worker),
                        'description' => null
                    ));
                //}
            }

            // dd($insured);

            $insured = collect($insured)->map(function($item, $key) use($plans, &$payperplan, $affiliate, $igv) {
                foreach ($plans as $plan) {
                    foreach ($plan->details as $pos => $detail) {
                        if ($affiliate >= $detail->min_affiliate && $affiliate <= $detail->max_affiliate) {
                            if ($item['age_min'] >= $detail->min_age && $item['age_max'] <= $detail->max_age)  {
                                if ($detail->min_age == "15" && $detail->max_age == "44")  {
                                    if ($detail->genders[0]->id == $item['gender_id']) {
                                        $item['price'] = $detail->price - $igv;
                                        $item['additional_1'] = $detail->additional_1;
                                        $item['additional_2'] = $detail->additional_2;
                                        $item['additional_3'] = isset($detail->additional_3) ? $detail->additional_3 : 0;
                                        $item['plan_id'] = $detail->plan_id;
                                        $item['igv'] = $igv;
                                        array_push($payperplan, $item);
                                        break;
                                    }
                                } elseif ($detail->min_age == "18" && $detail->max_age == "28") {
                                    if ($detail->genders[0]->id == $item['gender_id']) {
                                        $item['price'] = $detail->price - $igv;
                                        $item['additional_1'] = $detail->additional_1;
                                        $item['additional_2'] = $detail->additional_2;
                                        $item['additional_3'] = isset($detail->additional_3) ? $detail->additional_3 : 0;
                                        $item['plan_id'] = $detail->plan_id;
                                        $item['igv'] = $igv;
                                        array_push($payperplan, $item);
                                        break;
                                    }
                                } else {
                                    $item['price'] = $detail->price - $igv;
                                    $item['additional_1'] = $detail->additional_1;
                                    $item['additional_2'] = $detail->additional_2;
                                    $item['additional_3'] = isset($detail->additional_3) ? $detail->additional_3 : 0;
                                    $item['plan_id'] = $detail->plan_id;
                                    $item['igv'] = $igv;
                                    array_push($payperplan, $item);
                                    break;
                                }
                            }
                        }
                    }
                }
                return $item;
            })->all();

            $payperplan = collect($payperplan);

            $data = Quotation::create([
                'name' => $request->name,
                'client_id' => $client,
                'worker' => $request->qty_worker,
                'dependent' => $request->qty_dependent,
                'amount' => $request->qty_contribute,
            ]);

            // Mail::to('dacosta@techpatagon.com')->send(new EmailConfirmation($data));

        } else {
        }

        $id = $data->id;
        //dd($plans);
       return view('public.plans.adds', compact('name', 'plans', 'insureds', 'payperplan', 'id','family', 'client', 'igv','identities'));
    }


    public function business(Quotation $quotation,$cod_dni)
    {
        $search = Family::where('quotation_id',$quotation->id)->where('cod_dni',$cod_dni)->get();
        //  dd($search);
      //  $familiess = Family::where('cod_dni',$cod_dni)->where('quotation_id',$quotation)->get();
     //   dd($familiess);
        //$affiliates = $quotation->families->count();
       $affiliates = $search->count();
      //  dd($affiliates);
        $amount = $quotation->amount;

        $annualCredit = $amount * 0.25;
        $monthCredit = $annualCredit / 12;
        $affiliateCredit = $monthCredit / $affiliates;
        $igv = $affiliateCredit * 1.18;

       $id = $quotation->plan_id;

        $plans = Plan::where('client_id',$quotation->client_id)->where('id',$id)->where('active', '1')->get();

        $payperplan = array();

        $insured = $quotation->families->map(function($item, $key) use($plans, &$payperplan, $affiliates, $igv) {
            foreach ($plans as $plan) {
                foreach ($plan->details as $pos => $detail) {
                    if ($affiliates >= $detail->min_affiliate && $affiliates <= $detail->max_affiliate) {

                        $itemAux = clone $item;

                        if ($itemAux['age'] >= $detail->min_age && $itemAux['age'] <= $detail->max_age) {
                            if ($detail->min_age == "15" && $detail->max_age == "44") {
                                if ($detail->genders[0]->id == $itemAux['gender_id']) {
                                    $itemAux['price'] = $detail->price - $igv;
                                    $itemAux['additional_1'] = $detail->additional_1 - $igv;
                                    $itemAux['additional_2'] = $detail->additional_2 - $igv;
                                    $itemAux['additional_3'] = isset($detail->additional_3) ? $detail->additional_3 - $igv : 0;
                                    $itemAux['plan_id'] = $detail->plan_id;
                                    $itemAux['plan_name'] = $plan->name;
                                    array_push($payperplan, $itemAux);
                                    break;
                                }
                            } elseif ($detail->min_age == "18" && $detail->max_age == "28") {
                                if ($detail->genders[0]->id == $item['gender_id']) {
                                    $itemAux['price'] = $detail->price - $igv;
                                    $itemAux['additional_1'] = $detail->additional_1 - $igv;
                                    $itemAux['additional_2'] = $detail->additional_2 - $igv;
                                    $itemAux['additional_3'] = isset($detail->additional_3) ? $detail->additional_3 - $igv : 0;
                                    $itemAux['plan_id'] = $detail->plan_id;
                                    $itemAux['plan_name'] = $plan->name;
                                    array_push($payperplan, $itemAux);
                                    break;
                                }
                            } else {
                                $itemAux['price'] = $detail->price - $igv;
                                $itemAux['additional_1'] = $detail->additional_1 - $igv;
                                $itemAux['additional_2'] = $detail->additional_2 - $igv;
                                $itemAux['additional_3'] = isset($detail->additional_3) ? $detail->additional_3 - $igv : 0;
                                $itemAux['plan_id'] = $detail->plan_id;
                                $itemAux['plan_name'] = $plan->name;
                                array_push($payperplan, $itemAux);
                                break;
                            }
                        }
                    }
                }
            }
            return $item;
        })->all();

        return view('public.quotation.business', compact('payperplan','plans', 'quotation', 'amount', 'annualCredit', 'monthCredit', 'affiliateCredit', 'igv'));
    }
}
