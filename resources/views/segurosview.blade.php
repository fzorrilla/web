@extends('layouts.mob')

@section('title', 'SOCORRO WEB')

@section('content')
<div style="background: #FFF;">
  <div style="background: #e2e2e252;">
    <div class="row">
      <div class="offset-md-1 col-md-10">
        <h2 class="title-section">SEGUROS </h2>
        <br>
        <hr class="borde-naranja">
      </div>
    </div>
  </div>
  <div class="container">  
    <div class="row">
      <div class="col-md-10 pt-3">
        <h2 style="color: #8ea1b2;font-family: Gotham-Bold;">El mejor Seguro</h2>
        <p style="color: #8ea1b2;font-size: 16px;">
          Elige tu seguro ideal para tu necesidad. Revisa las opciones y encuentra como proteger lo importante.
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 text-center seguros-one">
        
          
        
      </div>
      <div class="col-md-6 text-center pt-5">
        <div class=" text-center mt-5">
          <img src="{{ asset('img/plan_salud_independiente.png') }}">
        </div>
        <div class="pt-3">
          <h2>Salud Independiente</h2>
        </div> 
        <div>
          <button class="ml-auto mr-auto active" onclick="window.location.href='/seguroInd'">MÁS INFORMACION</button>
        </div>
      </div>

      <div class="col-md-6 text-center pt-5 ">
        <div class=" text-center">
          <img src="{{ asset('img/plan_salud_empresas.png') }}">
        </div>
        <div class="pt-3">
          <h2>Salud Empresarial</h2>
        </div>
        <div>
          <button class="ml-auto mr-auto active" onclick="window.location.href='/seguroEmp'">MÁS INFORMACION</button>
        </div>
      </div>
      <div class="col-md-6 text-center seguros-two ">
        
          
        
      </div>
      <div class="col-md-6 text-center seguros-three">
        
          
        
      </div>
      <div class="col-md-6 text-center pt-5">
        <div class=" text-center">
          <img src="{{ asset('img/plan_seguro_auto.png') }}">
        </div>
        <div class="pt-3">
          <h2 class="h2-planes">Aseguro mi vehiculo</h2>
        </div>
        <div>
          <button class="ml-auto mr-auto disabled">MUY PRONTO</button>
        </div>
      </div>

      <div class="col-md-6 text-center pt-5">
        <div class=" text-center">
          <img src="{{ asset('img/icon4.png') }}">
        </div>
        <div class="pt-3">
          <h2 class="h2-planes-riesgo">Aseguro en caso de riesgos de trabajo</h2>
        </div>
        <div>
          <button class="ml-auto mr-auto disabled">MUY PRONTO</button>
        </div>
      </div>
      <div class="col-md-6 text-center seguros-four">
        
          
        
      </div>
      
    </div>
    <div class="row">
      <div class="col-md-10 pt-5">
        <h2 style="color: #8ea1b2;font-family: Gotham-Bold;">Cotiza, elige tu mejor plan y contrata en línea</h2>
        
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 pt-5 text-center">
        <img src="{{ asset('img/iconseguros3.png') }}">
        <div class="row mt-2">
          <div class="col-md-12 pt-3" style="color: #8ea1b2;font-size: 22px;">
            El seguro de su necesidad desde su casa
          </div>
        </div>
      </div>
      <div class="col-md-4 pt-5 text-center">
        <img src="{{ asset('img/iconseguros1.png') }}">
        <div class="row">
          <div class="col-md-12" style="color: #8ea1b2;font-size: 22px;">
            Pague en línea y controle su proteccion
          </div>
        </div>
      </div>
      <div class="col-md-4 pt-5 text-center">
        <img src="{{ asset('img/iconseguros2.png') }}">
        <div class="row">
          <div class="col-md-12 pt-2" style="color: #8ea1b2;font-size: 22px;">
            Cobertura inmediata a pocos pasos
          </div>
        </div>
      </div>
    </div>
    
  
  </div>
</div>
<br><br>
@endsection