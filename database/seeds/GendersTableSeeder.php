<?php

use Illuminate\Database\Seeder;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genders = [
            ['name' => 'Masculino', 'abbreviation' => 'M'],
            ['name' => 'Femenino', 'abbreviation' => 'F']
        ];

        foreach ($genders as $gender) {
            App\Gender::create($gender);      
        }
    }
}
