<?php

use Illuminate\Database\Seeder;

class IdentityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $identities = [
            ['dstipoiden' => 'DNI'],
            ['dstipoiden' => 'Carnet Extranjeria'],
            ['dstipoiden' => 'Pasaporte'],
        ];

        foreach ($identities as $identity) {
            App\Identity::create($identity);
        }
    }
}
