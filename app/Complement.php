<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complement extends Model
{
   protected $fillable = [
    	'name',
        'client_id',
        'identity_type',
        'activity_id',
        'district_id',
        'worker_number',
        'monthly_salary',
        'total',
        'plan_id',
    ];

    public function schedule()
    {
        return $this->hasOne(Schedule::class);
    }
    public function visits()
    {
        return $this->hasOne(Visit::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function families()
    {
        return $this->hasMany(Family::class);
    }
}
