<?php

use Illuminate\Database\Seeder;

class TurnsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $turns = [
            [
                'name' => '09:00 am - 11:00 am',
                'start' => '09:00',
                'end' => '11:00',
                'limit' => '5',
            ], [
                'name' => '11:00 am - 01:00 pm',
                'start' => '11:00',
                'end' => '01:00',
                'limit' => '5',
            ], [
                'name' => '01:00 pm - 03:00 pm',
                'start' => '01:00',
                'end' => '03:00',
                'limit' => '5',
            ], [
                'name' => '03:00 pm - 05:00 pm',
                'start' => '03:00',
                'end' => '05:00',
                'limit' => '5',
            ]
        ];

        foreach ($turns as $turn) {
            App\Turn::create($turn);
        }
    }
}
