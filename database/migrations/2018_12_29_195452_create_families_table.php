<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('identity_id')->nullable();
            $table->string('cod_dni')->nullable();
            $table->string('dni')->nullable();
            $table->string('name')->nullable();
            $table->string('name2')->nullable();
            $table->string('last_name_1')->nullable();
            $table->string('last_name_2')->nullable();
            $table->unsignedInteger('gender_id')->nullable();
            $table->string('bod')->nullable();
            $table->string('age')->nullable();
            $table->unsignedInteger('nationality_id')->nullable();
            $table->unsignedInteger('civil_id')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->unsignedInteger('direction_id')->nullable();
            $table->unsignedInteger('way_id')->nullable();
            $table->string('address')->nullable();
            $table->unsignedInteger('district_id')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('insured_id')->nullable();
            $table->enum('treatment', ['0', '1'])->default('0');
            $table->enum('disability', ['0', '1'])->default('0');
            $table->enum('weightloss', ['0', '1'])->default('0');
            $table->enum('emergency', ['0', '1'])->default('0');
            $table->enum('drug', ['0', '1'])->default('0');
            $table->enum('pregnant', ['0', '1'])->nullable();
            $table->string('months')->nullable();
            $table->float('weigh')->nullable();
            $table->string('size')->nullable();
            $table->string('insurance')->nullable();
            $table->string('typo')->nullable();
            $table->string('insurer')->nullable();
            $table->string('outre')->nullable();
            $table->string('bod1')->nullable();
            $table->string('bod2')->nullable();
            $table->enum('completed', ['1', '0'])->default('0');
            $table->unsignedInteger('quotation_id');
            $table->softDeletes(); //Nueva línea, para el borrado lógico
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
