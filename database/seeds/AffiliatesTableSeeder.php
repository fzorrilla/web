<?php

use Illuminate\Database\Seeder;

class AffiliatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$affiliates = [
            [
                'name' => '1-9',
                'min' => '1',
                'max' => '9',
                'description' => 'De 1 a 9 Afiliados'
            ], [
                'name' => '10-99999',
                'min' => '10',
                'max' => '99999',
                'description' => 'Más de 10 Afiliados'
            ], [
                'name' => '1-99999',
                'min' => '1',
                'max' => '99999',
                'description' => 'Más de 1 Afiliado'
            ], [
                'name' => '1-5',
                'min' => '1',
                'max' => '5',
                'description' => 'De 1 a 5 Afiliados'
            ], [
                'name' => '6-9',
                'min' => '6',
                'max' => '9',
                'description' => 'De 6 a 9 Afiliados'
            ]
        ];

        foreach ($affiliates as $affiliate) {
        	App\Affiliate::create($affiliate);      
        }
    }
}
