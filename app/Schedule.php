<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
	protected $fillable = [
    	'district_id',
    	'address',
    	'square',
    	'block',
    	'reference',
    	'phone',
    	'email',
    	'date',
        'turn_id',
    	'quotation_id',
        'validity',
    ];

    public function quotation()
    {
        return $this->belongsTo(Quotation::class);
    }

    public function turn()
    {
        return $this->belongsTo(Turn::class);
    }
}
