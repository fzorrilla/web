<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function plans()
    {
    	return $this->hasMany(Plan::class);
    }

    public function quotations()
    {
    	return $this->hasMany(Quotation::class);
    }    
}
