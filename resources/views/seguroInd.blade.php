@extends('layouts.mob')

@section('title', 'SOCORRO WEB')

@section('content')
<div style="background: #FFF;" class="pb-5">
  <div style="background: #e2e2e252;">
    <div class="row">
      <div class="offset-md-1 col-md-10">
        <h2 class="title-section">SEGUROS </h2>
        <br>
        <hr class="borde-naranja">
      </div>
    </div>
  </div>
  <div class="container">  
    <div class="row">
      <div class="col-md-12 text-center mt-5">
        <img src="{{ asset('img/saludIndependiente.png') }}">
      </div>
      <div class="col-md-12 text-center mt-3">
        <h2 style="color: #8ea1b2;font-family: Gotham-Bold;text-transform: none;font-size: 42px;">
          Salud Individual
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
        <img src="{{ asset('img/SeguroIndependiente.png') }}">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center mt-5">
        <p style="color: #8ea1b2;font-size: 20px;font-weight: 500">
          Un seguro para disfrutar de tu vida. Y asegurar un futuro mas seguro.<br>Elige el plan a tu medida.
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-2 text-center mt-2">

      </div>
      <div class="title-height-first-persona col-md-2 text-center mt-5 border-left border-top border-bottom background-1">
        Esencial
      </div>
      <div class="title-height-first-persona col-md-2 text-center mt-5 border-left border-top border-bottom  background-2">
        Solter@
      </div>
      <div class="title-height-first-persona col-md-2 text-center mt-5 border-left border-top border-bottom  background-3">
        VIP
      </div>
      <div class="title-height-first-persona col-md-2 text-center mt-5 border-left border-top border-bottom  background-4">
        VIP INTERNACIONAL
      </div>
      <div class="title-height-first-persona col-md-2 text-center mt-5 border-left border-top border-bottom  background-5">
        Élite Iternacional
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Cobertura Obligatoria
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Cobertura Complementaria
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Chequeo Preventivo Anuela
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Cobertura Odontología
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Clínicas Disponibles
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        30
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        60
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        70
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        80
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        100
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Emergencias
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        100%
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Maternidad
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        10%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        100%
      </div>
      
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Medicinas (hasta)
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        70%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        100%
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Cobertura Oncológica
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Cobertura Iternacional
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Asistencia de Viaje
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-right border-top border-bottom border-left border-right title-height-persona">
        Precio de Plan Mensual
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        <b>Desde 50</b>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        <b>Desde 109</b>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        <b>Desde 121</b>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <b>Desde 182</b>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <b>Desde 321</b>
      </div>
      <div class="col-md-2 background-4 text-right border-top border-bottom border-left border-right title-height-persona">
        ELIGE TU PLAN
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/1'">
          COTIZA
        </button>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/1'">
          COTIZA
        </button>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/1'">
          COTIZA
        </button>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/1'">
          COTIZA
        </button>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/1'">
          COTIZA
        </button>
      </div>
    </div>
    <div class="row">
      <div class="content-colet col-md-6 text-left">
         <span style="display: inline;">Lista de clínicas según el plan: </span><span class="clinic">Clínicas autorizadas</span>
      </div>
      
    </div>

  </div>
</div>
<div class="pt-5" style="background: #e2e2e252;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 pb-5">
        <h2 class="title-section" style="margin-right: 80px;">
          PREGUNTAS QUE SIEMPRE ME HACEN
        </h2>
        <br>
        <hr class="borde-naranja">
      </div>
    </div>
    
    <p class="pt-4"></p>
    <div class="row">
      <div class="col-md-12">
        <div id="accordion" class="faq-socorro">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link btn-pregunta" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <img src="{{ asset('img/plus.png') }}">¿Qué es un EPS?
                </button>
              </h5>
            </div>

            <div id="collapseOne" class="collapse text-left" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                  Un EPS es una Entidad Prestadora de Salud y se encarga de administrar tu plan de salud y ofrecerte servicios médicos a través de las clínicas y otras instituciones de salud afiliadas.
                </div>
              </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link btn-pregunta collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <img src="{{ asset('img/plus.png') }}">¿Qué es un Plan de Salud?
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                Un Plan de Salud es un convenio que se tiene entre la EPS y sus usuarios (ustedes) donde se especifica las coberturas, exclusiones, copagos y red de clínicas disponibles a los usuarios (ustedes).
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed btn-pregunta" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  <img src="{{ asset('img/plus.png') }}">¿Existe límite de edad para afiliarme a un Plan de Salud?
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                No, la edad no es una limitación.
              </div>
            </div>
          </div>
        </div>
        <p class="text-center pb-5 mb-5">
          <div class="pb-5 mb-5">
            <button class="ml-auto mr-auto active text-white pt-2 pb-2 pl-4 pr-4" onclick="window.location.href='/faq'">¿TIENES MAS PREGUNTAS?</button>
          </div>
        </p>
      </div>

    </div>
    
    
  
  </div>
</div>
<div style="background: #FFF;" class="pt-5 mt-5">
  <div class="container mt-3 mb-5 pb-5">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title-section">CONOCE MAS SOBRE</h2>
        <br>
        <hr class="borde-naranja">
      </div>
    </div>
    <div class="row">
        <div class="col-md-6 text-center seguros-one segurosoneHover">
            <div id="overlayone"></div>
            <div class=" text-center"  style="position: relative;z-index: 3">
              <img src="{{ asset('img/icon1.png') }}">
            </div>
            <div class="pt-3" style="position: relative;z-index: 3">
              <h2>Salud Independiente</h2>
            </div>
            <div>
              <button class="ml-auto mr-auto active" onclick="window.location.href='/seguroInd'">MÁS INFORMACION</button>
            </div>
          
        </div>
        <div class="col-md-6 text-center seguros-two">
            <div id="overlaytwo"></div>
            <div class=" text-center" style="position: relative;z-index: 3">
              <img src="{{ asset('img/icon2.png') }}">
            </div>
            <div class="pt-3" style="position: relative;z-index: 3">
              <h2>Salud Empresarial</h2>
            </div>
            <div>
              <button class="ml-auto mr-auto active" onclick="window.location.href='/seguroEmp'">MÁS INFORMACION</button>
            </div>
        </div>
    </div>
  </div>
  
</div>
<script type="text/javascript">
  $(".seguros-one").hover(function(){
      $("#overlayone").show("slow");
  },function(){
      $("#overlayone").hide("slow");
  });
  $(".seguros-two").hover(function(){
      $("#overlaytwo").show("slow");
  },function(){
      $("#overlaytwo").hide("slow");
  });
  $(".seguros-three").hover(function(){
      $("#overlaythree").show("slow");
  },function(){
      $("#overlaythree").hide("slow");
  });
  $(".seguros-four").hover(function(){
      $("#overlayfour").show("slow");
  },function(){
      $("#overlayfour").hide("slow");
  });
  

</script>
@endsection