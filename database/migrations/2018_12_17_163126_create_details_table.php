<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->increments('id');
            $table->float('price');
            $table->float('additional_1')->nullable();
            $table->float('additional_2')->nullable();
            $table->float('additional_3')->nullable();
            $table->unsignedInteger('min_affiliate');
            $table->unsignedInteger('max_affiliate');
            $table->unsignedInteger('min_age');
            $table->unsignedInteger('max_age');
            $table->unsignedInteger('age_id');
            $table->unsignedInteger('plan_id');
            // $table->unsignedInteger('gender_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
