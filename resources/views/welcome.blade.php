@extends('layouts.mob')

@section('title', 'SOCORRO WEB')

@section('content')

	@include('slider')

	@include('fourStep')

	@include('seguros')

	@include('socorro')

	@include('faq')
	
	

@endsection