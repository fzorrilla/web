<div class="four-step pt-5">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2 class="title-section">¿Cómo Funciona?</h2>
				<hr class="borde-naranja">
			</div>
		</div>
		<div class="row content-step">
              <div class="col-md-2 text-center back-step">
                <span class="content-step-p">
                  <p class="number-step">01</p>
                  <p class="">Cuéntame</p>
                  <p class="">que necesitas</p> 
                </span>
                <img src="{{ asset('img/four-4.png') }}" alt="First slide">
              </div>
              <div class="col-md-1 text-center content-step-arrow">
                <img src="{{ asset('img/arrow.png') }}" alt="First slide">
              </div>
              <div class="col-md-2 text-center back-step">
                <span class="content-step-p">
                  <p class="number-step">02</p>
                  <p class="">Compara</p>
                  <p class="">propuestas</p> 
                </span>
                <img src="{{ asset('img/four-1.png') }}" alt="First slide">
                
              </div>
              <div class="col-md-1 text-center content-step-arrow">
                <img src="{{ asset('img/arrow.png') }}" alt="First slide">
              </div>
              <div class="col-md-2 text-center back-step">
                <span class="content-step-p">
                  <p class="number-step">03</p>
                  <p class="">Paga</p>
                  <p class="">en línea</p> 
                </span>
                <img src="{{ asset('img/four-2.png') }}" alt="First slide">
              </div>
              <div class="col-md-1 text-center content-step-arrow">
                <img src="{{ asset('img/arrow.png') }}" alt="First slide">
              </div>
              <div class="col-md-2 text-center back-step">
                <span class="content-step-p">
                  <p class="number-step">04</p>
                  <p class="">Recibe</p>
                  <p class="">tu poliza</p> 
                </span>
                <img src="{{ asset('img/four-3.png') }}" alt="First slide">
              </div>
            </div>
	</div>
</div>
