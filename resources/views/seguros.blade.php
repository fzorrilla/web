<section id="" class="">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="title-section" style="margin-right: 170px;">SEGUROS </h2>
        <hr class="borde-naranja">
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 text-center seguros-one segurosoneHover">
          <div id="overlayone"></div>
          <div class=" text-center"  style="position: relative;z-index: 3">
            <img src="{{ asset('img/icon1.png') }}">
          </div>
          <div class="pt-3" style="position: relative;z-index: 3">
            <h2>Salud Independiente</h2>
          </div>
          <div>
            <button class="ml-auto mr-auto active" onclick="window.location.href='/seguroInd'">MÁS INFORMACION</button>
          </div>
        
      </div>
      <div class="col-md-6 text-center seguros-two">
          <div id="overlaytwo"></div>
          <div class=" text-center" style="position: relative;z-index: 3">
            <img src="{{ asset('img/icon2.png') }}">
          </div>
          <div class="pt-3" style="position: relative;z-index: 3">
            <h2>Salud Empresarial</h2>
          </div>
          <div>
            <button class="ml-auto mr-auto active" onclick="window.location.href='/seguroEmp'">MÁS INFORMACION</button>
          </div>
      </div>
      <div class="col-md-6 text-center seguros-three">
        <div id="overlaythree"></div>
        <div class=" text-center" style="position: relative;z-index: 3">
          <img src="{{ asset('img/icon3.png') }}">
        </div>
        <div class="pt-3" style="position: relative;z-index: 3">
          <h2 class="h2-planes">Aseguro mi vehiculo</h2>
        </div>
        <div style="position: relative;z-index: 3">
          <button class="ml-auto mr-auto disabled">MUY PRONTO</button>
        </div>
      </div>
      <div class="col-md-6 text-center seguros-four">
        <div id="overlayfour"></div>
        <div class=" text-center" style="position: relative;z-index: 3">
          <img src="{{ asset('img/icon4.png') }}">
        </div>
        <div class="pt-3" style="position: relative;z-index: 3">
          <h2 class="h2-planes-riesgo">Aseguro en caso de riesgos de trabajo</h2>
        </div>
        <div style="position: relative;z-index: 3">
          <button class="ml-auto mr-auto disabled">MUY PRONTO</button>
        </div>
      </div>
    </div>
    <!--<div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="mu-latest-courses-area">
          
          <div id="mu-latest-course-slide" class="mu-latest-courses-content">
            <div class="col-lg-4 col-md-4 col-xs-12 text-center seguro-vida">
              <div class=" text-center">
              	<img src="{{ asset('img/plan_salud_independiente.png') }}">
              </div>
              <div class="pt-5">
              	<h2>SEGURO DE MI VIDA</h2>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-12 text-center seguro-empresa">
              <div class=" text-center">
              	<img src="{{ asset('img/plan_salud_empresas.png') }}">
              </div>
              <div class="pt-5">
              	<h2>SEGURO DE EMPRESA</h2>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-12 text-center seguro-riesgo">
              <div class=" text-center">
                <img src="{{ asset('img/plan_salud_riesgo.png') }}">
              </div>
              <div class="pt-5">
                <h2>SEGURO DE RIESGO</h2>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-12 text-center seguro-auto">
              <div class=" text-center">
              	<img src="{{ asset('img/plan_seguro_auto.png') }}">
              </div>
              <div class="pt-5">
              	<h2>SEGURO DE AUTO</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>-->
  </div>
</section>
<script type="text/javascript">
  $(".seguros-one").hover(function(){
      $("#overlayone").show("slow");
  },function(){
      $("#overlayone").hide("slow");
  });
  $(".seguros-two").hover(function(){
      $("#overlaytwo").show("slow");
  },function(){
      $("#overlaytwo").hide("slow");
  });
  $(".seguros-three").hover(function(){
      $("#overlaythree").show("slow");
  },function(){
      $("#overlaythree").hide("slow");
  });
  $(".seguros-four").hover(function(){
      $("#overlayfour").show("slow");
  },function(){
      $("#overlayfour").hide("slow");
  });
  

</script>