<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preexistence extends Model
{
    // public function details()
    // {
    // 	return $this->hasMany(Detail::class);
    // }
   protected $fillable = [
        'pathologies_id',
        'families_id',
        'disease_id',
        'end',
        'doctor',
        'year',
        'situation',
    ];

   /* public function pathology()
    {
        return $this->belongsToMany(Pathology::class);
    }

    public function families()
    {
    	return $this->belongsToMany(Family::class);
    }*/
}
