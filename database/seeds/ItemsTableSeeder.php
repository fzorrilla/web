<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['description' => 'Cobertura sin limite'],
            ['description' => 'Cobertura obligatoria sin límite'],
            ['description' => 'Cobertura de maternidad al 100%'],
            ['description' => 'Oncología al 100%'],
            ['description' => 'Médico a domicilio'],
            ['description' => 'Odontología'],
            ['description' => 'Oftalmología'],
            ['description' => 'Programa preventivo'],
            ['description' => 'Programa de paciente crónico'],
            ['description' => 'Vacunas a domicilio'],
            ['description' => 'Atención ambulatoria y derivada en Colombia'],
            ['description' => 'Asistencia de viajero'],
            ['description' => 'Copagos fijos sin deducibles'],
            ['description' => 'Control del niño sano hasta los 11 años'],
            ['description' => 'Cobertura de medicamentos genéricos al 100% y a domicilio sin costo'],
            ['description' => 'Atenciones ambulatorias y hospitalarias derivadas de emergencias en Colombia'],
            ['description' => 'Assist Card para emergencias en cualquier parte del mundo'],
        ];

        foreach ($items as $item) {
            App\Item::create($item);
        }

        $data = [
            ['item_id' => 1, 'plan_id' => 1],
            ['item_id' => 2, 'plan_id' => 1],
            ['item_id' => 3, 'plan_id' => 1],
            ['item_id' => 4, 'plan_id' => 1],
            ['item_id' => 5, 'plan_id' => 1],
            ['item_id' => 6, 'plan_id' => 1],
            ['item_id' => 7, 'plan_id' => 1],
            ['item_id' => 8, 'plan_id' => 1],
            ['item_id' => 9, 'plan_id' => 1],
            ['item_id' => 10, 'plan_id' => 1],
            ['item_id' => 11, 'plan_id' => 1],
            ['item_id' => 12, 'plan_id' => 1],
            ['item_id' => 13, 'plan_id' => 1],
            ['item_id' => 14, 'plan_id' => 1],
            ['item_id' => 15, 'plan_id' => 1],
            ['item_id' => 1, 'plan_id' => 2],
            ['item_id' => 2, 'plan_id' => 2],
            ['item_id' => 3, 'plan_id' => 2],
            ['item_id' => 4, 'plan_id' => 2],
            ['item_id' => 5, 'plan_id' => 2],
            ['item_id' => 6, 'plan_id' => 2],
            ['item_id' => 7, 'plan_id' => 2],
            ['item_id' => 8, 'plan_id' => 2],
            ['item_id' => 9, 'plan_id' => 2],
            ['item_id' => 10, 'plan_id' => 2],
            ['item_id' => 13, 'plan_id' => 2],
            ['item_id' => 14, 'plan_id' => 2],
            ['item_id' => 15, 'plan_id' => 2],
            ['item_id' => 16, 'plan_id' => 2],
            ['item_id' => 17, 'plan_id' => 2],
            ['item_id' => 1, 'plan_id' => 3],
            ['item_id' => 2, 'plan_id' => 3],
            ['item_id' => 3, 'plan_id' => 3],
            ['item_id' => 4, 'plan_id' => 3],
            ['item_id' => 5, 'plan_id' => 3],
            ['item_id' => 6, 'plan_id' => 3],
            ['item_id' => 7, 'plan_id' => 3],
            ['item_id' => 8, 'plan_id' => 3],
            ['item_id' => 9, 'plan_id' => 3],
            ['item_id' => 10, 'plan_id' =>3],
            ['item_id' => 11, 'plan_id' => 3],
            ['item_id' => 12, 'plan_id' => 3],
            ['item_id' => 13, 'plan_id' => 3],
            ['item_id' => 14, 'plan_id' => 3],
            ['item_id' => 15, 'plan_id' => 3],
            ['item_id' => 1, 'plan_id' => 4],
            ['item_id' => 2, 'plan_id' => 4],
            ['item_id' => 3, 'plan_id' => 4],
            ['item_id' => 4, 'plan_id' => 4],
            ['item_id' => 5, 'plan_id' => 4],
            ['item_id' => 6, 'plan_id' => 4],
            ['item_id' => 7, 'plan_id' => 4],
            ['item_id' => 8, 'plan_id' => 4],
            ['item_id' => 9, 'plan_id' => 4],
            ['item_id' => 10, 'plan_id' => 4],
            ['item_id' => 13, 'plan_id' => 4],
            ['item_id' => 14, 'plan_id' => 4],
            ['item_id' => 15, 'plan_id' => 4],
            ['item_id' => 16, 'plan_id' => 4],
            ['item_id' => 17, 'plan_id' => 4],
            ['item_id' => 1,  'plan_id' => 5],
            ['item_id' => 3,  'plan_id' => 5],
            ['item_id' => 4,  'plan_id' => 5],
            ['item_id' => 5,  'plan_id' => 5],
            ['item_id' => 6,  'plan_id' => 5],
            ['item_id' => 7,  'plan_id' => 5],
            ['item_id' => 8,  'plan_id' => 5],
            ['item_id' => 9,  'plan_id' => 5],
            ['item_id' => 10,  'plan_id' => 5],
            ['item_id' => 11,  'plan_id' => 5],
            ['item_id' => 12,  'plan_id' => 5],
            ['item_id' => 13,  'plan_id' => 5],
            ['item_id' => 14,  'plan_id' => 5],
            ['item_id' => 15,  'plan_id' => 5],
            ['item_id' => 16,  'plan_id' => 5],
            ['item_id' => 17,  'plan_id' => 5],
            ['item_id' => 1,  'plan_id' => 6],
            ['item_id' => 2,  'plan_id' => 6],
            ['item_id' => 3,  'plan_id' => 6],
            ['item_id' => 4,  'plan_id' => 6],
            ['item_id' => 5,  'plan_id' => 6],
            ['item_id' => 6,  'plan_id' => 6],
            ['item_id' => 7,  'plan_id' => 6],
            ['item_id' => 8,  'plan_id' => 6],
            ['item_id' => 9,  'plan_id' => 6],
            ['item_id' => 10,  'plan_id' => 6],
            ['item_id' => 13,  'plan_id' => 6],
            ['item_id' => 14,  'plan_id' => 6],
            ['item_id' => 15,  'plan_id' => 6],
        ];

        DB::table('item_plan')->insert($data);
    }
}
