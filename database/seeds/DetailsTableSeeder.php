<?php

use Illuminate\Database\Seeder;

class DetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $details = [
            [ // MediSanitas 1 - 9
                'price' => '317',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 0,
                'max_age' => 9,
                'plan_id' => 1,
                'age_id' => 1,
                // 'gender_id' => null
            ], [
                'price' => '182',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 10,
                'max_age' => 14,
                'plan_id' => 1,
                'age_id' => 2,
                // 'gender_id' => null
            ], [
                'price' => '217',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 1,
                'age_id' => 4,
                // 'gender_id' => 1
            ], [
                'price' => '419',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 1,
                'age_id' => 4,
                // 'gender_id' => 2
            ], [
                'price' => '680',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 45,
                'max_age' => 59,
                'plan_id' => 1,
                'age_id' => 5,
                // 'gender_id' => null
            ], [
                'price' => '1540',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 60,
                'max_age' => 64,
                'plan_id' => 1,
                'age_id' => 6,
                // 'gender_id' => null
            ], [
                'price' => '1899',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 65,
                'max_age' => 99999,
                'plan_id' => 1,
                'age_id' => 8,
                // 'gender_id' => null
            ], [ // MediSanitas 10 -99999
                'price' => '296',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 0,
                'max_age' => 9,
                'plan_id' => 1,
                'age_id' => 1,
                // 'gender_id' => null
            ], [
                'price' => '170',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 10,
                'max_age' => 14,
                'plan_id' => 1,
                'age_id' => 2,
                // 'gender_id' => null
            ], [
                'price' => '205',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 1,
                'age_id' => 4,
                // 'gender_id' => 1
            ], [
                'price' => '392',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 1,
                'age_id' => 4,
                // 'gender_id' => 2
            ], [
                'price' => '635',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 45,
                'max_age' => 59,
                'plan_id' => 1,
                'age_id' => 5,
                // 'gender_id' => null
            ], [
                'price' => '1439',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 60,
                'max_age' => 64,
                'plan_id' => 1,
                'age_id' => 6,
                // 'gender_id' => null
            ], [
                'price' => '1772',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 65,
                'max_age' => 99999,
                'plan_id' => 1,
                'age_id' => 8,
                // 'gender_id' => null
            ], [ // Sanitas 1 - 9
                'price' => '212',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 0,
                'max_age' => 9,
                'plan_id' => 2,
                'age_id' => 1,
                // 'gender_id' => null
            ], [
                'price' => '121',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 10,
                'max_age' => 14,
                'plan_id' => 2,
                'age_id' => 2,
                // 'gender_id' => null
            ], [
                'price' => '136',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 2,
                'age_id' => 4,
                // 'gender_id' => 1
            ], [
                'price' => '288',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 2,
                'age_id' => 4,
                // 'gender_id' => 2
            ], [
                'price' => '491',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 45,
                'max_age' => 59,
                'plan_id' => 2,
                'age_id' => 5,
                // 'gender_id' => null
            ], [
                'price' => '1109',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 60,
                'max_age' => 64,
                'plan_id' => 2,
                'age_id' => 6,
                // 'gender_id' => null
            ], [
                'price' => '1368',
                'min_affiliate' => 1,
                'max_affiliate' => 9,
                'min_age' => 65,
                'max_age' => 99999,
                'plan_id' => 2,
                'age_id' => 8,
                // 'gender_id' => null
            ], [ // Sanitas 10 - 99999
                'price' => '198',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 0,
                'max_age' => 9,
                'plan_id' => 2,
                'age_id' => 1,
                // 'gender_id' => null
            ], [
                'price' => '112',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 10,
                'max_age' => 14,
                'plan_id' => 2,
                'age_id' => 2,
                // 'gender_id' => null
            ], [
                'price' => '128',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 2,
                'age_id' => 4,
                // 'gender_id' => null
            ], [
                'price' => '270',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 2,
                'age_id' => 4,
                // 'gender_id' => null
            ], [
                'price' => '458',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 45,
                'max_age' => 59,
                'plan_id' => 2,
                'age_id' => 5,
                // 'gender_id' => null
            ], [
                'price' => '1035',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 60,
                'max_age' => 64,
                'plan_id' => 2,
                'age_id' => 6,
                // 'gender_id' => null
            ], [
                'price' => '1277',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 65,
                'max_age' => 99999,
                'plan_id' => 2,
                'age_id' => 8,
                // 'gender_id' => null
            ], [ // Integral 1 - 99999
                'price' => '397',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 0,
                'max_age' => 9,
                'plan_id' => 3,
                'age_id' => 1,
                // 'gender_id' => null
            ], [
                'price' => '321',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 10,
                'max_age' => 14,
                'plan_id' => 3,
                'age_id' => 2,
                // 'gender_id' => null
            ], [
                'price' => '395',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 3,
                'age_id' => 4,
                // 'gender_id' => 1
            ], [
                'price' => '596',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 3,
                'age_id' => 4,
                // 'gender_id' => 2
            ], [
                'price' => '968',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 45,
                'max_age' => 59,
                'plan_id' => 3,
                'age_id' => 5,
                // 'gender_id' => null
            ], [
                'price' => '1542',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 60,
                'max_age' => 64,
                'plan_id' => 3,
                'age_id' => 6,
                // 'gender_id' => null
            ], [
                'price' => '1901',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 65,
                'max_age' => 99999,
                'plan_id' => 3,
                'age_id' => 8,
                // 'gender_id' => null
            ], [ // PEAS
                'price' => '81',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 0,
                'max_age' => 9,
                'plan_id' => 4,
                'age_id' => 1,
                // 'gender_id' => null
            ], [
                'price' => '50',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 10,
                'max_age' => 14,
                'plan_id' => 4,
                'age_id' => 2,
                // 'gender_id' => null
            ], [
                'price' => '65',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 4,
                'age_id' => 4,
                // 'gender_id' => 1
            ], [
                'price' => '101',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 4,
                'age_id' => 4,
                // 'gender_id' => 2
            ], [
                'price' => '125',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 45,
                'max_age' => 59,
                'plan_id' => 4,
                'age_id' => 5,
                // 'gender_id' => null
            ], [
                'price' => '227',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 60,
                'max_age' => 64,
                'plan_id' => 4,
                'age_id' => 6,
                // 'gender_id' => null
            ], [
                'price' => '286',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 65,
                'max_age' => 69,
                'plan_id' => 4,
                'age_id' => 7,
                // 'gender_id' => null
            ], [
                'price' => '365',
                'min_affiliate' => 1,
                'max_affiliate' => 99999,
                'min_age' => 70,
                'max_age' => 99999,
                'plan_id' => 4,
                'age_id' => 9,
            ], [ // [Empresas] - MediSanitas
                'price' => '130',
                'additional_1' => '29',
                'additional_2' => '119',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 0,
                'max_age' => 9,
                'plan_id' => 5,
                'age_id' => 1,
            ], [
                'price' => '111',
                'additional_1' => '24',
                'additional_2' => '101',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 10,
                'max_age' => 14,
                'plan_id' => 5,
                'age_id' => 2,
            ], [
                'price' => '145',
                'additional_1' => '32',
                'additional_2' => '131',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 5,
                'age_id' => 4,
            ], [
                'price' => '180',
                'additional_1' => '41',
                'additional_2' => '163',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 5,
                'age_id' => 4,
            ], [
                'price' => '212',
                'additional_1' => '47',
                'additional_2' => '192',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 45,
                'max_age' => 59,
                'plan_id' => 5,
                'age_id' => 5,
            ], [
                'price' => '253',
                'additional_1' => '56',
                'additional_2' => '230',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 60,
                'max_age' => 64,
                'plan_id' => 5,
                'age_id' => 6,
            ], [
                'price' => '482',
                'additional_1' => '107',
                'additional_2' => '439',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 65,
                'max_age' => 69,
                'plan_id' => 5,
                'age_id' => 7,
            ], [
                'price' => '889',
                'additional_1' => '189',
                'additional_2' => '810',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 70,
                'max_age' => 99999,
                'plan_id' => 5,
                'age_id' => 9,
            ], [
                'price' => '160',
                'additional_1' => '36',
                'additional_2' => '145',
                'min_affiliate' => 6,
                'max_affiliate' => 9,
                'min_age' => 0,
                'max_age' => 99999,
                'plan_id' => 5,
                'age_id' => 11,
            ], [
                'price' => '147',
                'additional_1' => '33',
                'additional_2' => '113',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 0,
                'max_age' => 99999,
                'plan_id' => 5,
                'age_id' => 11,
            ], [ // [Empresas] - Plan Vital Salud (PEAS)
                'price' => '81',
                'additional_1' => '49',
                'additional_2' => '78',
                'additional_3' => '87',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 0,
                'max_age' => 9,
                'plan_id' => 6,
                'age_id' => 1,
            ], [
                'price' => '50',
                'additional_1' => '61',
                'additional_2' => '85',
                'additional_3' => '112',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 10,
                'max_age' => 14,
                'plan_id' => 6,
                'age_id' => 2,
            ], [
                'price' => '65',
                'additional_1' => '80',
                'additional_2' => '112',
                'additional_3' => '146',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 6,
                'age_id' => 4,
            ], [
                'price' => '101',
                'additional_1' => '79',
                'additional_2' => '120',
                'additional_3' => '141',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 15,
                'max_age' => 44,
                'plan_id' => 6,
                'age_id' => 4,
            ], [
                'price' => '125',
                'additional_1' => '87',
                'additional_2' => '134',
                'additional_3' => '154',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 45,
                'max_age' => 59,
                'plan_id' => 6,
                'age_id' => 5,
            ], [
                'price' => '227',
                'additional_1' => '26',
                'additional_2' => '82',
                'additional_3' => '29',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 60,
                'max_age' => 64,
                'plan_id' => 6,
                'age_id' => 6,
            ], [
                'price' => '286',
                'additional_1' => '196',
                'additional_2' => '303',
                'additional_3' => '349',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 65,
                'max_age' => 69,
                'plan_id' => 6,
                'age_id' => 7,
            ], [
                'price' => '365',
                'additional_1' => '524',
                'additional_2' => '722',
                'additional_3' => '969',
                'min_affiliate' => 1,
                'max_affiliate' => 5,
                'min_age' => 70,
                'max_age' => 99999,
                'plan_id' => 6,
                'age_id' => 9,
            ], [
                'price' => '73',
                'additional_1' => '87',
                'additional_2' => '123',
                'additional_3' => '159',
                'min_affiliate' => 6,
                'max_affiliate' => 9,
                'min_age' => 0,
                'max_age' => 99999,
                'plan_id' => 6,
                'age_id' => 11,
            ], [
                'price' => '73',
                'additional_1' => '74',
                'additional_2' => '107',
                'additional_3' => '114',
                'min_affiliate' => 10,
                'max_affiliate' => 99999,
                'min_age' => 0,
                'max_age' => 99999,
                'plan_id' => 6,
                'age_id' => 11,
            ]
            // [
            //     'price' => '81',
            //     'min_affiliate' => 1,
            //     'max_affiliate' => 9999999,
            //     'min_age' => 0,
            //     'max_age' => 9,
            //     'plan_id' => 4,
            //     'age_id' => 1,
            //     'gender_id' => null
            // ], [
            //     'price' => '50',
            //     'min_affiliate' => 1,
            //     'max_affiliate' => 9999999,
            //     'min_age' => 10,
            //     'max_age' => 14,
            //     'plan_id' => 4,
            //     'age_id' => 2,
            //     'gender_id' => null
            // ], [
            //     'price' => '65',
            //     'min_affiliate' => 1,
            //     'max_affiliate' => 9999999,
            //     'min_age' => 15,
            //     'max_age' => 44,
            //     'plan_id' => 4,
            //     'age_id' => 3,
            //     'gender_id' => 1
            // ], [
            //     'price' => '101',
            //     'min_affiliate' => 1,
            //     'max_affiliate' => 9999999,
            //     'min_age' => 15,
            //     'max_age' => 44,
            //     'plan_id' => 4,
            //     'age_id' => 3,
            //     'gender_id' => 2
            // ], [
            //     'price' => '125',
            //     'min_affiliate' => 1,
            //     'max_affiliate' => 9999999,
            //     'min_age' => 45,
            //     'max_age' => 59,
            //     'plan_id' => 4,
            //     'age_id' => 4,
            //     'gender_id' => null
            // ], [
            //     'price' => '227',
            //     'min_affiliate' => 1,
            //     'max_affiliate' => 9999999,
            //     'min_age' => 60,
            //     'max_age' => 64,
            //     'plan_id' => 4,
            //     'age_id' => 5,
            //     'gender_id' => null
            // ], [
            //     'price' => '286',
            //     'min_affiliate' => 1,
            //     'max_affiliate' => 9999999,
            //     'min_age' => 65,
            //     'max_age' => 69,
            //     'plan_id' => 4,
            //     'age_id' => 6,
            //     'gender_id' => null
            // ], [
            //     'price' => '365',
            //     'min_affiliate' => 1,
            //     'max_affiliate' => 9999999,
            //     'min_age' => 70,
            //     'max_age' => 99999,
            //     'plan_id' => 4,
            //     'age_id' => 7,
            //     'gender_id' => null
            // ]
        ];

        foreach ($details as $detail) {
            App\Detail::create($detail);
        }

        $datas = [
            ['detail_id' => 3, 'gender_id' => 1],
            ['detail_id' => 4, 'gender_id' => 2],
            ['detail_id' => 10, 'gender_id' => 1],
            ['detail_id' => 11, 'gender_id' => 2],
            ['detail_id' => 17, 'gender_id' => 1],
            ['detail_id' => 18, 'gender_id' => 2],
            ['detail_id' => 24, 'gender_id' => 1],
            ['detail_id' => 25, 'gender_id' => 2],
            ['detail_id' => 31, 'gender_id' => 1],
            ['detail_id' => 32, 'gender_id' => 2],
            ['detail_id' => 38, 'gender_id' => 1],
            ['detail_id' => 39, 'gender_id' => 2],
            ['detail_id' => 46, 'gender_id' => 1],
            ['detail_id' => 47, 'gender_id' => 2],
            ['detail_id' => 56, 'gender_id' => 1],
            ['detail_id' => 57, 'gender_id' => 2],
        ];

        DB::table('detail_gender')->insert($datas);
    }
}
