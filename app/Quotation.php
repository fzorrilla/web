<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable = [
    	'name',
        'client_id',
        'children',
        'worker',
        'dependent',
        'amount',
        'transaction',
        'email',
        'total',
        'paid',
        'plan_id',
    ];

    public function schedule()
    {
        return $this->hasOne(Schedule::class);
    }
    public function visits()
    {
        return $this->hasOne(Visit::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function families()
    {
        return $this->hasMany(Family::class);
    }
}
