@extends('layouts.mob')

@section('title', 'SOCORRO WEB')

@section('content')
<div style="background: #FFF;">
  <div style="background: #e2e2e252;">
    <div class="row">
      <div class="col-md-12">
        <h2 class="title-section" style="margin-left: 13%;">
          SOCORRO
        </h2>
        <br>
        <hr class="borde-naranja" style="margin-left: 13%;">
      </div>
    </div>
  </div>
  <div class="container">  
    <p class="pt-4"></p>
    <div class="row">
      <div class="col-md-12 pt-3">
        <h2 style="color: rgba(142, 161, 178, 1);font-family: Montserrat-Regular;font-size: 22px;text-transform: none;">
            Es un dolor de cabeza buscar el mejor <span>seguro</span> para proteger tu familia, tu auto, tu empresa, tus bienes. Lo sé por experiencia propia.
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 pt-3">
        <h2 class="mb-4" style="color: rgba(142, 161, 178, 1);font-family: Montserrat-Regular;font-size: 25px;text-transform: none;">
          <b>¿Quién es Socorro? </b>
        </h2>

        <h2 style="color: rgba(142, 161, 178, 1);font-family: Montserrat-Regular;font-size: 22px;text-transform: none;">Hola, mi nombre es Socorro, y he pasado por este proceso muchas veces; hay muchos
        tipos de seguro, muchas empresas, muchos escenarios, muchas cláusulas, muchas
        letras pequeñas. Me mareo de solo recordar las interminables reuniones con los
        vendedores, las llamadas, los cambios a las cláusulas, las revisiones. Y al final, cuando
        dices todo lo que debe tener el seguro, éste se sale del presupuesto.</h2>
        <h2 style="color: rgba(142, 161, 178, 1);font-family: Montserrat-Regular;font-size: 22px;text-transform: none;">Con tantas opciones en el mercado, lo único que buscaba era tener un seguro en un
        solo sitio. Sin tener que lidiar con vendedores; donde la información esté clara y simple
        de entender; donde pueda comparar sin presiones entre varias opciones. Por eso
        decidí crear este sitio web, que de forma gratuita ayude a todos, los que como yo,
        quieren encontrar un seguro de forma rápida, asequible y confiable.</h2>
      </div>
      <div class="col-md-4 text-center">
        <img src="{{ asset('img/socorro_quien.png') }}" style="width: 84%;">
      </div>
      <div class="col-md-12 pt-3 pb-3 mb-3">
        <h2 class="mb-4" style="color: rgba(142, 161, 178, 1);font-family: montserrat-bold;font-size: 25px;text-transform: none;">
          <b>¿Porqué Socorro? </b>
        </h2>
        <h2 style="color: rgba(142, 161, 178, 1);font-family: Montserrat-Regular;font-size: 22px;text-transform: none;">
          En <span>socorro.pe</span> tú podrás: 
        </h2>
        <ul style="color: rgba(142, 161, 178, 1);font-family: Montserrat-Regular;font-size: 22px;text-transform: none;">
          <li>
            Buscar y comparar de forma gratuita entre seguros de diferentes empresas.
          </li>
          <li>
            Escoger el que más te convenga y comprarlo en cuestión de minutos.
          </li>
          <li>
            Personalizar el seguro que elijas para que cubra todo lo que necesites y que sea
acorde con tu presupuesto.
          </li>
          <li>
            Tener acceso inmediato cuando quieras a tus registros, recibos, historiales citas
con doctores, alertas etc. Sin necesidad de lidiar con la burocracia.
          </li>
          <li>
            Conseguir ayuda cuando lo necesites, ya que siempre podrás contactarme por
teléfono o chat para que te guíe en el uso de la plataforma; o inclusive si tienes
alguna consulta sobre los seguros.
          </li>
          <li>
            Y finalmente asegurar lo importante en unos cuantos clicks.
          </li>
        </ul>
        <h2 style="color: rgba(142, 161, 178, 1);font-family: Montserrat-Regular;font-size: 22px;text-transform: none;">
          En conclusión, tendrás todo lo que necesitas para encontrar el seguro que más te
convenga, y lo mejor es que lo podrás hacer desde donde estés; desde tu casa, tu
trabajo, incluso desde la playa. No tendrás que lidiar con el tráfico, con vendedores,
sentirte mal por no entender las palabras y terminologías extrañas. Recuperarás tu
tiempo. 
        </h2>
        <h2 style="color: rgba(142, 161, 178, 1);font-family: Montserrat-Regular;font-size: 22px;text-transform: none;">
          Es muy simple:
        </h2>
        <div class="four-step pt-5" style="height: 343px;">
          <div class="container">
            <div class="row content-step" >
              <div class="col-md-2 text-center back-step">
                <span class="content-step-p">
                  <p class="number-step">01</p>
                  <p class="">Cuéntame</p>
                  <p class="">que necesitas</p> 
                </span>
                <img src="{{ asset('img/four-4.png') }}" alt="First slide">
              </div>
              <div class="col-md-1 text-center content-step-arrow">
                <img src="{{ asset('img/arrow.png') }}" alt="First slide">
              </div>
              <div class="col-md-2 text-center back-step">
                <span class="content-step-p">
                  <p class="number-step">02</p>
                  <p class="">Compara</p>
                  <p class="">propuestas</p> 
                </span>
                <img src="{{ asset('img/four-1.png') }}" alt="First slide">
                
              </div>
              <div class="col-md-1 text-center content-step-arrow">
                <img src="{{ asset('img/arrow.png') }}" alt="First slide">
              </div>
              <div class="col-md-2 text-center back-step">
                <span class="content-step-p">
                  <p class="number-step">03</p>
                  <p class="">Paga</p>
                  <p class="">en línea</p> 
                </span>
                <img src="{{ asset('img/four-2.png') }}" alt="First slide">
              </div>
              <div class="col-md-1 text-center content-step-arrow">
                <img src="{{ asset('img/arrow.png') }}" alt="First slide">
              </div>
              <div class="col-md-2 text-center back-step">
                <span class="content-step-p">
                  <p class="number-step">04</p>
                  <p class="">Recibe</p>
                  <p class="">tu poliza</p> 
                </span>
                <img src="{{ asset('img/four-3.png') }}" alt="First slide">
              </div>
            </div>
          </div>
        </div>
        <h2 style="color: rgba(142, 161, 178, 1);font-family: Montserrat-Regular;font-size: 22px;text-transform: none;">
          Haz tu vida con tranquilidad sabiendo que tú, los tuyos y tus bienes están protegidos.
Ingresa ahora mismo a <span>socorro.pe</span> y encuentra el seguro perfecto para ti.
        </h2>
        <h2 class="mb-5" style="color: rgba(142, 161, 178, 1);font-family: Montserrat-Regular;font-size: 22px;text-transform: none;">
          Socorro es una plataforma digital para la venta de seguros creada por <span>camote&co.</span>, una startup con ADN joven e innovador, especializada en consultoría y creación de modelos de negocios digitales.
        </h2>
      </div>
    </div>   
  
  </div>
</div>
<style type="text/css">
  h2 span{
    color: rgba(10, 98, 162, 1);
    font-weight: 600;
  }
</style>
@endsection