<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'price',
    ];

    public function details()
    {
    	return $this->hasMany(Detail::class);
    }

    public function families()
    {
        return $this->hasMany(Family::class);
    }

    public function quotations()
    {
        return $this->hasMany(Quotation::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function items()
    {
        return $this->belongsToMany(Item::class);
    }
    public function ages()
    {
        return $this->belongsToMany(Age::class);
    }
}
