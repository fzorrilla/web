<div class="pt-5 mb-5">
  <div class="container text-center">
    
    <div class="row back-socorro">
        <div class="col-md-8 text-left offset-md-3 text-soy-socorro mt-5 pt-5">
          <div class="row">
            <div class="col-md-12 pb-5">
              <h2 class="title-section" style="margin-right: 80px;">SOY SOCORRO</h2>
              <br>
              <hr class="borde-naranja">
            </div>
          </div>
          <p>
            Hola, soy tu asesora virtual que te ayudará a encontrar la mejor protección para ti, tu familia o tu empresa.
          </p>
          <p>
            Pregúntame sobre nuestros seguros y te iré guiando para proteger de la mejor manera lo que mas te importa.
          </p>
          <div class="pt-5 mt-5">
            <button class="ml-auto mr-auto active text-white pt-2 pb-2 pl-4 pr-4">CONVERSEMOS</button>
          </div>
          <p class="p-protege ">
            "Protege lo que más te importa"
          </p>
        </div>
    </div>
  
  </div>
</div>