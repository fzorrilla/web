<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
    protected $table = 'diseases';
    protected $fillable = ['name', 'pathology_id']; //En la migración te recomiendo usar polaridad_id para mantener la nomenclatura estándar y evitar posibles errores

    public function pathology()
    {
         return $this->belongsTo(Pathology::class); //usar Polaridad en singular e inicial en mayúsculas al ser el nombre del modelo
    }
}
