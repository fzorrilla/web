<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Age extends Model
{
    public function details()
    {
    	return $this->hasMany(Detail::class);
    }

    public function insured()
    {
        return $this->belongsTo(Insured::class);
    }
     public function plan()
    {
    	return $this->hasMany(Plan::class);
    }
}
