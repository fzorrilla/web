<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/layout.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">


    <!-- Scripts -->
    <script src="{{ asset('bootstrap/js/jquery.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/slick.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript" src="https://unpkg.com/popper.js/dist/"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.all.min.js"></script>

</head>
</head>
<body>
    <!-- Navigation -->
    
        <div class="wrapper  sub-header2" style="">
          <div id="topbar2" class="clear row">
            <div class="fl_left_quotation col-md-6 pl-5">
              <img src="{{ asset('img/socorro-logo2.png') }}">
            </div>
            <div class="fl_right_quotation col-md-6 text-right">
              <ul>
                <li class="content-login" style="list-style: none;">
                    <i class="fas fa-power-off"></i> <a href="#">CERRAR</a>
                </li>
              </ul>
            </div>
          </div>
        </div>


    @yield('content')

    @include('footer')

    @yield('js')

    <style type="text/css">
        .dropdown-menu{
            min-width: 14rem;
        }
        .btn-cotiza{
            border: none !important;
            color: white;
        }
        button.btn-cotiza:focus {
            outline: 0px dotted;
            outline: 0px auto -webkit-focus-ring-color;
        }
        .dropdown-toggle::after {
            display: inline-block;
            margin-left: .255em;
            vertical-align: .255em;
            content: "";
            border-top: .0em solid;
            border-right: .0em solid transparent;
            border-bottom: 0;
            border-left: .0em solid transparent;
        }
    </style>
</body>
</html>
