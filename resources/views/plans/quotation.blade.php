@extends('layouts.mob_new')

@section('title', 'Lima Lab Consulting Group')

@section('content') {{-- {{ dd($igv) }} --}}
<div class="row pt-5 pb-5 mb-5 content-card" style="background: #F7F7F7;">
  @php
  $amount = 0;
  $base = 0;
  $additional_1 = 0;
  $additional_2 = 0;
  $additional_3 = 0;

  @endphp
  <!-- Personas -->
  @if (isset($client))
  @foreach ($plans as $plan)

  <form action="{{ route('public.quotation.update', [$id]) }}" method="POST" role="form" id="{{$plan->id}}">
    @method('PUT')
    @csrf
    <input type="hidden" name="plan_id" value="{{ $plan->id }}">
    {{-- <input type="hidden" name="igv" value="{{ $i }}"> --}}
    <div class="content-Slider-Planes">
      
            <div class="content-card-slider quotation ">
              <div class="title-slider1 quotation">
                {{ $plan->name }}
              </div>
              <div class="content-slider pt-3">
                <div class="col-md-12 pt-3 pb-4 text-center text-color-blue contain-info-plans">

                  @foreach ($payperplan as $payplan)
                  @if ($payplan['plan_id'] == $plan->id)
                  @php
                  $amount = round ($amount + $payplan['price'], 0);
                  @endphp
                  @endif
                  @endforeach
                  <h4 class="per-person">A pagar por persona: </h4>
                  <h3 class="amount-per-person">
                    S/ <span class="{{ $plan->id }}">{{ $amount }}</span>
                  </h3>
                  
                </div>

                <div class="col-md-12 pt-3 pl-4 pb-2 text-left">
                  <div class="form-group">
                    <input type="hidden" name="base_{{ $plan->id }}" value="{{ $amount }}">
                    <input type="hidden" name="amount_{{ $plan->id }}" value="{{ $amount }}">
                    <input type="hidden" name="total" value="{{ $amount }}">
                    @if ($client == "1")
                    <ul class="list-group list-group-flush list-group-quotation text-center">
                      @foreach ($payperplan as $payplan)
                      @if ($payplan['plan_id'] == $plan->id)
                      <li class="list-group-item">
                          <label class="checkbox">
                            <input name="affiliate[]" type="checkbox" class="" id="check" value="{{ $payplan['type']}}{{'-'}}{{$payplan['family_id'] }}{{'-'}}{{'1'}}" data-plan="{{ $plan->id }}" data-price="{{ $payplan['price'] }}" checked>
                            <span class="check"></span>

                          </label>
                              {{ $payplan['description'] }}<b> - S/ {{ $payplan['price'] }}</b>
                      </li>

                      @endif
                      @endforeach
                    </ul>
                    @else
                    <ul class="list-group list-group-flush list-group-quotation text-center">
                      {{-- Base --}}
                      <li class="list-group-item">
                        
                          @foreach ($payperplan as $payplan)
                          @if ($payplan['plan_id'] == $plan->id)
                          @php
                          $base = round ($base + $payplan['price'], 0);
                          @endphp
                          @endif
                          @endforeach
                            <label class="checkbox">
                              <input type="checkbox" name="optradio" data-price="{{ $base }}" data-plan="{{ $plan->id }}" disabled checked="true">Base <b>- S/  {{ $base }}</b>
                              <span class="check"></span>
                            </label>
                          {{-- <a class="price" href="#" data-price="{{ $base }}" data-plan="{{ $plan->id }}"></a> --}}
                          @php
                          $base = 0;
                          @endphp
                        
                      </li>
                      {{-- Adicional 1 --}}
                      <li class="list-group-item">
                        
                          @foreach ($payperplan as $payplan)
                          @if ($payplan['plan_id'] == $plan->id)
                          @php
                          $additional_1 = round ($additional_1 + $payplan['additional_1'], 0);
                          @endphp
                          @endif
                          @endforeach
                            <label class="checkbox">
                              <input type="checkbox" name="optradio" data-price="{{ $additional_1 }}" data-plan="{{ $plan->id }}">Adicional 1 <b>- S/ {{ $additional_1 }}</b></del>
                              <span class="check"></span>
                            </label>
                          {{-- <a class="price" href="#" data-price="{{ $additional_1 }}" data-plan="{{ $plan->id }}"></a> --}}
                          @php
                          $additional_1 = 0;
                          @endphp
                        
                      </li>
                      {{-- Adicional 2 --}}
                      <li class="list-group-item">
                        
                          @foreach ($payperplan as $payplan)
                          @if ($payplan['plan_id'] == $plan->id)
                          @php
                          $additional_2 = round ($additional_2 + $payplan['additional_2'], 0);
                          @endphp
                          @endif
                          @endforeach
                            <label class="checkbox">
                              <input type="checkbox" name="optradio" data-price="{{ $additional_2 }}" data-plan="{{ $plan->id }}">Adicional 2 <b>- S/ {{ $additional_2 }}</b></del>
                              <span class="check"></span>

                            </label>
                          {{-- <a class="price" href="#" data-price="{{ $additional_2 }}" data-plan="{{ $plan->id }}"></a> --}}
                          @php
                          $additional_2 = 0;
                          @endphp
                        
                      </li>
                      {{-- Adicional 3 --}}
                      <li class="list-group-item">
                        
                          @foreach ($payperplan as $payplan)
                          @if ($payplan['plan_id'] == $plan->id)
                          @php
                          $additional_3 = round ($additional_3 + $payplan['additional_3'], 0);
                          @endphp
                          @endif
                          @endforeach
                          @if ($additional_3)
                            <label class="checkbox">
                              <input type="checkbox" name="optradio" data-price="{{ $additional_3 }}" data-plan="{{ $plan->id }}">Adicional 3 <b>- S/ {{ $additional_3 }}</b></del>
                            <span class="check"></span>

                            </label>
                          @else
                            <label class="checkbox">
                              <input type="checkbox" name="optradio" disabled><del>Adicional 3 <b>- S/ {{ $additional_3 }}</b></del>
                              <span class="check"></span>

                            </label>
                          @endif
                          @php
                          $additional_3 = 0;
                          @endphp
                        
                      </li>
                    </ul>
                
                    @endif
                  </div>
                </div>
                <div class="col-md-12 pb-2 text-center">
                  <a href="#{{ $plan->id }}">
                    <input type="button" value="VER MAS" class="socorro__card-button_blue" >
                      <div class="collapse" id="{{ $plan->id }}"><br>
                        <ul class="list-group list-group-flush list-group-quotation text-center">
                          @foreach ($plan->items as $item)
                          <li class="list-group-item">
                            <span class="text warning">
                              {{ $item->description }}
                            </span>
                          </li>
                          @endforeach
                        </ul>
                      </div>
                  </a>
                </div>
                <div class="panel-footer">
                  <button type="submit" class="socorro__card-button">LO QUIERO</button>
                </div>
              </div>
            </div>
    </div>
  </form>
  <br>
  @php
  $amount = 0;
  @endphp
  @endforeach
  @else <!-- Empresas -->
  <form action="{{ route('import-excel')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="quotation_id" id="input" value="{{$id}}">
    <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading">Empresas</div>
      <!-- Table -->
      <table class="table">
        <tbody>
          <tr>
            <td style="width: 800px;"><label>Plantilla de Carga Masiva</label></td>
            <td><a href="{{ url('/docs/payload_data.xlsx') }}" target="_blank" class="btn btn-block btn-primary">Descargar</a></td>
          </tr>
          <tr>
            <td style="width: 800px;"><input type="file" name="excel" class="form-control form-control-file"></td>
            <td><input type="submit" value="Enviar" class="btn btn-block btn-primary"></td>
          </tr>
        </tbody>
      </table>
    </div>
  </form>
  @endif
</div>
<input type="hidden" name="client" value="{{ $client }}">

<!-- Modal -->
<div class="modal fade" id="formBusiness" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Formulario Empresas</h4>
      </div>
      <!-- Modal Body -->
      <div class="modal-body">
       <form id="companyForm" class="form-horizontal" role="form" data-backdrop="static" data-keyboard="false">
        @csrf
        <input type="hidden" name="quotation_id" id="input" value="{{ $id }}">
        <div class="form-group">
          <label  class="col-sm-3 control-label" for="inputName">Nombre Empresa(*)</label>
          <div class="col-sm-8">
            <input type="text" name="name" id="inputName_for" class="form-control" placeholder="Nombre Empresa (Razón Social)" onkeypress='return validar(event);' onkeyup="mayus(this);"/>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label" for="inputRuc">RUC(*)</label>
          <div class="col-sm-8">
            <input type="text" name="ruc" id="inputRuc_for" minlength="1" maxlength="11" class="form-control" placeholder="RUC" onkeypress='return solonumeros(event);'/>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label" for="inputContact">Contacto(*)</label>
          <div class="col-sm-8">
            <input type="text" name="contact" id="inputContact_for" class="form-control" placeholder="Nombre Representante Legal" onkeypress='return validar(event);'  onkeyup="mayus(this);"/>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label" for="identity_id">Tipo de Documento(*)</label>
          <div class="col-sm-8">
           <select class="form-control" id="identity_id_for" name="identity_id" required="true" tabindex="5">
             <option value="">
              Seleccionar
            </option>
            @foreach ($identities as $identity)
            <option value="{{ $identity->id }}">
             {{ $identity->dstipoiden }}
           </option>
           @endforeach
         </select>
       </div>
     </div>
     <div class="form-group">
      <label class="col-sm-3 control-label" for="inputDni">Numero Documento(*)</label>
      <div class="col-sm-8">
        <input type="text" name="dni" id="inputDni_for" class="form-control" minlength="1" maxlength="11" placeholder="Numero de DNI CE o Pasaporte"onkeypress='return solonumeros(event);'/>
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-sm-3 control-label">Correo Electrónico(*)</label>
      <div class="col-sm-8">
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-envelope"></i>
          </div>
          <input type="email" class="form-control" name="email" id="inputEmail_for" placeholder="Correo Electrónico" onkeyup="mayus(this);">
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="phone" class="col-sm-3 control-label">Teléfono(*)</label>
      <div class="col-sm-8">
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-phone"></i>
          </div>
          <input type="text" class="form-control" minlength="1" maxlength="11" id="phone_for" name="phone" placeholder="Teléfono" tabindex="8" onkeypress='return solonumeros(event);'>
        </div>
      </div>
    </div>
    <div class="form-group">
      <hr class="hr-primary" color="blue" size="6"/>
      <label class="col-sm-3 control-label" for="#">
        Dirección:
      </label>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label" for="direction_id">
        Tipo Dirección(*)
      </label>
      <div class="col-sm-8">
        <select class="form-control validateForm" id="direction_id_for" name="direction_id" required="true" tabindex="5">
          <option value="">
            Seleccionar
          </option>
          @foreach ($directions as $direction)
          <option value="{{ $direction->id }}">
            {{ $direction->name }}
          </option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label" for="way_id">
        Tipo de Via(*)
      </label>
      <div class="col-sm-8">
        <select class="form-control validateForm" id="way_id_for" name="way_id" required="true" tabindex="5">
          <option value="">
            Seleccionar
          </option>
          @foreach ($ways as $way)
          <option value="{{ $way->id }}">
            {{ $way->name }}
          </option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label" for="address">
      </label>
      <div class="col-sm-8">
        <input class="form-control validateForm" id="address_for" name="address" placeholder="Dirección Empresa" required="true" tabindex="3" type="text"  onkeyup="mayus(this);">
      </input>
    </div>
  </div>
  <div class="form-group">
    <label for="optionsRadios1" class="col-sm-3 control-label">Departamentos</label>
    <div class="col-sm-8">
      <select name="departament" id="departament_for" class="form-control provinces" tabindex="1" required="true">
        <option value="">Seleccionar</option>
        @foreach ($departaments as $departament)
        <option value="{{ $departament->id }}{{'-'}}{{ $departament->delivery }}">{{ $departament->name }}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="optionsRadios1" class="col-sm-3 control-label">Provincias</label>
    <div class="col-sm-8">
      <select name="provinces" id="provinces_two" class="form-control districts" tabindex="2" required="true" disabled >
        <option value="">Seleccionar</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="optionsRadios1" class="col-sm-3 control-label">Distritos</label>
    <div class="col-sm-8">
      <select name="district_id" id="district_id_two" class="form-control" tabindex="3" required="true"  disabled >
        <option value="">Seleccionar</option>
      </select>
    </div>
  </div>
  <div class="col-sm-9">
    <label class="col-sm-8 control-label" for="#">
      Los Campos (*) son obligatorios
    </label>
  </div>

</form>
</div>
<!-- Modal Footer -->
<div class="modal-footer">
  {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --}}
  <button id="wrapped" type="button" class="btn btn-primary">Aceptar</button>
</div>
</div>
</div>
</div>
<!-- Modal otro PYME -->
<div class="modal fade" id="formBusiness1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Formulario PYME</h4>
      </div>
      <!-- Modal Body -->
      <div class="modal-body">
        <form id="companyForm1" class="form-horizontal" role="form" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog">
          @csrf
          <input type="hidden" name="quotation_id" id="input" value="{{ $id }}">
          <div class="form-group">
            <label  class="col-sm-3 control-label" for="inputName">Nombre Empresa(*)</label>
            <div class="col-sm-8">
              <input type="text" name="name" id="inputName_pyme" class="form-control" placeholder="Nombre Empresa (Razón Social)" onkeypress='return validar(event);' onkeyup="mayus(this);"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputRuc">RUC(*)</label>
            <div class="col-sm-8">
              <input type="text" name="ruc" id="inputRuc_pyme" minlength="1" maxlength="11" class="form-control" placeholder="RUC" onkeypress='return solonumeros(event);'/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputContact">Contacto(*)</label>
            <div class="col-sm-8">
              <input type="text" name="contact" id="inputContact_pyme" class="form-control" placeholder="Nombre Representante Legal" onkeypress='return validar(event);'  onkeyup="mayus(this);"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="inputContact">Tipo de Documento(*)</label>
            <div class="col-sm-8">
             <select class="form-control" id="identity_id_pyme" name="identity_id" required="true" tabindex="5">
               <option value="">
                Seleccionar
              </option>
              @foreach ($identities as $identity)
              <option value="{{ $identity->id }}">
               {{ $identity->dstipoiden }}
             </option>
             @endforeach
           </select>
         </div>
       </div>
       <div class="form-group">
        <label class="col-sm-3 control-label" for="inputDni">Numero Documento(*)</label>
        <div class="col-sm-8">
          <input type="text" name="dni" id="inputDni_pyme" class="form-control" minlength="1" maxlength="11" placeholder="Numero de DNI CE o Pasaporte"onkeypress='return solonumeros(event);'/>
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail" class="col-sm-3 control-label">Correo Electrónico(*)</label>
        <div class="col-sm-8">
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-envelope"></i>
            </div>
            <input type="email" class="form-control" name="email" id="inputEmail_pyme" placeholder="Correo Electrónico" onkeyup="mayus(this);">
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="phone" class="col-sm-3 control-label">Teléfono(*)</label>
        <div class="col-sm-8">
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-phone"></i>
            </div>
            <input type="text" class="form-control" minlength="1" maxlength="11" id="phone_pyme" name="phone" placeholder="Teléfono" tabindex="8" onkeypress='return solonumeros(event);'>
          </div>
        </div>
      </div>
      <div class="form-group">
        <hr class="hr-primary" color="blue" size="6"/>
        <label class="col-sm-3 control-label" for="#">
          Dirección:
        </label>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label" for="direction_id">
          Tipo Dirección(*)
        </label>
        <div class="col-sm-8">
          <select class="form-control validateForm" id="direction_id_pyme" name="direction_id" required="true" tabindex="5">
            <option value="">
              Seleccionar
            </option>
            @foreach ($directions as $direction)
            <option value="{{ $direction->id }}">
              {{ $direction->name }}
            </option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label" for="way_id">
          Tipo de Via(*)
        </label>
        <div class="col-sm-8">
          <select class="form-control validateForm" id="way_id_pyme" name="way_id" required="true" tabindex="5">
            <option value="">
              Seleccionar
            </option>
            @foreach ($ways as $way)
            <option value="{{ $way->id }}">
              {{ $way->name }}
            </option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label" for="address">
        </label>
        <div class="col-sm-8">
          <input class="form-control validateForm" id="address_pyme" name="address" placeholder="Dirección Empresa" required="true" tabindex="3" type="text"  onkeyup="mayus(this);">
        </input>
      </div>
    </div>
    <div class="form-group">
      <label for="optionsRadios1" class="col-sm-3 control-label">Departamentos</label>
      <div class="col-sm-8">
        <select name="departament" id="departament_pyme" class="form-control provinces" tabindex="1" required="true">
          <option value="">Seleccionar</option>
          @foreach ($departaments as $departament)
          <option value="{{ $departament->id }}{{'-'}}{{ $departament->delivery }}">{{ $departament->name }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="optionsRadios1" class="col-sm-3 control-label">Provincias</label>
      <div class="col-sm-8">
        <select name="provinces" id="provinces_tree" class="form-control districts" tabindex="2" required="true" disabled >
          <option value="">Seleccionar</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="optionsRadios1" class="col-sm-3 control-label">Distritos</label>
      <div class="col-sm-8">
        <select name="district_id" id="district_id_tree" class="form-control" tabindex="3" required="true"  disabled >
          <option value="">Seleccionar</option>
        </select>
      </div>
    </div>
    <div class="col-sm-9">
      <label class="col-sm-8 control-label" for="#">
        Los Campos (*) son obligatorios
      </label>
    </div>
  </form>
</div>
<!-- Modal Footer -->
<div class="modal-footer">
  {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --}}
  <button id="wrapped1" type="button" class="btn btn-primary">Aceptar</button>
</div>
</div>
</div>
</div>
<input type="hidden" id="form_id">
<!-- Modal para mini carga -->
<div class="modal fade" id="formBusiness4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Carga Datos Básicos Afiliados</h4>
      </div>
      <!-- Modal Body -->
      <div class="modal-body">
        <form id="companyForm4" action="{{ route('public.store') }}" method="POST" class="form-horizontal" role="form" data-backdrop="static" data-keyboard="false">



          <input type="hidden" name="quotation_id[]" id="input" value="{{ $id }}">

          <input type="hidden" name="total" id="input" value="">
          <table class="table table-bordered" border="1">
            <thead>
              <tr>
                <th scope="col" align="center">Tipo de Doc(*)</th>
                <th scope="col" align="center">Numero de Doc.(*)</th>
                <th scope="col" align="center">Primer Nombre(*)</th>
                <th scope="col" align="center">Apellido Paterno(*)</th>
                <th scope="col" align="center">Genero(*)</th>
                <th scope="col" align="center">Correo(*)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td scope="row">
                 <select class="form-control" id="identity_id_date" name="identity_id[]" required="true" tabindex="5">
                   <option value="">
                    Seleccionar
                  </option>
                  @foreach ($identities as $identity)
                  <option value="{{ $identity->id }}">
                   {{ $identity->dstipoiden }}
                 </option>
                 @endforeach
               </select></td>
               <td><input type="text" min="1" max="8" name="dni[]" id="inputDni_date" class="form-control" minlength="1" maxlength="11" placeholder="Ingrese DNI, CE o Pasaporte" onkeypress='return solonumeros(event);'/></td>
               <td><input type="text" name="name[]" id="inputName_date" class="form-control" placeholder="Primer Nombre" onkeypress='return validar(event);' onkeyup="mayus(this);"/></td>
               <td><input type="text" name="last_name_1[]" id="inputApellido_date" class="form-control" placeholder="Primer Apellido" onkeypress='return validar(event);' onkeyup="mayus(this);"/></td>
               <td scope="row"><select name="gender_id[]" class=" form-control" id="gender_id_date">
                <option value=" " selected>Seleccione</option>
                <option value="1">Masculino</option>
                <option value="2">Femenino</option>
              </select></td>
              <td><input type="email" class="form-control" name="email[]" id="inputEmail_date" placeholder="Correo Electrónico" onkeyup="mayus(this);"></td>
            </tr>
          </tbody>
        </table>

        <!-- Modal Footer -->
        <div class="col-sm-9">
          <label class="col-sm-8 control-label" for="#">
            Los Campos (*) son obligatorios
          </label>
        </div>
      </form>
      <div class="modal-footer">
        {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --}}
        <button id="wrapped4" type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!--FIn mini carga-->
@stop

@section('css')
<style type="text/css">
  .list-group-item {
    text-align: left;
  }
</style>
@stop

@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript">
  function mayus(e) {
    e.value = e.value.toUpperCase();
  }
  	  function solonumeros(e) { // 1
		tecla = (document.all) ? e.keyCode: e.which;// 2
		if (tecla == 8) return true; // 3
		patron = /[0-9\s]/; // 4
		te = String.fromCharCode(tecla); // 5
		return patron.test(te); // 6
	}
	function validar(e) {
		tecla = (document.all) ? e.keyCode : e.which;
		if (tecla==8) return true;
		patron =/[A-Za-z\s]/;
		te = String.fromCharCode(tecla);
		return patron.test(te);
	}
</script>
<script type="text/javascript">
  $(document).ready(function() {
      // $(function() {
      // 	$('.panel').on('mouseover',function() {
      // 		$(this).css('top','-7px');
      // 	});
      // 	$('.panel').on('mouseout',function() {
      // 		$(this).css('top','0px');
      // 	});
      // });

      // $('.sendToPayment').on('click', function(e) {
      // 	window.location.href = $(this).attr('data-url');
      // 	return false;
      // });
      $("input[type=radio]").on( "click", function() {

        var amount = 0;

        if ($(this).data('price') == $("input[name='base_" + $(this).data('plan') + "']").val()) {
          amount = parseFloat($("input[name='base_" + $(this).data('plan') + "']").val()).toFixed(2);
        } else {
          amount = parseFloat($("input[name='base_"+ $(this).data('plan') +"']").val()) + parseFloat($(this).data('price'));
        }

        $("input[name='amount_" + $(this).data('plan') + "']").val(amount);

        $("input[name=total]", $(this.form)).val($("input[name='amount_" + $(this).data('plan') + "']").val());
        $('.' + $(this).data('plan')).text(parseFloat($("input[name='amount_" + $(this).data('plan') + "']").val()).toFixed(2));
      });

      $("input[type=checkbox]").on("click", function() {

        var amount = 0;

        if ($(this).is(':checked')) {
          amount = Number($("input[name='amount_" + $(this).data('plan') + "']").val()) + Number($(this).data('price'));
        } else {
          amount = Number($("input[name='amount_" + $(this).data('plan') + "']").val()) - Number($(this).data('price'));
        }

        $("input[name='amount_" + $(this).data('plan') + "']").val(amount);

        $("input[name=total]", $(this.form)).val($("input[name='amount_" + $(this).data('plan') + "']").val());
        $('.' + $(this).data('plan')).text($("input[name='amount_" + $(this).data('plan') + "']").val());


      });

      $("form").submit(function(e) {
        // e.preventDefault();
        var unchecked = [];
        var flat = 0;

        $.each($("input[type=checkbox]", $(this)), function(k,j) {
          if(!j.checked) {
            var string = j.value;
            arraySubStrings = string.split('-');
            arraySubStrings[2] = 0;
            var value = arraySubStrings[0]+'-'+arraySubStrings[1]+'-'+arraySubStrings[2];
            unchecked.push(value);
            $("input[name=unchecked]").val(unchecked);
          } else {
            flat = flat +1;
            var string = j.value;
            arraySubStrings = string.split('-');
            arraySubStrings[2] = 1;
            var value = arraySubStrings[0]+'-'+arraySubStrings[1]+'-'+arraySubStrings[2];
            unchecked.push(value);
            $("input[name=unchecked]").val(unchecked);

          }
        });
        if(flat < 1) {
          e.preventDefault();
          Swal({
            type: 'warning',
            title: 'Debe seleccionar al menos un beneficiario para este Plan'
          });
        } else {
          if ($("input[name='client']").val() == "2") {
            e.preventDefault();
            $("#form_id").val($(this).attr('id'));
            console.table($(this));
            console.log($("#form_id").val());
            $.ajax({
              type: $(this).attr("method"),
              url: $(this).attr("action"),
              data:$(this).serialize(),

            })


            Swal.fire({
              type: 'success',
              width: '40%',
              title: 'Su facturación anual es mayor que S/ 7,140,000.?',
              confirmButtonText:  'SI',
              cancelButtonText:  'No',
              showCancelButton: true,
              showCloseButton: false,
            }).then((result) => {
              if (result.value) {
                e.preventDefault();
                $("button[type='submit']").attr("disabled", true);
                $('#formBusiness').modal('show');
              }
              else if (result.dismiss === Swal.DismissReason.cancel) {
                if (fund <= 5)
                {
                  e.preventDefault();
                  $("button[type='submit']").attr("disabled", true);
                  $('#formBusiness3').modal('show');
                }
                else
                {
                  e.preventDefault();
                  $("button[type='submit']").attr("disabled", true);
                  $('#formBusiness1').modal('show');
                }

              }
            });
          } else {
            return true;
          }
        }
      });
      // Send
      $('#wrapped').on('click', function(event) {
        if ($.trim($('#inputName_for').val()).length) {
          if ($.trim($('#inputRuc_for').val()).length) {
            if ($.trim($('#inputContact_for').val()).length) {
             if ($.trim($('#identity_id_for').val()).length) {
              if ($.trim($('#inputDni_for').val()).length) {
                if ($.trim($('#inputEmail_for').val()).length) {
                  if ($.trim($('#phone_for').val()).length) {
                    if ($.trim($('#direction_id_for').val()).length) {
                      if ($.trim($('#way_id_for').val()).length) {
                       if ($.trim($('#address_for').val()).length) {
                         if ($.trim($('#departament_for').val()).length) {
                          if ($.trim($('#provinces_two').val()).length) {
                            if ($.trim($('#district_id_two').val()).length) {
                              var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
                              if (regex.test($('#inputEmail_for').val().trim())) {
                                Swal.fire({
                                  type: 'info',
                                  width: '40%',
                                  title: 'Está conforme con la información ingresada?',
                                  confirmButtonText:  'Si',
                                  cancelButtonText:  'No',
                                  showCancelButton: true,
                                  showCloseButton: true,
                                }).then((result) => {
                                  if (result.value) {
                                    return true;
                                  }
                                  else if (result.dismiss === Swal.DismissReason.cancel)
                                  {
                                    return false;
                                  }
                                });
                              } else {
                                Swal({
                                  type: 'warning',
                                  title: 'Validar',
                                  text: 'La direccón de correo no es válida'
                                });
                              }
                            } else {
                              Swal({
                                type: 'warning',
                                title: 'Validar',
                                text: 'Debe seleccionar un districto'
                              });
                            }
                          } else {
                            Swal({
                              type: 'warning',
                              title: 'Validar',
                              text: 'Debe seleccionar una provincia'
                            });
                          }
                        } else {
                          Swal({
                            type: 'warning',
                            title: 'Validar',
                            text: 'Debe seleccionar un departamento'
                          });
                        }
                      } else {
                        Swal({
                          type: 'warning',
                          title: 'Validar',
                          text: 'Debe ingresar una dirección'
                        });
                      }
                    } else {
                      Swal({
                        type: 'warning',
                        title: 'Validar',
                        text: 'Debe seleccionar un tipo via'
                      });
                    }
                  } else {
                    Swal({
                      type: 'warning',
                      title: 'Validar',
                      text: 'Debe seleccionar un tipo de dirección'
                    });
                  }
                } else {
                  Swal({
                    type: 'warning',
                    title: 'Validar',
                    text: 'Debe ingresar un Teléfono'
                  });
                }
              } else {
                Swal({
                  type: 'warning',
                  title: 'Validar',
                  text: 'Debe ingresar un Correo Electrónico'
                });
              }
            } else {
              Swal({
                type: 'warning',
                title: 'Validar',
                text: 'Debe ingresar un DNI, CE o Pasaporte'
              });
            }
          } else {
            Swal({
              type: 'warning',
              title: 'Validar',
              text: 'Debe seleccionar un tipo de documento'
            });
          }
        } else {
          Swal({
            type: 'warning',
            title: 'Validar',
            text: 'Debe ingresar un Nombre Contacto'
          });
        }
      }else {
        Swal({
          type: 'warning',
          title: 'Validar',
          text: 'Debe ingresar un RUC'
        });
      }
    } else {
      Swal({
        type: 'warning',
        title: 'Validar',
        text: 'Debe ingresar un Nombre Empresa'
      });
    }
    var company = {
      'name' : $("input[name='name']", $('#companyForm')).val(),
      'ruc' : $("input[name='ruc']", $('#companyForm')).val(),
      'contact' : $("input[name='contact']", $('#companyForm')).val(),
      'identity_id' : $("select[name='identity_id']", $('#companyForm')).val(),
      'dni' : $("input[name='dni']", $('#companyForm')).val(),
      'email' : $("input[name='email']", $('#companyForm')).val(),
      'phone' : $("input[name='phone']", $('#companyForm')).val(),
      'direction_id' : $("select[name='direction_id']", $('#companyForm')).val(),
      'way_id' : $("select[name='way_id']", $('#companyForm')).val(),
      'address' : $("input[name='address']", $('#companyForm')).val(),
      'district_id': $("select[name='district_id']", $('#companyForm')).val(),
      'quotation_id' : $("input[name='quotation_id']", $('#companyForm')).val(),
    };


    console.table(company);

    var data = JSON.stringify(company);

    var baseUrl = '{{ env("BASE_URL", url('/')) }}';

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
      }
    });

    $.ajax({
      type: "POST",
      enctype: 'multipart/form-data',
      url: baseUrl + "/companies",
      data: data,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      cache: false,
      timeout: 600000,
      success: function (data) {
        Swal({
          type: 'success',
          width: '40%',
          title: 'Gracias por cotizar con nosotros en breve será contactado por un asesor de SOCORRO',

        }).then((result) => {
          if (result.value) {
            var baseUrl = '{{ env("BASE_URL", url('/')) }}';
            window.location.href = baseUrl;
          }
        });
      },
      error: function (e) {
            // $("#result").text(e.responseText);
            console.log("ERROR : ", e);
          }
        }).always(function(){
          // $('#spinner').fadeOut()
        });
      });
      ////////////////
      $('#wrapped1').on('click', function(event) {
       if ($.trim($('#inputName_pyme').val()).length) {
        if ($.trim($('#inputRuc_pyme').val()).length) {
          if ($.trim($('#inputContact_pyme').val()).length) {
           if ($.trim($('#identity_id_pyme').val()).length) {
            if ($.trim($('#inputDni_pyme').val()).length) {
              if ($.trim($('#inputEmail_pyme').val()).length) {
                if ($.trim($('#phone_pyme').val()).length) {
                  if ($.trim($('#direction_id_pyme').val()).length) {
                    if ($.trim($('#way_id_pyme').val()).length) {
                     if ($.trim($('#address_pyme').val()).length) {
                       if ($.trim($('#departament_pyme').val()).length) {
                        if ($.trim($('#provinces_tree').val()).length) {
                          if ($.trim($('#provinces_tree').val()).length) {
                            var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
                            if (regex.test($('#inputEmail_pyme').val().trim())) {
                              Swal.fire({
                                type: 'info',
                                width: '40%',
                                title: 'Está conforme con la información ingresada?',
                                confirmButtonText:  'Si',
                                cancelButtonText:  'No',
                                showCancelButton: true,
                                showCloseButton: true,
                              }).then((result) => {
                                if (result.value) {
                                  return true;
                                }
                                else if (result.dismiss === Swal.DismissReason.cancel)
                                {
                                  return false;
                                }
                              });
                            } else {
                              Swal({
                                type: 'warning',
                                title: 'Validar',
                                text: 'La direccón de correo no es válida'
                              });
                            }
                          } else {
                            Swal({
                              type: 'warning',
                              title: 'Validar',
                              text: 'Debe seleccionar un districto'
                            });
                          }
                        } else {
                          Swal({
                            type: 'warning',
                            title: 'Validar',
                            text: 'Debe seleccionar una provincia'
                          });
                        }
                      } else {
                        Swal({
                          type: 'warning',
                          title: 'Validar',
                          text: 'Debe seleccionar un departamento'
                        });
                      }
                    } else {
                      Swal({
                        type: 'warning',
                        title: 'Validar',
                        text: 'Debe ingresar una dirección'
                      });
                    }
                  } else {
                    Swal({
                      type: 'warning',
                      title: 'Validar',
                      text: 'Debe seleccionar un tipo via'
                    });
                  }
                } else {
                  Swal({
                    type: 'warning',
                    title: 'Validar',
                    text: 'Debe seleccionar un tipo de dirección'
                  });
                }
              } else {
                Swal({
                  type: 'warning',
                  title: 'Validar',
                  text: 'Debe ingresar un Teléfono'
                });
              }
            } else {
              Swal({
                type: 'warning',
                title: 'Validar',
                text: 'Debe ingresar un Correo Electrónico'
              });
            }
          } else {
            Swal({
              type: 'warning',
              title: 'Validar',
              text: 'Debe ingresar un DNI, CE o Pasaporte'
            });
          }
        } else {
          Swal({
            type: 'warning',
            title: 'Validar',
            text: 'Debe seleccionar un tipo de documento'
          });
        }
      } else {
        Swal({
          type: 'warning',
          title: 'Validar',
          text: 'Debe ingresar un Nombre Contacto'
        });
      }
    }else {
      Swal({
        type: 'warning',
        title: 'Validar',
        text: 'Debe ingresar un RUC'
      });
    }
  } else {
    Swal({
      type: 'warning',
      title: 'Validar',
      text: 'Debe ingresar un Nombre Empresa'
    });
  }
  var company = {
    'name' : $("input[name='name']", $('#companyForm1')).val(),
    'ruc' : $("input[name='ruc']", $('#companyForm1')).val(),
    'contact' : $("input[name='contact']", $('#companyForm1')).val(),
    'identity_id' : $("select[name='identity_id']", $('#companyForm1')).val(),
    'dni' : $("input[name='dni']", $('#companyForm1')).val(),
    'email' : $("input[name='email']", $('#companyForm1')).val(),
    'phone' : $("input[name='phone']", $('#companyForm1')).val(),
    'direction_id' : $("select[name='direction_id']", $('#companyForm1')).val(),
    'way_id' : $("select[name='way_id']", $('#companyForm1')).val(),
    'address' : $("input[name='address']", $('#companyForm1')).val(),
    'district_id': $("select[name='district_id']", $('#companyForm1')).val(),
    'quotation_id' : $("input[name='quotation_id']", $('#companyForm1')).val(),
  };
  console.table(company);

  var data = JSON.stringify(company);

  var baseUrl = '{{ env("BASE_URL", url('/')) }}';

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
    }
  });

  $.ajax({
    type: "POST",
    enctype: 'multipart/form-data',
    url: baseUrl + "/companies1",
    data: data,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    cache: false,
    timeout: 600000,
    success: function (data) {
      Swal({
        type: 'success',
        width: '40%',
        title: 'Gracias por su preferencia, Se le fue enviado un correo a: ' + $("input[name='email']", $('#companyForm1')).val() + ', con más detalles sobre su cotización. ¡Muchas gracias!',
      }).then((result) => {
        if (result.value) {
         var baseUrl = '{{ env("BASE_URL", url('/')) }}';
         window.location.href = baseUrl;
       }
     });
    },
    error: function (e) {
            // $("#result").text(e.responseText);
            console.log("ERROR : ", e);
          }
        }).always(function(){
          // $('#spinner').fadeOut()
        });
      });
      //show de 1-5 businness
      $('#wrapped2').on('click', function(event) {
       if ($.trim($('#inputName').val()).length) {
        if ($.trim($('#inputRuc').val()).length) {
          if ($.trim($('#inputContact').val()).length) {
           if ($.trim($('#identity_id').val()).length) {
            if ($.trim($('#inputDni').val()).length) {
              if ($.trim($('#inputEmail').val()).length) {
                if ($.trim($('#phone').val()).length) {
                  if ($.trim($('#direction_id').val()).length) {
                    if ($.trim($('#way_id').val()).length) {
                      if ($.trim($('#address').val()).length) {
                       if ($.trim($('#departament').val()).length) {
                        if ($.trim($('#provinces').val()).length) {
                          if ($.trim($('#district_id').val()).length) {
                            var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
                            if (regex.test($('#inputEmail').val().trim())) {
                              Swal.fire({
                                type: 'info',
                                width: '40%',
                                title: 'Está conforme con la información ingresada?',
                                confirmButtonText:  'Si',
                                cancelButtonText:  'No',
                                showCancelButton: true,
                                showCloseButton: true,
                              }).then((result) => {
                                if (result.value) {
                                  return true;
                                }
                                else if (result.dismiss === Swal.DismissReason.cancel)
                                {
                                  return false;
                                }
                              });
                            } else {
                              Swal({
                                type: 'warning',
                                title: 'Validar',
                                text: 'La direccón de correo no es válida'
                              });
                            }
                          } else {
                            Swal({
                              type: 'warning',
                              title: 'Validar',
                              text: 'Debe seleccionar un districto'
                            });
                          }
                        } else {
                          Swal({
                            type: 'warning',
                            title: 'Validar',
                            text: 'Debe seleccionar una provincia'
                          });
                        }
                      } else {
                        Swal({
                          type: 'warning',
                          title: 'Validar',
                          text: 'Debe seleccionar un departamento'
                        });
                      }
                    } else {
                      Swal({
                        type: 'warning',
                        title: 'Validar',
                        text: 'Debe ingresar una dirección'
                      });
                    }
                  } else {
                    Swal({
                      type: 'warning',
                      title: 'Validar',
                      text: 'Debe seleccionar un tipo via'
                    });
                  }
                } else {
                  Swal({
                    type: 'warning',
                    title: 'Validar',
                    text: 'Debe seleccionar un tipo de dirección'
                  });
                }
              } else {
                Swal({
                  type: 'warning',
                  title: 'Validar',
                  text: 'Debe ingresar un Teléfono'
                });
              }
            } else {
              Swal({
                type: 'warning',
                title: 'Validar',
                text: 'Debe ingresar un Correo Electrónico'
              });
            }
          } else {
            Swal({
              type: 'warning',
              title: 'Validar',
              text: 'Debe ingresar un DNI, CE o Pasaporte'
            });
          }
        } else {
          Swal({
            type: 'warning',
            title: 'Validar',
            text: 'Debe seleccionar un tipo de documento'
          });
        }
      } else {
        Swal({
          type: 'warning',
          title: 'Validar',
          text: 'Debe ingresar un Nombre Contacto'
        });
      }
    }else {
      Swal({
        type: 'warning',
        title: 'Validar',
        text: 'Debe ingresar un RUC'
      });
    }
  } else {
    Swal({
      type: 'warning',
      title: 'Validar',
      text: 'Debe ingresar un Nombre Empresa'
    });
  }
  var company = {
    'name' : $("input[name='name']", $('#companyForm2')).val(),
    'ruc' : $("input[name='ruc']", $('#companyForm2')).val(),
    'contact' : $("input[name='contact']", $('#companyForm2')).val(),
    'identity_id' : $("select[name='identity_id']", $('#companyForm2')).val(),
    'dni' : $("input[name='dni']", $('#companyForm2')).val(),
    'email' : $("input[name='email']", $('#companyForm2')).val(),
    'phone' : $("input[name='phone']", $('#companyForm2')).val(),
    'direction_id' : $("select[name='direction_id']", $('#companyForm2')).val(),
    'way_id' : $("select[name='way_id']", $('#companyForm2')).val(),
    'address' : $("input[name='address']", $('#companyForm2')).val(),
    'district_id': $("select[name='district_id']", $('#companyForm2')).val(),
    'quotation_id' : $("input[name='quotation_id']", $('#companyForm2')).val(),
  };


  console.table(company);

  var data = JSON.stringify(company);


  var baseUrl = '{{ env("BASE_URL", url('/')) }}';

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
    }
  });

  $.ajax({
    type: "POST",
    enctype: 'multipart/form-data',
    url: baseUrl + "/companies_overstore",
    data: data,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    cache: false,
    timeout: 600000,
    success: function (data) {
      Swal({
        type: 'success',
        width: '40%',
        title: 'Gracias por su preferencia, iremos a la pantalla carga de datos básicos',
      }).then((result) => {
        if (result.value) {
          $("button[type='submit']").attr("disabled", true);
          $('#formBusiness4').modal('show');
        }
      });
    },
    error: function (e) {
            // $("#result").text(e.responseText);
            console.log("ERROR : ", e);
          }
        }).always(function(){
          // $('#spinner').fadeOut()
        });
      });
      //fin de 1-5
      //validar campos carga datos basicos
      $('#wrapped4').on('click', function(event) {
       if ($.trim($('#identity_id_date').val()).length) {
        if ($.trim($('#inputDni_date').val()).length) {
          if ($.trim($('#inputName_date').val()).length) {
           if ($.trim($('#inputApellido_date').val()).length) {
            if ($.trim($('#gender_id_date').val()).length) {
              if ($.trim($('#inputEmail_date').val()).length) {
                var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
                if (regex.test($('#inputEmail_date').val().trim())) {
                  Swal.fire({
                    type: 'info',
                    width: '40%',
                    title: 'Está conforme con la información ingresada?',
                    confirmButtonText:  'Si',
                    cancelButtonText:  'No',
                    showCancelButton: true,
                    showCloseButton: true,
                  }).then((result) => {
                    if (result.value) {
				            //$('#companyForm4').submit();
                    return true;
                  }
                  else if (result.dismiss === Swal.DismissReason.cancel)
                  {
                    return false;
                  }
                });
                } else {
                  Swal({
                    type: 'warning',
                    title: 'Validar',
                    text: 'La direccón de correo no es válida',
                  });
                }
              } else {
                Swal({
                  type: 'warning',
                  title: 'Validar',
                  text: 'Debe ingresar un correo',
                });
              }
            } else {
              Swal({
                type: 'warning',
                title: 'Validar',
                text: 'Debe seleccionar un genero',
              });
            }
          } else {
            Swal({
              type: 'warning',
              title: 'Validar',
              text: 'Debe ingresar el Apellido Paterno',
            });
          }
        } else {
          Swal({
            type: 'warning',
            title: 'Validar',
            text: 'Debe ingresar el Primer Nombre',
          });
        }
      } else {
        Swal({
          type: 'warning',
          title: 'Validar',
          text: 'Debe ingresar un Dni, CE y Pasaporte',
        });
      }
    }else {
      Swal({
        type: 'warning',
        title: 'Validar',
        text: 'Debe seleccionar un Tipo de Documento',
      });
    }
    var baseUrl = '{{ env("BASE_URL", url('/')) }}';

    $.ajax({
      type: "GET",
      url: baseUrl + "/store",
      data: $("#companyForm4").serialize(),

      success: function (data) {
        Swal({
          type: 'success',
          width: '40%',
          title: 'Fue realizado exitosamente la carga de los afiliados, ir a gestionar agendamiento!!',
          text: 'Le fue enviado un correo a los empleados para cargar sus datos requeridos y con más detalles sobre su cotización. ¡Muchas gracias!'

        }).then((result) => {
          if (result.value) {

            window.location.href = '/visits/' + $("input[name='quotation_id']").val();
          }

        });
      },
      error: function (e) {
            // $("#result").text(e.responseText);
            console.log("ERROR : ", e);
          }
        }).always(function(){
		          // $('#spinner').fadeOut()
		        });


         /* error: function (request, status, error) {
            alert(request.responseText);
          }
        }).always(function(){
          // $('#spinner').fadeOut()
        });*/

      });
      //fin validar campos basicos
      $("input[type=checkbox]").on( "click", function() {

        if ($(this).is(':checked')) {

        } else {
          $('#idInsured').val($("#check").val());
        }

      });
    });
//valida los combos de provinces, etc
$('.provinces').on('change', function() {
  if ($.trim($(this).val()).length) {
    $('.loading').show();
    $.get('/provinces/' + $(this).val(), function(data) {
      $('#provinces').html("<option value=''>Seleccionar</option>");
      $.each(data, function(index, item) {
        $("#provinces").append(
          $("<option></option>")
          .text(item.name)
          .val(item.id+'-'+item.delivery)
          );
      });
      $("#provinces").prop('disabled', false);
      $('.loading').fadeOut();
    });
  }
  else{
    $('#provinces').html("<option value=''>Seleccionar</option>");
    $("#provinces").prop('disabled', true);
    $('#district_id').html("<option value=''>Seleccionar</option>");
    $("#district_id").prop('disabled', true);
  }
});

$('.districts').on('change', function() {
  if ($.trim($(this).val()).length) {
    $('.loading').show();
    $.get('/districts/' + $(this).val(), function(data) {
      $('#district_id').html("<option value=''>Seleccionar</option>");
      $.each(data, function(index, item) {
        $("#district_id").append(
          $("<option></option>")
          .text(item.name)
                        //.val(item.id+'-'+item.delivery)
                        .val(item.id)
                        );
      });
      $("#district_id").prop('disabled', false);
      $('.loading').fadeOut();
    });
  }
  else{
    $('#district_id').html("<option value=''>Seleccionar</option>");
    $("#district_id").prop('disabled', true);
  }
});
//
//valida los combos de provinces, etc
$('.provinces').on('change', function() {
  if ($.trim($(this).val()).length) {
    $('.loading').show();
    $.get('/provinces/' + $(this).val(), function(data) {
      $('#provinces_two').html("<option value=''>Seleccionar</option>");
      $.each(data, function(index, item) {
        $("#provinces_two").append(
          $("<option></option>")
          .text(item.name)
          .val(item.id+'-'+item.delivery)
          );
      });
      $("#provinces_two").prop('disabled', false);
      $('.loading').fadeOut();
    });
  }
  else{
    $('#provinces_two').html("<option value=''>Seleccionar</option>");
    $("#provinces_two").prop('disabled', true);
    $('#district_id_two').html("<option value=''>Seleccionar</option>");
    $("#district_id_two").prop('disabled', true);
  }
});

$('.districts').on('change', function() {
  if ($.trim($(this).val()).length) {
    $('.loading').show();
    $.get('/districts/' + $(this).val(), function(data) {
      $('#district_id_two').html("<option value=''>Seleccionar</option>");
      $.each(data, function(index, item) {
        $("#district_id_two").append(
          $("<option></option>")
          .text(item.name)
                        //.val(item.id+'-'+item.delivery)
                        .val(item.id)
                        );
      });
      $("#district_id_two").prop('disabled', false);
      $('.loading').fadeOut();
    });
  }
  else{
    $('#district_id_two').html("<option value=''>Seleccionar</option>");
    $("#district_id_two").prop('disabled', true);
  }
});
//
//valida los combos de provinces, etc
$('.provinces').on('change', function() {
  if ($.trim($(this).val()).length) {
    $('.loading').show();
    $.get('/provinces/' + $(this).val(), function(data) {
      $('#provinces_tree').html("<option value=''>Seleccionar</option>");
      $.each(data, function(index, item) {
        $("#provinces_tree").append(
          $("<option></option>")
          .text(item.name)
          .val(item.id+'-'+item.delivery)
          );
      });
      $("#provinces_tree").prop('disabled', false);
      $('.loading').fadeOut();
    });
  }
  else{
    $('#provinces_tree').html("<option value=''>Seleccionar</option>");
    $("#provinces_tree").prop('disabled', true);
    $('#district_id_tree').html("<option value=''>Seleccionar</option>");
    $("#district_id_tree").prop('disabled', true);
  }
});

$('.districts').on('change', function() {
  if ($.trim($(this).val()).length) {
    $('.loading').show();
    $.get('/districts/' + $(this).val(), function(data) {
      $('#district_id_tree').html("<option value=''>Seleccionar</option>");
      $.each(data, function(index, item) {
        $("#district_id_tree").append(
          $("<option></option>")
          .text(item.name)
                        //.val(item.id+'-'+item.delivery)
                        .val(item.id)
                        );
      });
      $("#district_id_tree").prop('disabled', false);
      $('.loading').fadeOut();
    });
  }
  else{
    $('#district_id_tree').html("<option value=''>Seleccionar</option>");
    $("#district_id_tree").prop('disabled', true);
  }
});
//

</script>
@stop