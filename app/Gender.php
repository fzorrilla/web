<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    // public function details()
    // {
    // 	return $this->hasMany(Detail::class);
    // }

    public function details()
    {
        return $this->belongsToMany(Detail::class);
    }

    public function families()
    {
    	return $this->hasMany(Family::class);
    }            
}
