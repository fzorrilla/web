<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turn extends Model
{
    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }
     public function visits()
    {
        return $this->hasMany(Visit::class);
    }
}
