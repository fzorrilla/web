<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company_sctr extends Model
{
	protected $fillable = [

	    'contact',
	    'phone',
	    'email',
	    'complement_id',
	];
}
