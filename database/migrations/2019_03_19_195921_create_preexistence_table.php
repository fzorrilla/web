<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreexistenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preexistences', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pathologies_id');
            $table->unsignedInteger('families_id');
            $table->unsignedInteger('disease_id')->nullable();
            $table->string('end')->nullable();
            $table->string('doctor')->nullable();
            $table->string('year')->nullable();
            $table->string('situation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preexistences');
    }
}
