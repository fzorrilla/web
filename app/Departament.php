<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departament extends Model
{
    public function provinces()
    {
    	return $this->hasMany(Province::class);
    }

    public function districts()
    {
        $this->hasManyThrough(District::class, Province::class);
    }        
}
