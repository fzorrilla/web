<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identity extends Model
{
     public function families()
    {
    	return $this->hasMany(Family::class);
    }
}
