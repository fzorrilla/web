<?php

use Illuminate\Database\Seeder;

class DirectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $directions = [
            ['name' => 'Residencia'],
            ['name' => 'Oficina'],
            ['name' => 'Consultorio'],
            ['name' => 'Adicional'],
            ['name' => 'Direccion Fiscal'],
            ['name' => 'Casa'],
        ];

        foreach ($directions as $direction) {
            App\Direction::create($direction);
        }
    }
}
