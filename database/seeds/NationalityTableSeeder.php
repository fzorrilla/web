<?php

use Illuminate\Database\Seeder;

class NationalityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nationalities = [
            [
                'id' => '1',
                'name' => 'COLOMBIA',
            ],
            [
                'id' => '3',
                'name' => 'PERU',
            ],
            [
                'id' => '11',
                'name' => 'ESPAÑA',
            ],
            [
                'id' => '21',
                'name' => 'COSTA DE MARFIL',
            ],
            [
                'id' => '30',
                'name' => 'MEXICO',
            ],
            [
                'id' => '32',
                'name' => 'SUDAFRICA',
            ],
            [
                'id' => '38',
                'name' => 'COREA DEL NORTE',
            ],
            [
                'id' => '39',
                'name' => 'POLONIA',
            ],
            [
                'id' => '52',
                'name' => 'BAHAMAS',
            ],
            [
                'id' => '10',
                'name' => 'ARGENTINA',
            ],
            [
                'id' => '12',
                'name' => 'ETIOPIA',
            ],
            [
                'id' => '13',
                'name' => 'FRANCIA',
            ],
            [
                'id' => '26',
                'name' => 'ECUADOR',
            ],
            [
                'id' => '27',
                'name' => 'ESTADOS UNIDOS',
            ],
            [
                'id' => '28',
                'name' => 'REINO UNIDO',
            ],
            [
                'id' => '31',
                'name' => 'PORTUGAL',
            ],
            [
                'id' => '33',
                'name' => 'GUATEMALA',
            ],
            [
                'id' => '35',
                'name' => 'AUSTRALIA',
            ],
            [
                'id' => '6',
                'name' => 'CHILE',
            ],
            [
                'id' => '8',
                'name' => 'VENEZUELA',
            ],
            [
                'id' => '34',
                'name' => 'COSTA RICA',
            ],
            [
                'id' => '36',
                'name' => 'BRASIL',
            ],
             [
                'id' => '37',
                'name' => 'PANAMÁ',
            ],
             [
                'id' => '40',
                'name' => 'REPUBLICA CHECA',
            ],
             [
                'id' => '41',
                'name' => 'JORDANIA',
            ],
             [
                'id' => '46',
                'name' => 'DINAMARCA',
            ],
             [
                'id' => '4',
                'name' => 'ITALIA',
            ],
             [
                'id' => '5',
                'name' => 'CHINA',
            ],
             [
                'id' => '7',
                'name' => 'SUIZA',
            ],
             [
                'id' => '29',
                'name' => 'ALEMANIA',
            ],
             [
                'id' => '44',
                'name' => 'NUEVA ZELANDA',
            ],
             [
                'id' => '45',
                'name' => 'HONDURAS',
            ],
            [
                'id' => '47',
                'name' => 'JAPON',
            ],
            [
                'id' => '48',
                'name' => 'BELGICA',
            ],
            [
                'id' => '49',
                'name' => 'URUGUAY',
            ]
        ];

        foreach ($nationalities as $nationalitie) {
            App\Nationality::create($nationalitie);
        }
    }
}
