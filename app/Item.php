<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function plans()
    {
        return $this->belongsToMany(Plan::class);
    }
}
