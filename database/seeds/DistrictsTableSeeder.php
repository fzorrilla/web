<?php

// use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

// class DistrictsTableSeeder extends Seeder
class DistrictsTableSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = 'districts';
        $this->csv_delimiter = ';';
        $this->filename = base_path().'/database/seeds/csvs/districts.csv';
        $this->offset_rows = 1;
        $this->mapping = [
            0 => 'name',
            1 => 'province_id',
            2 => 'delivery',
        ];
        $this->should_trim = true;
    }

    public function run()
    {
        // $districts = [
        //     ['name' => 'Chachapoyas', 'province_id' => 1],
        //     ['name' => 'Asunción', 'province_id' => 1],
        //     ['name' => 'Balsas', 'province_id' => 1],
        //     ['name' => 'Cheto', 'province_id' => 1],
        //     ['name' => 'Chiliquin', 'province_id' => 1],
        //     ['name' => 'Chuquibamba', 'province_id' => 1],
        //     ['name' => 'Granada', 'province_id' => 1],
        //     ['name' => 'Huancas', 'province_id' => 1],
        //     ['name' => 'La Jalca', 'province_id' => 1],
        //     ['name' => 'Leimebamba', 'province_id' => 1],
        //     ['name' => 'Levanto', 'province_id' => 1],
        //     ['name' => 'Magdalena', 'province_id' => 1],
        //     ['name' => 'Mariscal Castilla', 'province_id' => 1],
        //     ['name' => 'Molinopampa', 'province_id' => 1],
        //     ['name' => 'Montevideo', 'province_id' => 1],
        //     ['name' => 'Olleros', 'province_id' => 1],
        //     ['name' => 'Quinjalca', 'province_id' => 1],
        //     ['name' => 'San Francisco de Daguas', 'province_id' => 1],
        //     ['name' => 'San Isidro de Maino', 'province_id' => 1],
        //     ['name' => 'Soloco', 'province_id' => 1],
        //     ['name' => 'Sonche', 'province_id' => 1],
        // ];

        // foreach ($districts as $district) {
        //     App\District::create($district);
        // }
        parent::run();
    }

}
