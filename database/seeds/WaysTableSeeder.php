<?php

use Illuminate\Database\Seeder;

class WayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ways = [
            ['name' => 'Plaza'],
            ['name' => 'Avenida'],
            ['name' => 'Autopista'],
            ['name' => 'Avenida calle'],
            ['name' => 'Avenida carrera'],
            ['name' => 'Carretera'],
            ['name' => 'Calle'],
            ['name' => 'Carrera'],
            ['name' => 'Diagonal'],
            ['name' => 'Kilometro'],
            ['name' => 'Transversal'],
            ['name' => 'Vereda'],
            ['name' => 'Barrio'],
            ['name' => 'Supermanzana'],
            ['name' => 'Torre'],
            ['name' => 'Edificio'],
            ['name' => 'Manzana'],
            ['name' => 'Centro Comercial'],
            ['name' => 'Invasión'],
            ['name' => 'Urbanización'],
            ['name' => 'Parque'],
            ['name' => 'Condominio'],
            ['name' => 'Finca'],
            ['name' => 'Circunvalar'],
            ['name' => 'Via'],
            ['name' => 'Jirón'],
            ['name' => 'Alameda'],
            ['name' => 'Bajada'],
            ['name' => 'Bulevar'],
            ['name' => 'Callejón'],
            ['name' => 'Geleria'],
            ['name' => 'Malecón'],
            ['name' => 'Ovalo'],
            ['name' => 'Pasaje'],
            ['name' => 'Paseo'],
            ['name' => 'Plazuela'],
            ['name' => 'Prolongación'],
            ['name' => 'Puente'],
        ];

        foreach ($ways as $way) {
            App\Way::create($way);
        }
    }
}
