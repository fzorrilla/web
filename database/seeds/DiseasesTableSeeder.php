<?php

// use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class DiseasesTableSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = 'diseases';
        $this->csv_delimiter = ';';
        $this->filename = base_path().'/database/seeds/csvs/diseases.csv';
        $this->offset_rows = 1;
        $this->mapping = [
            0 => 'name',
            1 => 'pathology_id',
        ];
        $this->should_trim = true;
    }

    public function run()
    {
     //

         parent::run();
    }
}
