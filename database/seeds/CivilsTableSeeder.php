<?php

use Illuminate\Database\Seeder;

class CivilsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $civils = [
            ['name' => 'Soltero/ra'],
            ['name' => 'Casado/da'],
            ['name' => 'Conviviente'],
            ['name' => 'Viudo/da'],
        ];

        foreach ($civils as $civil) {
            App\Civil::create($civil);
        }
    }
}
