<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/layout.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
    

    <!-- Scripts -->
    <script src="{{ asset('bootstrap/js/jquery.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/slick.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript" src="https://unpkg.com/popper.js/dist/"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.all.min.js"></script>
    <script src="https://unpkg.com/imask"></script>
</head>
</head>
<body>
    <!-- Navigation -->
    <div class="wrapper row0 sub-header">
      <div id="topbar" class="clear"> 
        <!-- ################################################################################################ -->
        <div class="fl_left">
          <ul>
            <li><i class="fab fa-facebook-f"></i></li>
            <li><i class="fab fa-twitter"></i></li>
            <li><i class="fab fa-youtube"></i></li>
          </ul>
        </div>
        <div class="fl_right">
          <ul>
            <li class="content-login">
                <i class="fas fa-lock"></i><a href="#">INGRESA O REGISTRATE</a>
            </li>
          </ul>
        </div> 
      </div>
    </div>
    <div class="container pt-4 pb-3">
        <div class="row main-mobile">
            <div class="text-left" style="cursor: pointer;width: 70%;" onclick="window.location.href='/'">
                <img src="{{ asset('img/socorro-logo2.png') }}">
            </div>
            <div class="col-md-9 col-xs-3 col-sm-3 pt-4" style="width: 30%;float: right;">
                <button class="main-mobile">
                    ---
                </button>
            </div>
        </div>
        <div class="row main-web">
            <div class="col-md-3 col-xs-9 col-sm-9 text-center" style="cursor: pointer;" onclick="window.location.href='/'">
                <img src="{{ asset('img/socorro-logo2.png') }}">
            </div>
            <div class="col-md-9 col-xs-3 col-sm-3 pt-4">
                <ul class="main">
                    <li class="seguros">
                        <a href="/seguros">SEGUROS</a>
                    </li>
                    <li class="socorro">
                        <a href="/socorro">SOCORRO</a>
                    </li>
                    <li>
                        <a href="#">BLOG</a>
                    </li>
                    <li>
                        <div class="btn-group ">
                          <button type="button" class="btn-cotiza dropdown-toggle"
                                  data-toggle="dropdown">
                            COTIZA
                          </button>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="/chatbot/1">Salud Independiente</a></li>
                            <li><a href="/chatbot/2">Salud Empresarial</a></li>
                          </ul>
                        </div>
                        
                    </li>
                    
                </ul>
                <button class="main-mobile">
                    ---
                </button>
            </div>
        </div>
    </div>

    @yield('content')

    @include('footer')

    @yield('js')
    <script type="text/javascript">
        $(window).ready(function(){
            var locat = window.location.pathname;
            if(locat == '/socorro')
                $("li.socorro").addClass("active");

            if(locat == '/seguroEmp' || locat == '/seguroInd' ||locat == '/seguros')
                $("li.seguros").addClass("active");

        })
        $(".btn-cotiza").click(function(){
            if(!$(".dropdown-menu").is(":visible"))
                $(".dropdown-menu").show("fade");
            else
                $(".dropdown-menu").hide("fade");
        })
    </script>
    <style type="text/css">
        .dropdown-menu{
            min-width: 14rem;
        }   
        .btn-cotiza{
            border: none !important;
            color: white;
        }
        button.btn-cotiza:focus {
            outline: 0px dotted;
            outline: 0px auto -webkit-focus-ring-color;
        }
        .dropdown-toggle::after {
            display: inline-block;
            margin-left: .255em;
            vertical-align: .255em;
            content: "";
            border-top: .0em solid;
            border-right: .0em solid transparent;
            border-bottom: 0;
            border-left: .0em solid transparent;
        }
    </style>
</body>
</html>
