<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
	public function age()
	{
		return $this->belongsTo(Age::class);
	}
	public function plan()
	{
		return $this->belongsTo(Plan::class);
	}

	// public function gender()
	// {
	// 	return $this->belongsTo(Gender::class);
	// }

    public function genders()
    {
        return $this->belongsToMany(Gender::class);
    }
}
