
<footer id="aa-footer">
  <div class="aa-footer-top">
   <div class="container">
      <div class="row">
        <div class="col-md-12 text-center" style="cursor: pointer;" onclick="window.location.href='/'">
          <img src="{{ asset('img/logofooter.png') }}">
        </div>
      </div>
      <div class="row mt-4 link-footer">
        <div class="col-md-8 offset-md-2">
          <div class="row">
            <div class="col-md-3 text-center">
              <a href="/seguros">Seguros</a>
            </div>
            <div class="col-md-3 text-center">
              <a href="/socorro">Socorro</a>
            </div>
            <div class="col-md-3 text-center">
              <a href="#">Blog</a>
            </div>
            <div class="col-md-3 text-center">
              <a href="#">Cotiza</a>
            </div>
          </div>
        </div>
      </div>
   </div>
   <div class="copy">
     <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
            Copyright 2019 - All Rights Reserved 
        </div>
      </div>

   </div>
  </div>
  <!-- footer-bottom -->
  <!--<div class="aa-footer-bottom">
    <div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="aa-footer-bottom-area">
          <p>Socorro <a href="http://www.markups.io/">MarkUps.io</a></p>
          <div class="aa-footer-payment">
            <span class="fa fa-cc-mastercard"></span>
            <span class="fa fa-cc-visa"></span>
            <span class="fa fa-paypal"></span>
            <span class="fa fa-cc-discover"></span>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>-->
</footer>
<style type="text/css">
  .link-footer a{
    color: white;
  }
  .link-footer a:hover{
    text-decoration: none;
  }
</style>