@extends('layouts.mob_new')

@section('title', 'Lima Lab Consulting Group')

@section('content') {{-- {{ dd($quotation) }} --}}
<div class="container culqui pb-5 mb-5">
    <div class="row title-pay">
        <div class="col-md-12 text-center">
           <h1 style="    font-family: Gotham-Bold !important;">Método de Pago</h1> 
        </div>
    </div>
    <form action="" method="POST" role="form" class="pl-5 pr-5">
    @csrf
        <div class="row">
            <div class="col-md-6 amount-placeholder-content">
                <br>
                <div class="amount-placeholder-text">
                    Monto a Pagar
                </div>
                <div class="amount-placeholder">
                    <span>S/</span>
                    <span>{{ round($quotation->total, 2) }}</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <img src="{{ asset('img/tarjetas/visa.jpeg') }}">
                    </div>
                    <div class="col-md-3">
                        <img src="{{ asset('img/tarjetas/mastercard.jpeg') }}">
                    </div>
                    <div class="col-md-3">
                        <img src="{{ asset('img/tarjetas/american.jpeg') }}">
                    </div>
                    <div class="col-md-3">
                        <img src="{{ asset('img/tarjetas/discover.jpeg') }}">
                    </div>
                    
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="radio-inline">
                            <label><input type="radio" id="dni" name="type" value="1" checked>DNI</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio-inline">
                            <label><input type="radio" id="ce" name="type" value="2" >CE</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio-inline">
                            <label><input type="radio" id="passport" name="type" value="3" >Pasaporte</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="NameOnCard">Número de Documento</label>
                    <input id="NameOnCard" name="NameOnCard" class="form-control" type="text" maxlength="255"></input>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="CreditCardNumber">Número de tarjeta</label>
                    <input type="text" data-culqi="card[number]" id="card[number]" name="number" class="form-control" pattern="[0-9]{16}" maxlength="16" title="Ingrese solo números" placeholder="Ingrese número de tarjeta sin espacios" required></input>
                </div>
            </div>
            <div class="col-md-4 pl-0 pr-0">
                <div class="row">
                    <div class="col-xs-4 pl-1 pr-1">
                        <div class="input-container">
                            <label for="card[exp_month]" class="control-label">Mes de Expiración</label>
                            <input type="number" class="form-control exp_month" data-culqi="card[exp_month]" id="card[exp_month]" placeholder="MM" required >
                        </div>
                    </div>
                    <div class="col-xs-4 pl-1 pr-1">
                        <div class="input-container">
                            <label for="card[exp_year]" class="control-label">Año de Expiración</label>
                            <input type="number" class="form-control exp_year" data-culqi="card[exp_year]" id="card[exp_year]" placeholder="AAAA" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                          <label for="name" class="control-label">Código de seguridad</label>
                          <div class="input-container">
                            <input type="number" class="form-control cvv" data-culqi="card[cvv]" id="card[cvv]" name="cvv" placeholder="CVC" required> <i id="cvc" class="fa fa-question-circle" style="position: relative;top: -27px;right: -123px;"></i>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="Email">Correo Electrónico</label>
                    <div class="input-container">
                        <input type="email" data-culqi="card[email]" id="card[email]" name="email" class="form-control" required>
                        <i class="fa fa-question-circle" style="position: relative;top: -27px;left: 97%;"></i>
                    </div>
                </div>
            </div>
        </div>
            <!--<div id="Checkout" class="inline">-->
        <div class="row">
    
            <div class="pad">
                <button type="submit" class="btn btn-small btn-block ml-4 mr-4">
                    <span class="submit-button-lock"></span>
                    <span class="align-middle">Pagar S/ {{ round($quotation->total, 2) }}</span>
                </button>
            </div>
            <input type="hidden" name="client" class="form-control" value="{{ $quotation->plan->client->name }}">
            <input type="hidden" name="plan" class="form-control" value="{{ $quotation->plan->name }}">
            <input type="hidden" name="plan_id" class="form-control" value="{{ $quotation->plan->id }}">
            <input type="hidden" name="amount" class="form-control" value="{{ round($quotation->total * 100, 2) }}">
            <input type="hidden" name="name" class="form-control" value="{{ $quotation->name }}">
            <input type="hidden" name="quotation_id" class="form-control" value="{{ $quotation->id }}">
        </div>
    </form>
</div>

  <!-- Modal -->
<div class="modal fade" id="formBusiness" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Body -->
            <div class="modal-body">
                <table border="0" cellpadding="0" cellspacing="0" width="570" style="font-family: Arial,Helvetica Neue,Helvetica,sans-serif;">
                    <tr>
                    <td align="left" width="100%">
                    <img src="http://ux.lealtad360.com/socorro/store/socorro.jpg" alt="Socorro" width="300" height="87" style="display: block; border: 0;">
                    </td>
                    </tr>
                <tr>
                <td width="100%" align="center" style="padding-top: 20px;">
                <h1 style="font-size: 28px; font-weight: 600; color: #2A2A2A;">Ops! No pudimos procesar tu pago, <br>¿lo intentamos de nuevo?</h1>
                <p style="font-size: 14px; font-weight: 400; color: #2A2A2A; width: 400px;"><output id="nombre">Juan</output>, por favor inténtalo con otro medio de pago, recuerda que también puedes realizar un pago en efectivo.</p>
                <a style="color: #FFF; font-size: 18px; padding: 12px 20px; background-color: #ff6b00; border-radius: 14px; margin-top: 30px; text-decoration: none; display: block;" href="#" data-dismiss="modal">Intentar nuevamente</a>
                </td>
                </tr>
                <tr>
                <td>
                <table width="100%" cellspacing="10px" style="padding-top: 30px;">
                <tr>
                <td width="30%" align="center" style="background-color: #ff6b00; border-radius: 20px; padding: 20px; color: #FFF; font-size: 20px; font-weight: 600;">Plan <output id="plan">Medisanitas</output><a href="#" style="color: #FFF; display: block; font-size: 14px; padding: 8px; background-color: #2A2A2A; border-radius: 10px; margin-top: 20px; text-decoration: none;">Más info +</a></td>
                <td width="70%" style="background-color: #CCC; border-radius: 20px; padding: 20px; margin-left: 10px;">
                <table border="1" bordercolor="white" width="100%" cellpadding="10px" cellspacing="0" style="font-size: 12px; margin-bottom: 20px;">
                <tr>
                <td width="50%">Titular - <output>35 a&ntilde;os</output></td>
                <td width="50%">S/205.00</td>
                </tr>
                <tr>
                <td width="50%">Cónyuge - <output>35 a&ntilde;os</output></td>
                <td width="50%">S/392.00</td>
                </tr>
                <tr>
                <td width="50%">Hijo - <output>10 a&ntilde;os</output></td>
                <td width="50%">S/170.00</td>
                </tr>
                <tr style="background-color: #ff6b00; color: #FFF; font-size: 14px; font-weight: 600;">
                <td width="50%">Total pagado</td>
                <td width="50%">S/767.00</td>
                </tr>
                </table>
                <hr>
                <p style="font-size: 12px;">Pago recurrente: <output>SI</output></p>
                <p style="font-size: 12px;">N° de tarjeta: <output>41-xxxxxxxxx23</output></p>
                <p style="font-size: 12px;">Frecuencia: <output>mensual</output></p>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                <tr>
                <td height="40px"></td>
                </tr>
                <tr>
                <td bgcolor="lightgrey" align="center" style="padding: 20px; font-size: 11px;">Estás suscrito con la siguiente dirección: sachuy7@hotmail.com.<br> Por favor, no respondas a este correo electrónico. <br>Para consultas visítanos contáctanos atraves de nuestras redes sociales.
                <table cellspacing="10px" style="padding-top: 10px;">
                <tr>
                <td><img src="http://ux.lealtad360.com/socorro/store/instagram.png" alt="Socorro" width="32" height="32" style="display: block; border: 0;"></td>
                <td><img src="http://ux.lealtad360.com/socorro/store/facebook.png" alt="Socorro" width="32" height="32" style="display: block; border: 0;"></td>
                <td><img src="http://ux.lealtad360.com/socorro/store/whatsapp.png" alt="Socorro" width="32" height="32" style="display: block; border: 0;"></td>
                </tr>
                </table>
                </td>
                </tr>
                </table>

            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --}}
                {{-- <button id="wrapped" type="button" class="btn btn-primary">Enviar</button> --}}
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
    <link href="{{ asset('css/payinvoice.css') }}" rel="stylesheet">
    <link href="{{ asset('css/loading.css') }}" rel="stylesheet">
    <style type="text/css">
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }
    input[type=number] { -moz-appearance:textfield; }
    .pad {
        padding: 0 15px;
    }
  </style>
@stop

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.all.min.js"></script>
    <script src="https://checkout.culqi.com/v2"></script>
    <script type="text/javascript">
        Culqi.publicKey = 'pk_test_l5ac4bbVFSSenqLf';
        Culqi.init();
    </script>
    <script type="text/javascript">

        function culqi() {
            if (Culqi.token) { // ¡Objeto Token creado exitosamente!

                var token = Culqi.token.id;
                var email = Culqi.token.email;

                var payment = {
                    'token_id' : token,
                    'name' : $("input[name='name']").val(),
                    'client' : $("input[name='client']").val(),
                    'plan' : $("input[name='plan']").val(),
                    'amount' : $("input[name='amount']").val(),
                    'quotation_id' : $("input[name='quotation_id']").val(),
                    'DocumentNumber': $("input[name='DocumentNumber']").val(),
                    'email' : email,
                };

                var data = JSON.stringify(payment);

                var baseUrl = '{{ env("BASE_URL", url('/')) }}';

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').val()
                    }
                });

                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: baseUrl + "/payment/culqi",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    // processData: false,
                    // contentType: false,
                    cache: false,
                    timeout: 600000,

                    success: function (data) {

                        const swalWithBootstrapButtons = Swal.mixin({
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-primary',
                        buttonsStyling: false,
                        })

                        swalWithBootstrapButtons.fire({
                        title: 'Estimado '+ $("input[name='name']").val(),
                        text: "¿Deseas que te afiliemos al pago recurrente para debitarte mensualmente tu prima?",
                        type: 'succes',
                        showCancelButton: true,
                        confirmButtonText: 'Si, por favor!',
                        cancelButtonText: 'No, Gracias!',
                        reverseButtons: true

                        }).then((result) => {
                            if (result.value) {
                              Swal.fire({
                                    title: 'Tu compra se realizo de manera exitosa! Ahora procederemos a completar los formularios de afiliación. Un paso mas para obtener tu contrato.',
                                    text: data.outcome.user_message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Ok'
                                }).then((result) => {
                                    if (result.value) {
                                        window.location.href = '/affiliation/' + $("input[name='quotation_id']").val();
                                    }
                                });
                                console.table( data.outcome );
                            }
                            else if (result.dismiss === Swal.DismissReason.cancel) {
                                Swal.fire({
                                    title: 'Tu compra se realizo de manera exitosa! Ahora procederemos a completar los formularios de afiliación. Un paso mas para obtener tu contrato.',
                                    text: data.outcome.user_message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Ok'
                                }).then((result) => {
                                    if (result.value) {
                                        window.location.href = '/affiliation/' + $("input[name='quotation_id']").val();
                                    }
                                });
                                console.table( data.outcome );
                            }
                        })

                    },
                    error: function (e) {

                        $('#formBusiness').modal('show');
                        // Swal({
                        //     type: 'error',
                        //     title: 'Transacción Fallida',
                        //     text: e.responseJSON.message
                        // });
                        // $("#result").text(e.responseText);
                        // console.log("ERROR : ", e);
                        console.table( e.responseJSON.message );
                    }
                }).always(function(){
                    // $('#spinner').fadeOut()
                    $('.loading').fadeOut();
                });

                // alert('Se ha creado un token:' + token);
            } else { // ¡Hubo algún problema!
                $('.loading').fadeOut();
                // Mostramos JSON de objeto error en consola
                // console.log(Culqi.error);
                Swal({
                    type: 'error',
                    title: 'Transacción Fallida',
                    text: Culqi.error.user_message
                });
                alert(Culqi.error.user_message);

            }
        };

        $(document).ready(function() {

            $('form').submit(function(e) {

                e.preventDefault();
                var year = new Date().getFullYear() - 1;

                if ($.trim($('.cvv').val()).length == 3) {
                    if ($.trim($('.exp_month').val()).length == 2) {
                        if ($('.exp_month').val() >= 1 && $('.exp_month').val() <= 12) {
                            if ($.trim($('.exp_year').val()).length == 4) {
                                if ($('.exp_year').val() > year) {
                                    $('.loading').show();
                                     Culqi.createToken();
                                   // window.location.href = '/affiliation/' + $("input[name='quotation_id']").val();
                                } else {
                                    Swal({
                                        type: 'error',
                                        title: 'Validar',
                                        text: 'El AÑO debe ser mayor a ' + year
                                    });
                                }
                            } else {
                                Swal({
                                    type: 'error',
                                    title: 'Validar',
                                    text: 'Valor del AÑO deb ser de 4 dígitos'
                                });
                            }
                        } else {
                            Swal({
                                type: 'error',
                                title: 'Validar',
                                text: 'El Rango de MES debe estar comprendido entre 01 y 12'
                            });
                        }
                    } else {
                        Swal({
                            type: 'error',
                            title: 'Validar',
                            text: 'Valor del MES deb ser de 2 dígitos'
                        });
                    }
                } else {
                    Swal({
                        type: 'error',
                        title: 'Validar',
                        text: 'Valor de CVV debe ser de 3 dígitos'
                    });
                }

                // Swal({
                //     title: 'Afiliación pago recurrente',
                //     text: '¿'+ $(this).attr('data-name') +', deseas que te afiliemos para debitarte mensualmente tu prima?',
                //     type: 'warning',
                //     showCancelButton: true,
                //     confirmButtonColor: '#3085d6',
                //     cancelButtonColor: '#d33',
                //     confirmButtonText: 'Si, por favor',
                //     cancelButtonText: 'No, gracias'
                // }).then((result) => {
                //     if (result.value) {
                //         Swal(
                //             'Aviso!',
                //             'Será redireccionado a un formulario para autorización de pago recurrente.',
                //             'success'
                //         )
                //     } else {
                //         window.location.href = $(this).attr('data-url');
                //     }
                // });
                // return false;
            });

            // Request to Sanitas Perú
            $("input[name='DocumentNumber']").on('blur', function(event) {

                if ($.trim($(this).val()).length == 8) {

                    $('.loading').show();

                    var baseUrl = '{{ env("BASE_URL", url('/')) }}';
                    var type = $("input[name='type']").val();
                    var number = $("input[name='DocumentNumber']").val();

                    var defaulter = $.getJSON(baseUrl + "/defaulters/numbers/" + number + "/types/" + type);

                    $.when(defaulter).done(function(data) {
                    // defaulter.then(function(data) {

                        // if (!data.PersonStatus) {
                        //     $("#relationship").prop("disabled", false);
                        // }
                        // $("#relationship option[value="+ data.Relationship +"]").attr('selected', 'selected');
                        if (!data.sell) {
                            $("form :input").prop({"disabled": !data.sell});
                            $("#ce").prop("disabled", false);
                            $("#passport").prop("disabled", false);
                            $("input[name='DocumentNumber']").prop("disabled", false);
                            // $("input[name='DocumentNumber']").focus();
                            Swal(
                                'Error',
                                'Gracias por cotizar, por el momento no puedes contratar un plan con nosotros. Nos contactaremos a la brevedad posible.',
                                'warning'
                            )
                            // window.location.href =
                        } else {
                            $("form :input").prop({"disabled": !data.sell});
                            $("#ce").prop("disabled", true);
                            $("#passport").prop("disabled", true);
                            $("input[name='number']").focus();
                        }
                        $('.loading').fadeOut();
                    });
                }
            });
        });
  </script>
@stop
