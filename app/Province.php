<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public function departament()
    {
        return $this->belongsTo(Departament::class);
    }

    public function districts()
    {
        return $this->hasMany(District::class);
    }    
}
