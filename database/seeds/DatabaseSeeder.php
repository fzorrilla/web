<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(PlansTableSeeder::class);
        $this->call(InsuredsTableSeeder::class);
        $this->call(GendersTableSeeder::class);
        $this->call(AgesTableSeeder::class);
        $this->call(DetailsTableSeeder::class);
        $this->call(PathologiesTableSeeder::class);
       // $this->call(DiseasesTableSeeder::class);
        $this->call(AffiliatesTableSeeder::class);
      //  $this->call(DepartamentsTableSeeder::class);
       // $this->call(ProvincesTableSeeder::class);
       // $this->call(DistrictsTableSeeder::class);
        $this->call(RelationshipsTableSeeder::class);
        $this->call(TurnsTableSeeder::class);
        $this->call(ItemsTableSeeder::class);
        $this->call(IdentityTableSeeder::class);
        $this->call(NationalityTableSeeder::class);
        $this->call(CivilsTableSeeder::class);
        $this->call(DirectionTableSeeder::class);
        $this->call(WayTableSeeder::class);
        $this->call(CountryTableSeeder::class);
    }
}
