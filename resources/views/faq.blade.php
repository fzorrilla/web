<div class="pt-5" style="background: #e2e2e252;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 pb-5">
        <h2 class="title-section" style="margin-right: 80px;">
          PREGUNTAS QUE SIEMPRE ME HACEN
        </h2>
        <br>
        <hr class="borde-naranja">
      </div>
    </div>
    
    <p class="pt-4"></p>
    <div class="row">
      <div class="col-md-12">
        <div id="accordion" class="faq-socorro">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link btn-pregunta" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <img src="{{ asset('img/plus.png') }}">¿Qué es un EPS?
                </button>
              </h5>
            </div>

            <div id="collapseOne" class="collapse text-left" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                  Un EPS es una Entidad Prestadora de Salud y se encarga de administrar tu plan de salud y ofrecerte servicios médicos a través de las clínicas y otras instituciones de salud afiliadas.
                </div>
              </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link btn-pregunta collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <img src="{{ asset('img/plus.png') }}">¿Qué es un Plan de Salud?
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                Un Plan de Salud es un convenio que se tiene entre la EPS y sus usuarios (ustedes) donde se especifica las coberturas, exclusiones, copagos y red de clínicas disponibles a los usuarios (ustedes).
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed btn-pregunta" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  <img src="{{ asset('img/plus.png') }}">¿Existe límite de edad para afiliarme a un Plan de Salud?
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                No, la edad no es una limitación.
              </div>
            </div>
          </div>
        </div>
        <p class="text-center pb-5 mb-5">
          <div class="pb-5 mb-5">
            <button class="ml-auto mr-auto active text-white pt-2 pb-2 pl-4 pr-4" onclick="window.location.href='/faq'">¿TIENES MAS PREGUNTAS?</button>
          </div>
        </p>
      </div>

    </div>
    
    
  
  </div>
</div>