<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	protected $fillable = [
	    'name',
	    'ruc',
	    'contact',
	    'identity_id',
	    'dni',
	    'email',
	    'phone',
	    'direction_id',
	    'way_id',
	    'address',
	    'district_id',
	    'quotation_id',
	];
}
