<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('client_id');
            $table->enum('children', ['0', '1'])->default('0');
            $table->integer('worker')->nullable();
            $table->integer('dependent')->nullable();
            $table->float('amount')->nullable();
            $table->string('transaction')->nullable();
            $table->string('email')->nullable();
            $table->float('total')->nullable();
            // $table->enum('paid', ['0', '1'])->default('0');
            $table->boolean('paid')->default(false);
            $table->unsignedInteger('plan_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
