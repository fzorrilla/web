@extends('layouts.mob')

@section('title', 'SOCORRO WEB')

@section('content')
<div style="background: #FFF;">
  <div style="background: #e2e2e252;">
    <div class="row">
      <div class="offset-md-1 col-md-10">
        <h2 class="title-section">SEGUROS </h2>
        <br>
        <hr class="borde-naranja">
      </div>
    </div>
  </div>
  <div class="container">  
    <div class="row">
      <div class="col-md-12 text-center mt-5">
        <img src="{{ asset('img/iconempresarial.png') }}">
      </div>
      <div class="col-md-12 text-center mt-3">
        <h2 style="color: #8ea1b2;font-family: Gotham-Bold;text-transform: none;font-size: 42px;">
          Salud Empresarial
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
        <img src="{{ asset('img/SALUDEMPRESARIAL.png') }}">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center mt-5">
        <p style="color: #8ea1b2;font-size: 18px;font-weight: 500">
          Asegura la salud de tus colaboradores asegura el éxito de tus proyectos.<br>Elige el plan a la medida de tu empresa.
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center mt-5">
        <h2 style="color: #8ea1b2;font-family: Gotham-Bold;text-transform: none;font-size: 36px;">
          Esencial Empresarial
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 text-center mt-5">

      </div>
      <div class="title-height-first col-md-2 text-center mt-5 border-left border-top background-1">
        BASE
      </div>
      <div class="title-height-first col-md-2 text-center mt-5 border-top background-2">
        ADICIONAL 1
      </div>
      <div class="title-height-first col-md-2 text-center mt-5 border-top background-3">
        ADICIONAL 2
      </div>
      <div class="title-height-first col-md-2 text-center mt-5 border-top border-right background-4">
        ADICIONAL 3
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Cobertura Obligatoria
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Cobertura Complementaria
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Chequeo Preventivo Anual
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Cobertura Odontología
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Cobertura Oftalmológica
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Clínicas Disponibles
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        25
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        95
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        107
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        114
      </div>
      <div class="text-number col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Reembolsos
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Emergencias
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        100%
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Maternidad
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        100%
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Medicinas (hasta)
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        70%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        100%
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        100%
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Cobertura Oncológica
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Cobertura Internacional
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Asistencia de Viaje
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height" style="padding-top: 1px;">
        Precio de Plan Mensual <br><span>(adicionales)</span>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        73
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        74
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        33
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        80
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        Precio de Plan Mensual*
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        73
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        147
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        180
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        260
      </div>
      <div class="col-md-4 text-right border-top border-bottom border-left border-right title-height">
        ELIGE TU PLAN:
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-1">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/2'">
          COTIZA
        </button>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-2">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/2'">
          COTIZA
        </button>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom background-3">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/2'">
          COTIZA
        </button>
      </div>
      <div class="text-number col-md-2 text-center vertical-align border-top border-bottom border-right background-4">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/2'">
          COTIZA
        </button>
      </div>
    </div>
    <div class="row">
      <div class="content-colet col-md-6">
        <span>(*) En base a un plan con mas de 10 afiliados</span>
        <span>(**) Zonas alejadas de provincias</span>
        <span>(***) Reembolsos en Lima segun plan</span>  
      </div>
      <div class="content-colet col-md-6 text-right">
         <span style="display: inline;">Lista de clínicas según el plan: </span><span class="clinic">Clínicas autorizadas</span>
      </div>
      
    </div>
    <div class="row">
      <div class="col-md-12 text-center mt-5">
        <h2 style="color: #8ea1b2;font-family: Gotham-Bold;text-transform: none;font-size: 36px;">
          VIP Empresarial
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 text-center mt-5">

      </div>
      <div class="title-height-first col-md-3 text-center mt-5 border-left border-top background-1">
        BASE
      </div>
      <div class="title-height-first col-md-3 text-center mt-5 border-top background-2">
        ADICIONAL 1
      </div>
      <div class="title-height-first col-md-3 text-center mt-5 border-top background-3">
        ADICIONAL 2
      </div>
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Cobertura Obligatoria
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Cobertura Complementaria
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Chequeo Preventivo Anual
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Cobertura Odontología
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Cobertura Oftalmológica
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Clínicas Disponibles
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-1">
        25
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-2">
        95
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-3">
        107
      </div>
      
      <div class="text-number col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Reembolsos
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Emergencias
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-1">
        100%
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-2">
        100%
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-3">
        100%
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Maternidad
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-1">
        100%
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-2">
        100%
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-3">
        100%
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Medicinas (hasta)
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-1">
        70%
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-2">
        100%
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-3">
        100%
      </div>
     
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Cobertura Oncológica
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      <div class="col-md-3 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkv.png') }}">
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Cobertura Internacional
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Asistencia de Viaje
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-1">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-2">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-3">
        <img src="{{ asset('img/checkx.png') }}">
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2" style="padding-top: 1px;">
        Precio de Plan Mensual <br><span>(adicionales)</span>
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-1">
        73
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-2">
        74
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-3">
        33
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        Precio de Plan Mensual*
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-1">
        73
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-2">
        147
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-3">
        180
      </div>
      
      <div class="col-md-3 text-right border-top border-bottom border-left border-right title-height-2">
        ELIGE TU PLAN:
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-1">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/2'">
          COTIZA
        </button>
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-2">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/2'">
          COTIZA
        </button>
      </div>
      <div class="text-number col-md-3 text-center vertical-align border-top border-bottom background-3">
        <button type="button" class="btn-cotiza dropdown-toggle" onclick="window.location.href='/chatbot/2'">
          COTIZA
        </button>
      </div>
    </div>
    <div class="row">
      <div class="content-colet col-md-6">
        <span>(*) En base a un plan con mas de 10 afiliados</span>
        <span>(**) Zonas alejadas de provincias</span>
        <span>(***) Reembolsos en Lima segun plan</span>  
      </div>
      <div class="content-colet col-md-6 text-right">
         <span style="display: inline;">Lista de clínicas según el plan: </span><span class="clinic">Clínicas autorizadas</span>
      </div>
      
    </div>
  </div>
</div>
<br><br>
<div style="background: #e2e2e252;" class="pb-5">
  <div class="container">
    <div class="row">
      <div class="offset-md-1 col-md-10">
        <h2 class="title-section">EPS PARA EMPRESAS </h2>
        <br>
        <hr class="borde-naranja">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 text-center background-1 border-left border-top border-bottom border-right title-height-first">
        Etapa
      </div>
      <div class="col-md-4 text-center background-2 border-top border-bottom  title-height-first">
        ¿Qué sucede en esta etapa?
      </div>
      <div class="col-md-4 text-center background-3 border-top border-bottom border-right title-height-first">
        Tiempos
      </div>
      <div class="col-md-4 text-center background-1 border-left border-bottom border-right title-height-3">
        Invitación a la EPS
      </div>
      <div class="col-md-4 text-left background-2 border-bottom title-height-3">
        Debes inviarnos una carta invitándonos a participar en el proceso. 
        <br>
        Recuerda que debes invitar a no menos de 2 empresas al mismo tiempo. De lo contrario tu proceso puede ser invalidado.
        <br>
        Detalles de la carta y formato aquí.
      </div>
      <div class="col-md-4 text-center background-3 border-bottom  title-height-3">
        
      </div>
      <div class="col-md-4 text-center background-1 border-left border-bottom border-right title-height-3">
        Presentación y evaluación de propuestas
      </div>
      <div class="col-md-4 text-left background-2 border-bottom title-height-3">
        Nosotros y otras aseguradoras te enviaremos nuestras propuestas y planes de salud.
      </div>
      <div class="col-md-4 text-center background-3 border-bottom  title-height-3">
        5 días desde la invitación
      </div>
      <div class="col-md-4 text-center background-1 border-left border-bottom border-right title-height-3">
        Difusión de propuestas
      </div>
      <div class="col-md-4 text-left background-2 border-bottom title-height-3">
        Lo que debes hacer es compararla y distribuirla entre tus colaboradores para que hagan su elección.
      </div>
      <div class="col-md-4 text-center background-3 border-bottom  title-height-3">
        
      </div>
      <div class="col-md-4 text-center background-1 border-left border-bottom border-right title-height-3">
        Recolección, conteo de votos y acta de escrutinio
      </div>
      <div class="col-md-4 text-left background-2 border-bottom title-height-3">
        Tus colaboradores eligen a través de una votación el plan de salud que más les guste. Mayoría de votos gana.<br>

        Modelo del acta de escrutinio aquí.
      </div>
      <div class="col-md-4 text-center background-3 border-bottom  title-height-3">
        Al día siguiente hábil, se debe comunicar a las EPS participantes el resultado.
      </div>
      <div class="col-md-4 text-center background-1 border-left border-bottom border-right title-height-3">
        Publicación de resultados
      </div>
      <div class="col-md-4 text-left background-2 border-bottom title-height-3">
        Debes publicar los resultados del proceso en lugares públicos.
      </div>
      <div class="col-md-4 text-center background-3 border-bottom  title-height-3">
        Durante los 3 días hábiles siguientes.
      </div>
      <div class="col-md-4 text-center background-1 border-left border-bottom border-right title-height-3">
        Suscripción del contrato
      </div>
      <div class="col-md-4 text-left background-2 border-bottom title-height-3">
        Firmaremos el contrato.
      </div>
      <div class="col-md-4 text-center background-3 border-bottom  title-height-3">
        
      </div>

    </div>
    <h2 class="mt-4 mb-1" style="color: #8ea1b2;font-family: Montserrat-Regular;font-size: 20px;text-transform: none;">
      ¿Que sigue después?
    </h2>
    <h2 class="mt-0 mb-5" style="color: #8ea1b2;font-family: Montserrat-Regular;font-size: 20px;text-transform: none;">
      Estaremos coordinando contigo el proceso de afiliación de tus colaboradores al plan.
    </h2>
    <h2 class="mt-0" style="color: #8ea1b2;font-family: Montserrat-Regular;font-size: 20px;text-transform: none;">
      Contáctanos a <span style="color: #0a62a2;">socorro@socorro.pe</span> para ayudarte en este proceso.
    </h2>
  </div>
  
</div>
<div style="background: #FFF;" class="pt-5 mt-5">
  <div class="container mt-3 mb-5 pb-5">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title-section">CONOCE MAS SOBRE</h2>
        <br>
        <hr class="borde-naranja">
      </div>
    </div>
    <div class="row">
        <div class="col-md-6 text-center seguros-one segurosoneHover">
            <div id="overlayone"></div>
            <div class=" text-center"  style="position: relative;z-index: 3">
              <img src="{{ asset('img/icon1.png') }}">
            </div>
            <div class="pt-3" style="position: relative;z-index: 3">
              <h2>Salud Independiente</h2>
            </div>
            <div>
              <button class="ml-auto mr-auto active" onclick="window.location.href='/seguroInd'">MÁS INFORMACION</button>
            </div>
          
        </div>
        <div class="col-md-6 text-center seguros-two">
            <div id="overlaytwo"></div>
            <div class=" text-center" style="position: relative;z-index: 3">
              <img src="{{ asset('img/icon2.png') }}">
            </div>
            <div class="pt-3" style="position: relative;z-index: 3">
              <h2>Salud Empresarial</h2>
            </div>
            <div>
              <button class="ml-auto mr-auto active" onclick="window.location.href='/seguroEmp'">MÁS INFORMACION</button>
            </div>
        </div>
    </div>
  </div>
  
</div>
<script type="text/javascript">
  $(".seguros-one").hover(function(){
      $("#overlayone").show("slow");
  },function(){
      $("#overlayone").hide("slow");
  });
  $(".seguros-two").hover(function(){
      $("#overlaytwo").show("slow");
  },function(){
      $("#overlaytwo").hide("slow");
  });
  $(".seguros-three").hover(function(){
      $("#overlaythree").show("slow");
  },function(){
      $("#overlaythree").hide("slow");
  });
  $(".seguros-four").hover(function(){
      $("#overlayfour").show("slow");
  },function(){
      $("#overlayfour").hide("slow");
  });
  

</script>
@endsection