@extends('layouts.mob')

@section('title', 'SOCORRO WEB')

@section('content')

<form id="quotationForm" action="{{ route('response') }}" autocomplete="false" method="POST" role="form">
  @csrf
    <div class="chatbot">
        <div class="text-center">
            <img src="{{ asset('img/socorro_personaje4.png') }}" alt="" class="chatbot__image">
            <p class="chatbot__name">Hola! Soy Socorro</p>
        </div>

        <div class="chatbot__conversation" id="chat1">
            <img src="{{ asset('img/socorro_personaje4.png') }}" class="chatbot__icon">
            <span class="chatbot__message" id="chat1-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat2">
            <span class="chatbot__message" id="chat2-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat3">
            <span class="chatbot__message chatbot__message__response" id="chat3-message" style="margin-left: auto;">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat4">
            <span class="chatbot__message" id="chat4-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat4-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat5-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat5">
            <span class="chatbot__message" id="chat6-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat6-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat7-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat7">
            <span class="chatbot__message" id="chat8-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat9-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat10-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat10">
            <span class="chatbot__message" id="chat11-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat11">
            <span class="chatbot__message" id="chat12-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat12-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat13-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat13">
            <span class="chatbot__message" id="chat14-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat14-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat15-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat16">
            <span class="chatbot__message" id="chat17-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat17-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat18-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat15">
            <span class="chatbot__message" id="chat16-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat18-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat19-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat19">
            <span class="chatbot__message" id="chat20-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat20-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat21-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat21">
            <span class="chatbot__message" id="chat22-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat22">
            <span class="chatbot__message" id="chat23-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat23-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat24-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat24">
            <span class="chatbot__message" id="chat25-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat25-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat26-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat28-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat29-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat26">
            <span class="chatbot__message" id="chat27-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat27-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat28-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat29">
            <span class="chatbot__message" id="chat30-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat30-respuesta">
            <span class="chatbot__message chatbot__message__response" id="chat311-message"></span>
        </div>

        <div class="chatbot__conversation chat__hidden" id="chat31">
            <span class="chatbot__message" id="chat32-message">
                <div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>
            </span>
        </div>

        <div id="question__section">
            <div class="question__user_1 chat__hidden">
                <input type="text" placeholder="Ingresar nombre" id="question-name" class="questions__user form-control">
                <img src="{{ asset('img/activeboton.png') }}" class="flecha-derecha" id="respuesta-1">
            </div>

            <div class="question__user_2 chat__hidden">
                @if ($client == '1')
                    <img src="{{ asset('img/hombre.png') }}" id="respuesta-2-person" class="person-image" value="person">
                @else
                    <img src="{{ asset('img/icono1.png') }}" id="respuesta-2-business" class="business-image" value='business'>
                @endif
            </div>

            <div class="question__user_3-person chat__hidden">

                <div data-type="1" data-detail="Plan Salud" class="insurance plan-salud-person">
                    <img src="{{ asset('img/icono3.png') }}">
                    <span>Plan Salud</span>
                </div>

                <div data-type="2" data-detail="Riesgo de Trabajo" class="insurance riesgo-trabajo-person">
                    <img src="{{ asset('img/icono2.png') }}">
                    <span>Riesgo de Trabajo</span>
                </div>
            </div>

            <div class="question__user_3-business chat__hidden">
                {{-- <div onclick="insuranceDetailTypeBusiness('Plan Salud')"> --}}
                <div data-type="1" data-detail="Plan Salud" class="insurance plan-salud-person">
                    <img src="{{ asset('img/icono3.png') }}">
                    <span>Plan Salud</span>
                </div>

                <div>
                    <img src="{{ asset('img/icono2.png') }}">
                    <span>Riesgo de Trabajo</span>
                </div>

                <div>
                    <img src="{{ asset('img/icono2.png') }}">
                    <span>Vida Ley</span>
                </div>

                <div>
                    <img src="{{ asset('img/icono2.png') }}">
                    <span>Vida Ley Pensión</span>
                </div>
            </div>

            <div class="question__user_4 chat__hidden">
                <input type="text" name="bod" placeholder="DD/MM/AAAA" id="datepicker2" class="js-date-birth questions__user form-control" pattern="\d{1,2}/\d{1,2}/\d{4}">
                <img src="{{ asset('img/activeboton.png') }}" class="flecha-derecha" id="date">
            </div>

            <div class="question__user_5 chat__hidden">
                <img src="{{ asset('img/hombre.png') }}" class="gender male mr-4" data-type="1" data-detail="Masculino">
                <img src="{{ asset('img/mujer.png') }}" class="gender female" data-type='2' data-detail="Femenino">
                <div style="text-align:center">
                    <span class="dot dot-male gender" data-type="1" data-detail="Masculino"></span>
                    <span class="dot dot-female gender" data-type='2' data-detail="Femenino"></span>
                </div>
            </div>

            <div class="question__user_6 chat__hidden">
                <img src="{{ asset('img/hombre.png') }}" class="married soltero mr-4" data-type="0" data-detail="Soltero">
                <img src="{{ asset('img/casado.png') }}" class="married casado" data-type="1" data-detail="Casado">
            </div>

            <div class="question__user_7 chat__hidden">
                <span class="children like" data-type="1" data-detail="Sí">SI</span>
                <span class="children dislike" data-type="0" data-detail="No">NO</span>
            </div>

            <div class="question__user_8 chat__hidden row">
                @foreach ($partners as $partner)
                <div class="content-range-year">
                    
                    <div data-min="{{ $partner->min }}" data-max="{{ $partner->max }}" data-description="{{ $partner->description }}" class="age-partner range-year col-md-4">
                        {{ $partner->description }}
                    </div>
                </div>
                @endforeach
            </div>

            <div class="question__user_9 chat__hidden">
                <input type="text" name="quantity" id="question-number-children" class="questions__user form-control" placeholder="Cantidad de Hijos">
                <img src="{{ asset('img/activeboton.png') }}" class="qty_children flecha-derecha" id="question-number-children-flecha" style="margin-top: 18px;">
            </div>

            <div class="question__user_10 chat__hidden">
                <div class="question__user_10_list pre-scrollable" style="width: 100%;height: 70px;"></div>
                <img src="{{ asset('img/activeboton.png') }}" class="next flecha-derecha" style="    margin-top: 18px;" id="catnChild"> 
            </div>

            <div class="question__user_11 chat__hidden">
                <input type="range" min="0" max="10000" step="1000" value="2000" class="slider" id="range" name="range">
                <img src="{{ asset('img/activeboton.png') }}" class="range flecha-derecha">
            </div>

            <div class="question__user_12 chat__hidden">
                <img src="{{ asset('img/like.png') }}" class="health like" data-type="1" data-detail="Sí">
                <img src="{{ asset('img/dislike.png') }}"  class="health dislike" data-type="0" data-detail="No">
            </div>

            <div class="question__user_13 chat__hidden">
                <input type="number" name="qty_worker" min="1" max="100" id="workers_number" class="questions__user" placeholder="Nro. de Colaboradores">
                <img src="{{ asset('img/activeboton.png') }}" class="worker flecha-derecha" id="respuesta-workers-number">
            </div>

            <div class="question__user_14 chat__hidden">
                <input type="number" name="qty_dependent" min="1" max="100" id="workers_dependency_number" class="questions__user" placeholder="Nro. de Dependientes">
                <img src="{{ asset('img/activeboton.png') }}" class="dependent flecha-derecha">
            </div>

            <div class="question__user_15 chat__hidden">
                <div class="append_dependency_worker_list">

                </div>
                <img src="{{ asset('img/activeboton.png') }}" class="nextQ3 flecha-derecha">
            </div>

            <div class="question__user_16 chat__hidden">
                <input type="number" name="qty_contribute" id="aporte_salud" class="questions__user" placeholder="Aporte ESSALUD">
                <img src="{{ asset('img/activeboton.png') }}" class="nextQ4 flecha-derecha">
            </div>
        </div>
    </div>
    <input type="hidden" name="name" class="form-control">
    <input type="hidden" name="client" class="form-control" value="{{ $client }}">
    <input type="hidden" name="insurance" class="form-control">
    <input type="hidden" name="gender" class="form-control">
    <input type="hidden" name="married" class="form-control">
    <input type="hidden" name="children" class="form-control">
    <input type="hidden" name="age-partner" class="form-control">
    <input type="hidden" name="health" class="form-control">
    <input type="hidden" name="age" class="form-control">
</form>
@stop

@section('css')
<style type="text/css">
    .dot {
        height: 120px;
        width: 120px;
        background-color: #fff;
        border-radius: 50%;
        display: inline-block;
        cursor: pointer;
    }
    .dot-male {
        background-image: url("../img/hombre.png");
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
        margin: 20px;
    }
    .dot-female {
        background-image: url("../img/mujer.png");
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
        margin: 20px;
    }
</style>
@stop

@section('js')
 
<script type="text/javascript">
    $(document).ready(function() {
        var dateMask = IMask(
          document.getElementById('datepicker2'),
          {
            mask: '00/00/0000',
            min: new Date(1990, 0, 1),
            max: new Date(2020, 0, 1),
            lazy: false
          });

        var numberMask = IMask(
          document.getElementById('question-number-children'),
          {
            mask: Number,
            min: 1,
            max: 10,
            thousandsSeparator: ' '
          });

        

        
        //$('.js-date-birth').mask('##/##/#####');

        var currentHeight;
        var date = new Date();
        var plans;
        var step;
        var regNum = new RegExp('^[0-9]+$');
        var dateRegex = /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;

        message('#chat1-message', "Hola! Soy Socorro. Estoy feliz de verte por aquí. Te acompañaré y asistiré durante tu compra.\n¿Qué tal si empezamos por lo más importante? ¿Cuál es tu nombre?");

        function message(selector, message) {
            setTimeout(function(){
                $(selector).text(message);
                setTimeout(function() {
                    //$("#chat2").css('display', 'inherit');
                    setTimeout(function() {
                        /*$('#chat2-message').text("");
                        currentHeight = $('#chat1').height();
                        var chat2Height = $('#chat2').height();
                        currentHeight = currentHeight + chat2Height - 8
                        $('.chatbot__icon').css('top',  currentHeight + 'px');*/
                        setTimeout(function() {

                            $('.question__user_1').css('display', 'flex');
                            //$("#chat2").css('display', 'none');
                        }, 300);
                    }, 300);
                }, 300);
            }, 1500);
        }

        $('#question-name').keyup(function(e) {
            if(e.keyCode == 13)
            {
                $('#respuesta-1').click();
            }
        });
        $('#datepicker2').keyup(function(e) {
            if(e.keyCode == 13)
            {
                $('#date').click();
            }
        });

        $('#respuesta-1' ).click(function() {
            if ($.trim($('#question-name').val()).length) {
                $("input[name='name']").val($('#question-name').val());

                $('#chat3').css('display', 'inherit');
                $('.question__user_1').css('display', 'none');
                currentHeight = $('#chat1').height();
                var chat2Height = $('#chat2').height();
                currentHeight = currentHeight + chat2Height - 8
                $('.chatbot__icon').css('top',  currentHeight + 'px');
 
                setTimeout(function() {
                    $('#chat3-message').text("Mi nombre es " + $('#question-name').val());
                    var chat2Height = currentHeight + $('#chat2-respuesta').height() + $('#chat3').height();
                    currentHeight = chat2Height + 20;
                    $('.chatbot__icon').css('top', currentHeight + 'px');
                    // plans();
                    // 
                    
                    $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
                    step();
                }, 1300);
            } else {
                Swal({
                    type: 'warning',
                    title: 'Validar',
                    text: 'Debe ingresar un valor'
                });
            }
        });

        step = function() {

            // $("input[name='insurance']").val($(this).data('type'));

            if ($("input[name='client']").val() == "1") {
                // $('#chat6-respuesta').css('display', 'block');
                // $('#chat7-message').text('Busco seguro de ' + $(this).data('detail'));
                $('.question__user_3-person').css('display','none');

                $('#chat7').css('display','block');

                setTimeout(() => {
                    $('#chat8-message').text($('#question-name').val() + ', ahora dime cual es tu fecha de nacimiento');
                    $('#chat8').css('display','block');
                    $('.question__user_4').css('display','flex');
                }, 1000);
            } else {
                $('.question__user_3-business').css('display','none');
                $('#chat26').css('display','block');
                // $('#chat29-message').text('Estamos buscando un seguro de '+ $(this).data('detail'));
                // $('#chat28-respuesta').css('display','block');
                setTimeout(() => {
                    $('#chat27-message').text('¿'+ $('#question-name').val() +', cuantos coloboradores tiene tu empresa?');
                    $('.question__user_13').css('display','block');
                }, 1300);
            }
        };

        plans = function() {

            var text = "¿Qué tipo de seguro buscas? Elije uno.";

            $('#chat4').css('display', 'block');

            if ($("input[name='client']").val() == "1") {
                setTimeout(() => {
                    $('#chat4-message').text(text);
                    currentHeight = currentHeight + $('#chat4-respuesta').height() + $('#chat5').height();
                    $('.question__user_3-person').css('display', 'flex');
                 }, 1300);
            } else {
                setTimeout(() => {
                    $('#chat4-message').text(text);
                    currentHeight = currentHeight + $('#chat4-respuesta').height() + $('#chat5').height();
                    $('.question__user_3-business').css('display', 'flex');
                }, 1300);
            }
        }

        $("#respuesta-2-person").click(function() {

            $("input[name='client']").val(1);

            $('#chat4-respuesta').css('display','block');
            $('#chat5-message').text("Estoy interesado en un seguro para Personas");
            $('#chat5').css('display','block');
            $('.question__user_2').css('display','none');
            currentHeight = $('#chat1').height();
                var chat2Height = $('#chat2').height();
                currentHeight = currentHeight + chat2Height - 8
                $('.chatbot__icon').css('top',  currentHeight + 'px');
            setTimeout(() => {
                $('#chat6-message').text("¿Qué tipo de seguro buscas? Elije uno.");
                currentHeight = currentHeight + $('#chat4-respuesta').height() + $('#chat5').height();
                // $('.chatbot__icon').css('top',  currentHeight + 97 + 'px');
                $('.question__user_3-person').css('display', 'flex');
                $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
             }, 1300);
        });

        $("#respuesta-2-business").click(function () {

            $("input[name='client']").val(2);

            $('#chat4-respuesta').css('display','block');
            $('#chat5-message').text("Estoy interesado en un seguro para Empresas");
            $('#chat5').css('display','block');
            $('.question__user_2').css('display','none');
            currentHeight = $('#chat1').height();
                var chat2Height = $('#chat2').height();
                currentHeight = currentHeight + chat2Height - 8
                $('.chatbot__icon').css('top',  currentHeight + 'px');
            setTimeout(() => {
                $('#chat6-message').text("¿Qué tipo seguro buscas? Elije uno.");
                currentHeight = currentHeight + $('#chat4-respuesta').height() + $('#chat5').height();
                // $('.chatbot__icon').css('top',  currentHeight + 97 + 'px');
                $('.question__user_3-business').css('display','flex');
                $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
             }, 1300);
        });

        $('.insurance').on('click', function() {

            $("input[name='insurance']").val($(this).data('type'));

            if ($("input[name='client']").val() == "1") {
                $('#chat6-respuesta').css('display', 'block');
                $('#chat7-message').text('Busco seguro de ' + $(this).data('detail'));
                $('.question__user_3-person').css('display','none');

                $('#chat7').css('display','block');
                currentHeight = $('#chat1').height();
                var chat2Height = $('#chat2').height();
                currentHeight = currentHeight + chat2Height - 8
                $('.chatbot__icon').css('top',  currentHeight + 'px');
                setTimeout(() => {
                    $('#chat8-message').text($('#question-name').val() + ', ahora dime cual es tu fecha de nacimiento');
                    $('#chat8').css('display','block');
                    $('.question__user_4').css('display', 'flex');
                    $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
                }, 1000);
            } else {
                $('.question__user_3-business').css('display','none');
                $('#chat26').css('display','block');
                $('#chat29-message').text('Estamos buscando un seguro de '+ $(this).data('detail'));
                $('#chat28-respuesta').css('display','block');
                currentHeight = $('#chat1').height();
                var chat2Height = $('#chat2').height();
                currentHeight = currentHeight + chat2Height - 8
                $('.chatbot__icon').css('top',  currentHeight + 'px');
                setTimeout(() => {
                    $('#chat27-message').text('¿'+ $('#question-name').val() +', cuantos coloboradores tiene tu empresa?');
                    $('.question__user_13').css('display','block');
                    $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
                }, 1300);
            }
        });

        $("#date").on('click', function () {
            if ($.trim($("input[name='bod']").val()).length) {
                if (dateRegex.test($("input[name='bod']").val())) {
                    var dob = $("input[name='bod']").val();
                    var now = new Date();
                    var birthdate = dob.split("/");
                    var born = new Date(birthdate[2], birthdate[1]-1, birthdate[0]);
                    age = get_age(born, now);

                    $("input[name='age']").attr('value', age);

                    // if (age < 18) {
                    //     Swal({
                    //       type: 'warning',
                    //       title: 'Aviso',
                    //       text: 'Edad debería ser igual o mayor a 18 años',
                    //       // footer: '<a href>Why do I have this issue?</a>'
                    //     });
                    //     return false;
                    // } else if (age >= 90) {
                    //     Swal({
                    //       type: 'warning',
                    //       title: 'Aviso',
                    //       text: 'Edad debería ser menor a 90 años',
                    //       // footer: '<a href>Why do I have this issue?</a>'
                    //     });
                    //     return false;
                    // } else {
                        $('.question__user_4').css('display','none');
                        $('#chat9-respuesta').css('display','block');
                        $('#chat10-message').text("Tengo " + $("input[name='age']").val() + ' años');
                        $('#chat10').css('display','block');
                        var coordenadas = $("#chat3").position()
                        var currentHeight = parseInt(coordenadas.top) - parseInt(12); 
                        
                        $('.chatbot__icon').css('top',  currentHeight + 'px');
                        setTimeout(() => {
                            $('#chat11-message').text('Ahora voy hacerte unas preguntas rápidas sobre ti y tu familia.');
                            $('#chat11').css('display', 'block');
                            setTimeout(() => {
                                $('#chat12-message').text('¿Cuál es tu género?');
                                //effect
                                $('.question__user_5').css('display', 'flex');
                            }, 1000);
                            $('body, html').animate({
                                scrollTop: $(document).height()
                            },1600);
                        }, 1000);
                    // }
                } else {
                    Swal({
                        type: 'warning',
                        title: 'Validar',
                        text: 'Debe ingresar un valor de fecha en formato: DD/MM/AAAA'
                    });
                }
            } else {
                Swal({
                    type: 'warning',
                    title: 'Validar',
                    text: 'Debe ingresar un valor de fecha en formato: DD/MM/AAAA'
                });
            }
        });

        $('.gender').on('click', function() {

            $("input[name='gender']").val($(this).data('type'));

            $('.question__user_5').css('display','none');
            $('#chat12-respuesta').css('display','block');
            $('#chat13-message').text('Mi género es ' + $(this).data('detail'));
            $('#chat13').css('display','block');
           
            $('.chatbot__icon').css('top', '630px');
            setTimeout(() => {
                $('#chat14-message').text('¿Eres casado? También se considera casado si estas en una convivencia.');
                $('.question__user_6').css('display','flex');
                $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
            }, 1600);
        });

        $('.married').on('click', function() {

            $("input[name='married']").val($(this).data('type'));

            $('.question__user_6').css('display','none');
            $('#chat14-respuesta').css('display','block');
            $('#chat15-message').text('Mi estado civil es ' + $(this).data('detail'));
            
            if($(this).data('type') == "0") {
                $('.chatbot__icon').css('top', '796px');
                $('#chat15').css('display', 'block');
                setTimeout(() => {
                    $('#chat16-message').text('¿Tienes hijos? Solo considera hijos menores a 28 años.');
                    //efecto
                    $('.question__user_7').css('display', 'flex');
                    //edades y genero de hijos
                    $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
                }, 1600);
            } else {
                //seleccione rango de edades de comyugue
                $('#chat16').css('display', 'block');
                $('.chatbot__icon').css('top', '798px');
                setTimeout(() => {
                    $('#chat17-message').text('Seleccione el rango de edad al que pertenece su cónyuge');
                    $('.question__user_8').css('display', 'flex');
                    $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
                }, 1300);
            }
        });

        $('.age-partner').on('click', function() {

            $("input[name='age-partner']").val($(this).data('min') + "-" + $(this).data('max'));

            $('.question__user_8').css('display','none');
            $('#chat17-respuesta').css('display','block');
            $('#chat18-message').text('El rango de edad de mi cónyuge es ' + $(this).data('description') + '.');
            $('#chat15').css('display','block');
            $('.chatbot__icon').css('top', '961px');
            setTimeout(() => {
                $('#chat16-message').text('¿Tienes hijos? Solo considera hijos menores a 28 años.');
                //efecto
                $('.question__user_7').css('display','flex');
                //edades y genero de hijos
                $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
            }, 1300);
        });

        $('.children').on('click', function() {

            $("input[name='children']").val($(this).data('type'));

            $('#chat19-message').text($(this).data('detail') + ', tengo hijos');
            $('#chat18-respuesta').css('display', 'block');
            $('.question__user_7').css('display', 'none');

            if($(this).data('type') == "1") {
                $('#chat19').css('display', 'block');
                if($("input[name='married']").val() == 0){
                    
                    $('.chatbot__icon').css('top', '960px');
                }
                else{
                    $('.chatbot__icon').css('top', '1125px');
                }

                setTimeout(() => {
                    $('#chat20-message').text('¿Cuantos hijos tienes? Ingrese la cantidad de hijos');
                    setTimeout(() => {
                        $('.question__user_9').css('display', 'flex');
                    }, 1200);
                    $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
                }, 1300);
            } else {
                // $('#chat24').css('display','block');
                setTimeout(() => {
                    // $('#chat25-message').text('¿En los últimos años has pasado tu o tu familia por algún tratamiento médico complejo?');
                    // $('.question__user_12').css('display', 'flex');
                    finishMessage();
                }, 1300);
                // $('.question__user_10').css('display','none');
                // $('#chat22').css('display','block');
                // setTimeout(() => {
                //     $('#chat23-message').text('Ya casi terminamos. Ahora indicanos tus ingresos aproximados');
                //     $('.question__user_11').css('display','block');
                // }, 1300);
            }
        });

        $('#question-number-children').keyup(function(e){
            if(e.keyCode == 13)
            {
                $('#question-number-children-flecha').click();
            }
        });

        $('.range').on('click', function() {
            $('.question__user_11').css('display','none');
            $('#chat24-message').text('Mis ingresos aproximados son ' + $("input[name='range']").val() +' soles.');
            $('#chat23-respuesta').css('display','block');

            //last question
            $('#chat24').css('display','block');
            $('.chatbot__icon').css('top', '961px');
            setTimeout(() => {
                $('#chat25-message').text('¿En los últimos años has pasado tu o tu familia por algún tratamiento médico complejo?');
                $('.question__user_12').css('display', 'flex');
                $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
            }, 1300);
        });

        $('.health').on('click', function() {

            var message_txt;

            $("input[name='health']").val($(this).data('type'));

            if ($(this).data('type') == "1") {
                message_txt = "hemos pasado por tratamientos médicos durante estos últimos años.";
            } else {
                message_txt = "no hemos pasado por ningún tratamiento médico durante estos últimos años.";
            }

            // $('#chat26').css('display','block');
            // setTimeout(() => {
            //     $('#chat25-message').text('¿En los últimos años has pasado tu o tu familia por algún tratamiento médico complejo?');
            //     $('.question__user_13').css('display', 'flex');
            // }, 1300);

            $('.question__user_12').css('display', 'none');
            $('#chat26-message').text( $(this).data('detail') + ", " + message_txt);
            $('#chat25-respuesta').css('display', 'block');

            Swal({
              // position: 'top-end',
              type: 'success',
              title: 'Felicitaciones '+ $('#question-name').val() +'! Cotización completada.',
              showConfirmButton: true,
              confirmButtonText: 'Ver Planes'
            }).then((result) => {
                if (result.value) {
                    $('#quotationForm').submit();
                }
            });
        });

        // function answerNine() {
        $('.qty_children').on('click', function() {
            if ($.isNumeric($('#question-number-children').val())) {
                if ($.trim($('#question-number-children').val()).length <= 2) {
                    if ($('#question-number-children').val() >= 1 && $('#question-number-children').val() <= 15) {
                        var childrenNumber = parseInt($('#question-number-children').val());
                        $('#chat21-message').text('Tengo ' + childrenNumber + ' hijos');
                        $('#chat20-respuesta').css('display', 'block');
                        $('.question__user_9').css('display', 'none');
                        
                        setTimeout(() => {
                            $('#chat22-message').text('Seleccione el detalle de sus hijos');
                            $('#chat21').css('display','block');
                            //forEach
                            $('.question__user_10').css('display','flex');
                            if($("input[name='married']").val() == 0){
                                
                                $('.chatbot__icon').css('top', '1124px');
                            }
                            else{
                                $('.chatbot__icon').css('top', '1285px');
                            }
                            
                            createChildrenHTML(childrenNumber);
                            $('body, html').animate({
                                scrollTop: $(document).height()
                            },1600);
                        }, 1300);
                    } else {
                        Swal({
                            type: 'warning',
                            title: 'Validar',
                            text: 'Debe ingresar un valor numérico valido'
                        });
                    }
                } else {
                    Swal({
                        type: 'warning',
                        title: 'Validar',
                        text: 'Debe ingresar un valor numérico valido'
                    });
                }
            } else {
                Swal({
                    type: 'warning',
                    title: 'Validar',
                    text: 'Debe ingresar un valor'
                });
            }
        });

        function createChildrenHTML(childrenNumberValue) {

            for(var i=0; i < childrenNumberValue; i++){
                $(".question__user_10_list").append(
                '<div class="row" style="width:80%">' +
                '<div class="form-group col-md-6">' +
                    '<input type="text" name="age_children[]" min="1" id="child-age-'+ i +'" class="questions__user form-control ageInput" autocomplete="false" placeholder="Edad" required>' +
                '</div>' +
                '<div class="form-group col-md-6">' +
                    '<select name="gender_children[]" class="questions__user form-control" id="child-sex-'+ i +'" required>'+
                        '<option value="0" selected>Selecione Sexo</option>'+
                        '<option value="1">Masculino</option>'+
                        '<option value="2">Femenino</option>'+
                    '</select>'+
                '</div>' +
                '</div>'
                );
                var ageInput = IMask(
                  document.getElementById('child-sex-'+ i),
                  {
                    mask: Number,
                    min: 1,
                    max: 28,
                    thousandsSeparator: ' '
                  });
                }
        }

        // function nextQuestion10() {
        $('.next').on('click', function() {

            var flag = false;

            $.each($('input, select', '.question__user_10_list'), function(k,j) {
                if ($(this).val() && ($(this).val() >= "1" && $(this).val() <= "28")) {
                    flag = true;
                }
            });

            if (flag) {
                $('#chat22-message').css('display', 'none');
                $('#chat24').css('display', 'none');
                $('.question__user_10').css('display', 'none');

                setTimeout(() => {
                    // $('#chat25-message').text('¿En los últimos años has pasado tu o tu familia por algún tratamiento médico complejo?');
                    // $('.question__user_12').css('display', 'flex');
                    finishMessage();
                    $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
                }, 1300);
                // $('.question__user_10').css('display','none');
                // $('#chat22').css('display','block');
                // setTimeout(() => {
                //     $('#chat23-message').text('Ya casi terminamos. Ahora indicanos tus ingresos aproximados');
                //     $('.question__user_11').css('display','block');
                // }, 1300);
            }else {
                  // console.log($(this).val());
                   if($(this).val() == '0')
                   {
                        Swal({
                            type: 'warning',
                            title: 'Validar',
                            text:  'Debe Seleccionar sexo'
                        });
                   }else if($(this).val() == '')
                   {
                        Swal({
                            type: 'warning',
                            title: 'Validar',
                            text:  'El campo edad no debe estar vacio'
                        });
                   }else if($(this).val() > 28)
                   {
                        Swal({
                            type: 'warning',
                            title: 'Validar',
                            text:  'La Edad debe ser menor a 28 Años'
                        });
                   }
                    flag = false;
                    return false;
                }
        });

        $('.worker').on('click', function() {
             if ($.trim($('#workers_number').val()).length) {

            workersNumber = parseInt($("input[name='qty_worker']").val());

             } else {
              Swal({
                  type: 'warning',
                  title: 'Validar',
                  text: 'Debe ingresar un valor'
              });
            //Fin validacion
        }
          if ($.trim($('#workers_number').val()).length) {
             var total = parseInt($("input[name='qty_worker']").val());
             if(total <= 100) {
                $('.question__user_13').css('display','none');
            workersNumber = parseInt($("input[name='qty_worker']").val());
            $('#chat28-message').text("Contamos con " + workersNumber + " colaboradores" );
            $('#chat27-respuesta').css('display','block');
            //new question
            $('#chat29').css('display','inherit');
            setTimeout(() => {
                $('#chat30-message').text( "¿" + $("input[name='name']").val() +", sabes cuantos dependientes en total tienen tus colaboradores?" );
                $('.question__user_14').css('display','block');
                $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
            }, 1200);
             } else {
              Swal({
                  type: 'warning',
                  title: 'Validar',
                  text: 'Debe ingresar un valor menor o igual a 100 Colaboradores'
              });
            //Fin validacion
        }
       }
    });

        $('.dependent').on('click', function() {
             if ($.trim($('#workers_dependency_number').val()).length)
              {
            var i;

            workers_dependency_number = parseInt($("input[name='qty_dependent']").val());
            $('.question__user_14').css('display','none');
            //show response
            $('#chat311-message').text( "Nuestros colaboradores tienen en total " + workers_dependency_number + " dependientes" );
            $('#chat30-respuesta').css('display','block');
            var total = parseInt($("input[name='qty_worker']").val()) + workers_dependency_number;
            if(total <= 5) {
                //show name and sex for each worker --- append
                $('.question__user_15').css('display', 'block');

                for(i=0; i < total; i++){
                    $(".append_dependency_worker_list").append(
                    '<div class="row">' +
                    '<div class="form-group col-md-6">'+
                        '<input type="number" name="age_worker[]" min="1" id="edad" class="questions__user form-control" placeholder="Edad" required>'+
                    '</div>' +
                    '<div class="form-group col-md-6">' +
                         '<select name="gender_worker[]" class="questions__user form-control" id="sexo'+ i +'" required>'+
                        '<option value="0" selected>Selecione Sexo</option>'+
                        '<option value="1">Masculino</option>'+
                        '<option value="2">Femenino</option>'+
                    '</select>'+
                      '</div>' +
                    '</div>'
                    );
                }
            } else {
                $('.question__user_15').css('display','none');
                $('#chat31').css('display','block');
                setTimeout(() => {
                    $('#chat32-message').text("¿Cual es tu aporte anueal a ESSALUD?" );
                    $('.question__user_16').css('display','block');
                    $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
                }, 1300);
            }
              } else {
                      Swal({
                              type: 'warning',
                              title: 'Validar',
                              text: 'Debe ingresar un valor'
                          });
             }

       } );
        $('.nextQ3').on('click', function() {
         //    if ($.trim($('#sexo').val()).length)
       //         {
             $('.question__user_15').css('display','none');
            //last question
            $('#chat31').css('display','block');
            setTimeout(() => {
                $('#chat32-message').text("¿Cual es tu aporte anueal a ESSALUD?" );
                $('.question__user_16').css('display','block');
                $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
            }, 1300);
          /*   }else {
              Swal({
                  type: 'warning',
                  title: 'Validar',
                  text: 'Debe llenar todos los campos de Edad y Sexo'
              });
          }*/
    });

        $('.nextQ4').on('click', function() {
            if ($.trim($('#aporte_salud').val()).length) {
            $('.question__user_16').css('display','block');
         //   finishMessage();
             }else {
              Swal({
                  type: 'warning',
                  title: 'Validar',
                  text: 'Debe ingresar un valor'
              });
          }

          if ($.trim($('#aporte_salud').val()).length) {
            var total = parseInt($("input[name='qty_contribute']").val());
             if(total <= 20000) {
            $('.question__user_16').css('display','none');
            finishMessage();
            $('body, html').animate({
                        scrollTop: $(document).height()
                    },1600);
             }else {
              Swal({
                  type: 'warning',
                  title: 'Validar',
                  text: 'Debe ingresar un valor menor o igual a 20000'
              });
          }
        }
    });

        function insuranceDetailTypeBusiness (insuranceDetailValue) {
            insuranceDetail = insuranceDetailValue;
            $('.question__user_3-business').css('display','none');
            //mostrar
            $('#chat26').css('display','block');
            setTimeout(() => {
                $('#chat27-message').text('¿'+ name +', cuantos coloboradores tiene tu empresa?');

            }, 1300);
        }

        // Get Age
        function get_age(born, now) {
          var birthday = new Date(now.getFullYear(), born.getMonth(), born.getDate());
          if (now >= birthday)
            return now.getFullYear() - born.getFullYear();
          else
            return now.getFullYear() - born.getFullYear() - 1;
        }

        $(function() {
            $( "#datepicker" ).datepicker({
                startDate: new Date(),
                dateFormat: 'dd/mm/yy',
                maxDate: new Date(),
                altField: "#date",
            });
        }).on('changeDate', function(e) {
            date = $('#date').val();
        });

        // Send
        // $('#wrapped').on('click', function(event) {
        function sendData() {

            // event.preventDefault()
            // $('#success-page').hide()
            // checkRequiredFields()
            // var formContent = getFormData()
            // $('#spinner').fadeIn()

            var person = {
                'name' : $('#question-name').val(),
                'client' : $("input[name='client']").val(),
                'insurance' : $("input[name='insurance']").val(),
                'age' : $("input[name='age']").val(),
                'gender' : $("input[name='gender']").val(),
                'married': $("input[name='married']").val(),
                'age-partner': $("input[name='age-partner']").val(),
                'children': $("input[name='children']").val(),
                'childrenNumber': 1,
                'income' : $("input[name='range']").val(),
                'health': $("input[name='health']").val(),
            };

            

            var data = JSON.stringify(person);

            var baseUrl = '{{ env("BASE_URL", url('/')) }}';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: baseUrl + "/quotation",
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                // processData: false,
                // contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    $('#principalPanel').empty().append($(json.data));
                },
                error: function (e) {
                    Swal({
                        type: 'success',
                        title: 'Validar',
                        // text: e.responseJSON.errors.rut[0]
                    });
                    $("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            }).always(function(){
                // $('#spinner').fadeOut()
            });
        }
        $("#catnChild").click(function(){
            $('.chatbot__icon').hide();
        })
        function finishMessage() {
            $('.chatbot__icon').hide();
            Swal({
              type: 'success',
              title: 'Felicitaciones '+ $('#question-name').val() +'! Cotización completada.',
              showConfirmButton: true,
              allowOutsideClick: false,
              allowEscapeKey: false, 
              confirmButtonText: 'Ver Planes'
            }).then((result) => {
                if (result.value) {
                    $('#quotationForm').submit();
                }
            });
        }
    });
</script>
<style type="text/css">
    .swal2-popup .swal2-styled.swal2-confirm {
        border-radius: 1.25em !important;
    }
    .swal2-popup .swal2-styled.swal2-confirm {
       box-shadow: none !important;
    }
    .swal2-popup .swal2-actions{
        width: 50%;
    }
    .swal2-popup .swal2-title{
        color: #8a8a8a !important;
    }
    .swal2-icon.swal2-success [class^=swal2-success-line] {
        display: block;
        position: absolute;
        height: 0.4135em !important;
        border-radius: .125em;
        background-color: #49b90a !important;
        z-index: 2;
    }
    .swal2-icon.swal2-success .swal2-success-ring {
        border: none !important;
    }
    .swal2-popup {
        display: none;
        position: relative;
        flex-direction: column;
        justify-content: center;
        width: 40em !important;
        max-width: 100%;
        padding: 3.25em !important;
        border-radius: .3125em;
        background: #fff;
        font-family: inherit;
        font-size: 1rem;
        box-sizing: border-box;
    }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
{{-- <link rel="stylesheet" href="/resources/demos/style.css"> --}}
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

@stop
