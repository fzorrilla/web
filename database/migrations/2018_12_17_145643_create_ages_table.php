<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('min');
            $table->unsignedInteger('max');
            $table->enum('active', ['1', '0'])->default('1');
            $table->string('description');
            $table->unsignedInteger('insured_id')->nullable();            
            $table->timestamps();
            $table->foreign('insured_id')->references('id')->on('insureds')
                ->onDelete('cascade')
                ->onUpdate('cascade');             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ages');
    }
}
