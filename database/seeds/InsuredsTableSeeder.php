<?php

use Illuminate\Database\Seeder;

class InsuredsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Insured::create([
            'name' => 'Titular',
            'description' => 'Comprador'
        ]);

        App\Insured::create([
            'name' => 'Conyuge',
            'description' => 'Parentesco de afinidad'
        ]);

        App\Insured::create([
            'name' => 'Menores de 18 años',
            'description' => 'Hijos menores de 18 años'
        ]);

        App\Insured::create([
            'name' => 'Mayores de 18 años',
            'description' => 'Hijos mayores de 18 años hasta 28 años'
        ]);        
    }
}
