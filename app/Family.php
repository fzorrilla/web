<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria

class Family extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'identity_id',
        'cod_dni',
        'dni',
        'name',
        'name2',
        'last_name_1',
        'last_name_2',
        'gender_id',
        'bod',
        'age',
        'nationality_id',
        'civil_id',
        'phone',
        'email',
        'direction_id',
        'way_id',
        'address',
        'district_id',
        'country_id',
        'insured_id',
       // 'treatment',
        'disability',
        'weightloss',
        'emergency',
        'drug',
        'pregnant',
        'weigh',
        'size',
        'months',
        'end',
        'doctor',
        'year',
        'situation',
        'insurance',
        'typo',
        'doctor',
        'insurer',
        'outre',
        'bod1',
        'bod2',
        'quotation_id',
        'completed'
    ];

    public function quotation()
    {
        return $this->belongsTo(Quotation::class);
    }
     public function identities()
    {
        return $this->belongsTo(Identity::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function insured()
    {
        return $this->belongsTo(Insured::class);
    }
    public function pathology()
    {
        return $this->belongsToMany(Pathology::class);

    }

}
