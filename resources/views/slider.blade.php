<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <p class="title-slide">Cuidamos la salud de ti</p>
      <p class="title-slide2">y los tuyos</p>
      <p class="title-slide3">::::::::::::::::::::::: Cuidamos lo que más te importa</p>
      <img class="d-block w-100" src="{{ asset('img/fase1.jpeg') }}" alt="First slide" style="max-height: 600px;">
    </div>
    <div class="carousel-item">
      <p class="title-slide">Tus colaboradores son parte</p>
      <p class="title-slide2">importante de tu empresa</p>
      <p class="title-slide3">::::::::::::::::::::::: Cuidamos de la salud de ellos</p>
      
      <img class="d-block w-100" src="{{ asset('img/fase2.jpeg') }}" alt="First slide" style="max-height: 600px;">
    </div>
    <div class="carousel-item">
      <p class="title-slide">Porque queremos verlos </p>
      <p class="title-slide2">siempre sanos</p>
      <p class="title-slide3">::::::::::::::::::::::: con una sombra Cuidamos la salud de ti y los tuyos</p>
      <img class="d-block w-100" src="{{ asset('img/fase3.jpeg') }}" alt="First slide" style="max-height: 600px;">
    </div>
  </div>
  <!--<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>-->
</div>